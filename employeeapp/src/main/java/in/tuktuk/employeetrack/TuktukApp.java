package in.tuktuk.employeetrack;

import android.location.Location;
import android.support.multidex.MultiDex;

import com.softminesol.locations.locationmanager.LocationManagerService;
import com.softminesol.propertysurvey.CommonBaseUrl;

import in.tuktuk.usersession.login.view.LoginActivity;
import in.tuktuk.usersession.userSession.data.net.LoginAPIURL;

import in.tuktuk.employeetrack.view.activity.SplashScreenActivity;
import frameworks.AppBaseApplication;
import frameworks.appsession.AppSessionManager;
import frameworks.basemvp.AppBaseActivity;
import frameworks.routers.ILocationRouter;
import frameworks.routers.ILoginInterceptor;

/**
 * Created by sandeepgoyal on 12/05/18.
 */

public class TuktukApp extends AppBaseApplication implements ILoginInterceptor, ILocationRouter {
    AppSessionManager sessionValue;
    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        //Fabric.with(this, new Crashlytics());
        sessionValue = new AppSessionManager(this);
        generateAppBaseUrl();
        LoginActivity.userType=LoginActivity.USER_TYPE_EMPLOYEE;
    }

    private void generateAppBaseUrl() {
        LoginAPIURL.BASE_URL=CommonBaseUrl.BASE_URL;
    }

    @Override
    public void startLogin() {
        mApplication.getBaseContext().startActivity(SplashScreenActivity.getIntent(mApplication.getBaseContext()));
    }

    @Override
    public void logout() {
        sessionValue.clearSession();
        startLogin();
    }


    @Override
    public void onLocationChanged(Location location, AppBaseActivity appBaseActivity) {
        LocationManagerService locationManagerService = LocationManagerService.getInstance();
        locationManagerService.setCurrentLocation(location);
    }
    /*public void initRealm() {

        Realm.init(this);
        propertyDataConfig = new RealmConfiguration.Builder()
                .name(RealmConstants.SCHEMA_DATA)
                .schemaVersion(RealmConstants.SCHEMA_DATA_VERSION).build();

    }

    public Realm getPropertyDataRealm() {
        Realm.compactRealm(propertyDataConfig);
        return Realm.getInstance(propertyDataConfig);
    }*/


}
