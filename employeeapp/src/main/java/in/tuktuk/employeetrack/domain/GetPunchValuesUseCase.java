package in.tuktuk.employeetrack.domain;

import javax.inject.Inject;

import frameworks.network.usecases.RequestParams;
import frameworks.network.usecases.UseCase;
import frameworks.punchsession.PunchData;
import frameworks.punchsession.PunchSessionManager;
import rx.Observable;

public class GetPunchValuesUseCase extends UseCase<PunchData> {

    private final IDeliveryRepository deliveryRepository;

    @Inject
    public GetPunchValuesUseCase(IDeliveryRepository deliveryRepository) {
        this.deliveryRepository=deliveryRepository;
    }

    @Override
    public Observable<PunchData> createObservable(RequestParams requestParams) {
        return deliveryRepository.getPunchStatus();
    }
}
