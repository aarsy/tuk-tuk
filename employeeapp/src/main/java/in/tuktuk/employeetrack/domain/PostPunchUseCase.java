package in.tuktuk.employeetrack.domain;

import javax.inject.Inject;

import in.tuktuk.employeetrack.view.model.PunchResponse;
import frameworks.network.usecases.RequestParams;
import frameworks.network.usecases.UseCase;
import frameworks.punchsession.PunchData;
import frameworks.punchsession.PunchSessionManager;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

public class PostPunchUseCase extends UseCase<PunchData> {

    private final IDeliveryRepository deliveryRepository;
    private final PunchSessionManager punchSessionManager;


    public RequestParams createRequestParams(int attendanceStatus){
        RequestParams requestParams=RequestParams.create();
        PunchData punchData=new PunchData();
        punchData.setAttendance(attendanceStatus);
        requestParams.putObject("body", punchData);
        return requestParams;
    }

    @Inject
    public PostPunchUseCase(IDeliveryRepository deliveryRepository, PunchSessionManager punchSessionManager) {
        this.deliveryRepository=deliveryRepository;
        this.punchSessionManager=punchSessionManager;
    }


    @Override
    public Observable<PunchData> createObservable(RequestParams requestParams) {
        PunchData punchData=(PunchData) requestParams.getObject("body");
        return deliveryRepository.punch(punchData).map(new Func1<PunchResponse, PunchData>() {
            @Override
            public PunchData call(PunchResponse punchResponse) {
                PunchData punchData = new PunchData();
                punchData.setAttendance(punchResponse.getData().getAttendance());
                punchData.setDate(punchResponse.getData().getDate());
                return punchData;
            }
        }).doOnNext(savePunchData());
    }

    private Action1<? super PunchData> savePunchData() {
        return new Action1<PunchData>() {
            @Override
            public void call(PunchData punchData) {
                punchSessionManager.savePunch(punchData);
            }
        };
    }
}
