package in.tuktuk.employeetrack.domain;

import com.google.gson.JsonObject;

import java.util.HashMap;

import in.tuktuk.employeetrack.view.model.PunchResponse;
import frameworks.punchsession.PunchData;
import rx.Observable;

public interface IDeliveryRepository {

    Observable<JsonObject> uploadCoordinates(HashMap<String, Object> parameters);

    Observable<PunchResponse> punch(PunchData punchData);

    Observable<PunchData> getPunchStatus();
}
