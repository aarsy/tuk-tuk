package in.tuktuk.employeetrack.domain;

import com.google.gson.JsonObject;

import javax.inject.Inject;

import frameworks.network.usecases.RequestParams;
import frameworks.network.usecases.UseCase;
import rx.Observable;

public class UploadCoordinatesUseCase extends UseCase<JsonObject> {

    private final IDeliveryRepository deliveryRepository;
    private final String LATITUDE="latitude";
    private final String LONGITUDE="longitude";
    private final String DATETIME="dateTime";
    private final String TRACKINGTYPE="trackingType";
    private final String DATA="data";

    @Inject
    public UploadCoordinatesUseCase(IDeliveryRepository deliveryRepository) {
        this.deliveryRepository = deliveryRepository;
    }

    public RequestParams createRequestParams(double lat, double longi, String dateTime, String trackingType){
        RequestParams requestParams=RequestParams.create();
        requestParams.putString(LATITUDE, String.valueOf(lat));
        requestParams.putString(LONGITUDE, String.valueOf(longi));
        requestParams.putString(DATETIME, dateTime);
        requestParams.putString(TRACKINGTYPE, trackingType);
        return requestParams;
    }

    @Override
    public Observable<JsonObject> createObservable(RequestParams requestParams) {
        return deliveryRepository.uploadCoordinates(requestParams.getParameters());
    }
}
