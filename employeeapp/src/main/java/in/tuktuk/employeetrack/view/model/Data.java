package in.tuktuk.employeetrack.view.model;

import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("time")
	private String date;
	@SerializedName("message")
	private String message;

	@SerializedName("attendance")
	private int attendance;

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public int getAttendance() {
		return attendance;
	}

	public void setAttendance(int attendance) {
		this.attendance = attendance;
	}

	@Override
 	public String toString(){
		return 
			"ImageResponseData{" +
			"date = '" + date + '\'' + 
			",message = '" + message + '\'' + 
			",attendance = '" + attendance + '\'' +
			"}";
		}
}
