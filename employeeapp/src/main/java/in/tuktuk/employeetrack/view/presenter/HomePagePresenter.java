package in.tuktuk.employeetrack.view.presenter;

import javax.inject.Inject;

import in.tuktuk.employeetrack.domain.GetPunchValuesUseCase;
import in.tuktuk.employeetrack.domain.PostPunchUseCase;
import frameworks.basemvp.AppBasePresenter;
import frameworks.network.model.ResponseException;
import frameworks.punchsession.PunchData;
import rx.Subscriber;


public class HomePagePresenter extends AppBasePresenter<IHomePageViewContractor.View> implements IHomePageViewContractor.Presenter {


    private final PostPunchUseCase postPunchUseCase;
    private final GetPunchValuesUseCase getPunchValuesUseCase;
    private PunchData punchData;

    @Inject
    public HomePagePresenter(PostPunchUseCase postPunchUseCase, GetPunchValuesUseCase getPunchValuesUseCase) {
        this.postPunchUseCase = postPunchUseCase;
        this.getPunchValuesUseCase = getPunchValuesUseCase;

    }


    @Override
    public void getPunchStatus() {
        getView().showProgressBar();
        getPunchValuesUseCase.execute(new Subscriber<PunchData>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                getView().hideProgressBar();
                if (e instanceof ResponseException) {
                    getView().showToast(e.getMessage());
                }else{
                    getView().showToast("Some Error Occurred Please Try again");
                }
            }

            @Override
            public void onNext(PunchData punchData) {
                getView().hideProgressBar();
                setPunchResult(punchData);
                getView().onSuccessPunch();
            }
        });
    }

    @Override
    public void setPunchStatus() {
        getView().showProgressBar();
        postPunchUseCase.execute(postPunchUseCase.createRequestParams(getAttendanceToPost()), new Subscriber<PunchData>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                getView().hideProgressBar();
                if (e instanceof ResponseException) {
                    getView().showToast(e.getMessage());
                }else{
                    getView().showToast("Some Error Occurred Please Try again");
                }
            }

            @Override
            public void onNext(PunchData punchData) {
                getView().hideProgressBar();
                setPunchResult(punchData);
                getView().onSuccessPunch();
            }
        });

    }

    private int getAttendanceToPost() {
        if (getPunchData() != null) {
            if(getPunchData().getAttendance()==1){
                return 0;
            }
        }
        return 1;
    }

    private void setPunchResult(PunchData punchData) {
        this.punchData = punchData;
    }

    public PunchData getPunchData() {
        return punchData;
    }
}
