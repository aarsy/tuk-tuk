package in.tuktuk.employeetrack.view;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.softminesol.locations.locationmanager.LocationManagerService;
import com.softminesol.locations.locationmanager.Locations;

import javax.inject.Inject;

import in.tuktuk.employeetrack.TuktukApp;
import in.tuktuk.employeetrack.di.DaggerHomeComponent;
import in.tuktuk.employeetrack.di.HomeComponent;
import in.tuktuk.employeetrack.view.presenter.UploadCoordinatesPresenter;


public class UploadCoordinatesService extends Service implements LocationManagerService.OnLocationChangedListener{

    @Inject
    UploadCoordinatesPresenter presenter;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public static Intent getIntent(Context context) {
        return new Intent(context, UploadCoordinatesService.class);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        HomeComponent component = DaggerHomeComponent.builder().baseAppComponent(((TuktukApp.getApplication()).getBaseAppComponent())).build();
        component.inject(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Locations location = LocationManagerService.getInstance().getCurrentLocation();
        if (location != null) {
            presenter.uploadCoordinates(location.getLattitude(), location.getLongitude());
        }
        LocationManagerService.getInstance().addOnLocationChangedListeners(this);
        return START_STICKY;
    }

    @Override
    public void onLocationChanged(Locations location) {
        presenter.uploadCoordinates(location.getLattitude(), location.getLongitude());
    }
}
