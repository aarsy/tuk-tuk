package in.tuktuk.employeetrack.view.presenter;

import android.util.Log;

import com.google.gson.JsonObject;

import javax.inject.Inject;


import in.tuktuk.employeetrack.domain.UploadCoordinatesUseCase;
import frameworks.formatter.DateFormatter;
import rx.Subscriber;

public class UploadCoordinatesPresenter implements IUploadCoordinatesContract.Presenter{


    private final UploadCoordinatesUseCase uploadCoordinatesUseCase;

    @Inject
    public UploadCoordinatesPresenter(UploadCoordinatesUseCase uploadCoordinatesUseCase) {
        this.uploadCoordinatesUseCase=uploadCoordinatesUseCase;
    }

    public void uploadCoordinates(double latitude, double longitude){
        uploadCoordinatesUseCase.execute(uploadCoordinatesUseCase.createRequestParams(latitude, longitude, DateFormatter.getCurrentDateTime(), "L"), new Subscriber<JsonObject>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(JsonObject jsonObject) {
                Log.d("Coordiupload response", jsonObject.toString());
            }
        });
    }
}
