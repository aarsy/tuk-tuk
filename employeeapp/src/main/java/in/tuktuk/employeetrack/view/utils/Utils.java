package in.tuktuk.employeetrack.view.utils;

public class Utils {
    public static final String FORCE_REFRESH = "FORCE_REFRESH";
    public static final String LIMIT = "limit";
    public static final String OFFSET = "offset";
    public static final String LAT = "lat";
    public static final String LONG = "long";
    public static final String KEY_DELIVERYID = "deliveryId";
    public static final String KEY_POSITION = "position";
}
