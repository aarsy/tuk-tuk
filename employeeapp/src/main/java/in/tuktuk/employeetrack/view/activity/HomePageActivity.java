package in.tuktuk.employeetrack.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import javax.inject.Inject;

import in.tuktuk.employeetrack.R;
import in.tuktuk.employeetrack.TuktukApp;
import in.tuktuk.employeetrack.di.DaggerHomeComponent;
import in.tuktuk.employeetrack.view.UploadCoordinatesService;
import in.tuktuk.employeetrack.view.presenter.HomePagePresenter;
import in.tuktuk.employeetrack.view.presenter.IHomePageViewContractor;
import frameworks.appsession.AppSessionManager;
import frameworks.basemvp.AppBaseActivity;
import frameworks.punchsession.PunchSessionManager;
import frameworks.utils.ServiceRunningStatusUtils;

public class HomePageActivity extends AppBaseActivity<IHomePageViewContractor.Presenter> implements IHomePageViewContractor.View {

    @Inject
    HomePagePresenter mPresenter;
    private TextView btnPunch;

    public static Intent getIntent(Context context) {
        Intent i = new Intent(context, HomePageActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        return i;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        btnPunch = findViewById(R.id.btn_punch);
        btnPunch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.setPunchStatus();
            }
        });
        mPresenter.getPunchStatus();
    }

    @Override
    protected void initInjector() {
        DaggerHomeComponent.builder().baseAppComponent((TuktukApp.getApplication()).getBaseAppComponent()).build().inject(this);
        mPresenter.attachView(this);
    }

    public void startLogin() {
        AppSessionManager sessionValue = new AppSessionManager(getContext());
        sessionValue.clearSession();
        PunchSessionManager punchSessionManager = new PunchSessionManager(getContext());
        punchSessionManager.clearPunch();
        startActivity(SplashScreenActivity.getIntent(getContext()));
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home_page, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.item_logout) {
            startLogin();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public int getViewToCreate() {
        return R.layout.activity_home;
    }

    @Override
    public IHomePageViewContractor.Presenter getPresenter() {
        return mPresenter;
    }


    @Override
    public void onSuccessPunch() {
        if (mPresenter.getPunchData() != null) {
            btnPunch.setVisibility(View.VISIBLE);
            if (mPresenter.getPunchData().getAttendance() == 1) {
                btnPunch.setText(getString(R.string.punch_out));
                if (!ServiceRunningStatusUtils.isMyServiceRunning(this, UploadCoordinatesService.class)) {
                    startService(UploadCoordinatesService.getIntent(this));
                }
            } else {
                btnPunch.setText(getString(R.string.punch_in));
                if (!ServiceRunningStatusUtils.isMyServiceRunning(this, UploadCoordinatesService.class)) {
                    stopService(UploadCoordinatesService.getIntent(this));
                }
            }
        }
    }
}
