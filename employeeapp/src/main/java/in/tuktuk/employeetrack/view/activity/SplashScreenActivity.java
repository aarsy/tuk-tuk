package in.tuktuk.employeetrack.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import in.tuktuk.usersession.login.view.LoginActivity;

import in.tuktuk.employeetrack.R;
import frameworks.appsession.AppSessionManager;

public class SplashScreenActivity extends AppCompatActivity{

    public static Intent getIntent(Context context) {
        Intent i = new Intent(context, SplashScreenActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        return i;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        AppSessionManager sessionValue;
        sessionValue = new AppSessionManager(this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(sessionValue.getSession()!=null){
                    startHome();
                }else{
                    startLogin();
                }
            }
        }, 2000);
    }

    private void startHome() {
        startActivity(HomePageActivity.getIntent(this));
        finish();
    }


    public void startLogin() {

        startActivityForResult(LoginActivity.getIntentWithoutFlags(this), 100);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==100) {
            if (resultCode == RESULT_OK) {
                startActivity(HomePageActivity.getIntent(this));
            }
        }
        finish();
    }
}
