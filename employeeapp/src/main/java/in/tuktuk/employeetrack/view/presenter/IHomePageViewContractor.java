package in.tuktuk.employeetrack.view.presenter;


import frameworks.basemvp.IPresenter;
import frameworks.basemvp.IView;
import frameworks.punchsession.PunchData;

public interface IHomePageViewContractor {
    interface View extends IView {
        void onSuccessPunch();
    }

    interface Presenter extends IPresenter<View> {

        void getPunchStatus();

        void setPunchStatus();
    }

}
