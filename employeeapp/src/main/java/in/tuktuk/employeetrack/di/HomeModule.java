package in.tuktuk.employeetrack.di;


import in.tuktuk.usersession.userSession.di.UnsafeOkHttpClient;

import dagger.Module;
import dagger.Provides;
import in.tuktuk.employeetrack.data.net.DeliveryAPI;
import in.tuktuk.employeetrack.data.net.DeliveryAPIURL;
import in.tuktuk.employeetrack.data.repository.DeliveryRepository;
import in.tuktuk.employeetrack.data.repository.datasource.DeliveryDataFactory;
import in.tuktuk.employeetrack.domain.IDeliveryRepository;
import frameworks.network.interceptor.AppAuthInterceptor;
import frameworks.network.interceptor.AppBaseInterceptor;
import frameworks.network.interceptor.ErrorResponseInterceptor;
import frameworks.network.model.BaseResponseError;
import frameworks.punchsession.PunchSessionManager;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;

@Module
public class HomeModule {

    @Provides
    Retrofit provideRetrofit(OkHttpClient okHttpClient,
                             Retrofit.Builder retrofitBuilder) {
        return retrofitBuilder.baseUrl(DeliveryAPIURL.BASE_URL).client(okHttpClient).build();
    }

    @Provides
    OkHttpClient provideOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor,AppAuthInterceptor appAuthInterceptor) {
        return UnsafeOkHttpClient.getUnsafeOkHttpClient()
                .addInterceptor(httpLoggingInterceptor)
                .addInterceptor(new AppBaseInterceptor())
                .addInterceptor(appAuthInterceptor)
                .addInterceptor(new ErrorResponseInterceptor(BaseResponseError.class))
                .build();
    }

    @Provides
    @HomeScope
    DeliveryAPI provideDeliveryApi(Retrofit retrofit) {
        return retrofit.create(DeliveryAPI.class);
    }
    @Provides
    @HomeScope
    IDeliveryRepository getWaitingListRepository(DeliveryDataFactory deliveryDataFactory, PunchSessionManager punchSessionManager) {
        return new DeliveryRepository(deliveryDataFactory, punchSessionManager);
    }

}

