package in.tuktuk.employeetrack.di;

import dagger.Component;
import in.tuktuk.employeetrack.view.UploadCoordinatesService;
import in.tuktuk.employeetrack.view.activity.HomePageActivity;
import frameworks.di.component.BaseAppComponent;

@HomeScope
@Component(modules = {HomeModule.class}, dependencies = BaseAppComponent.class)
public interface HomeComponent {
    void inject(HomePageActivity homePageActivity);

    void inject(UploadCoordinatesService uploadCoordinatesService);
}
