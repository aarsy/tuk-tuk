package in.tuktuk.employeetrack.data.net;

import com.softminesol.propertysurvey.CommonBaseUrl;

public interface DeliveryAPIURL {
    String BASE_URL = CommonBaseUrl.BASE_URL;
    String COORDINATES_UPLOAD_API = "tracking/create";
    String ATTENDANCE = "attendance";
    String GET_PUNCH_STATUS = "getAttendance";
}
