package in.tuktuk.employeetrack.data.repository;


import android.text.TextUtils;

import com.google.gson.JsonObject;

import java.util.HashMap;

import javax.inject.Inject;

import in.tuktuk.employeetrack.data.repository.datasource.DeliveryDataFactory;
import in.tuktuk.employeetrack.domain.IDeliveryRepository;
import in.tuktuk.employeetrack.view.model.PunchResponse;
import frameworks.formatter.DateFormatter;
import frameworks.punchsession.PunchData;
import frameworks.punchsession.PunchSessionManager;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;


public class DeliveryRepository implements IDeliveryRepository {
    private final PunchSessionManager punchSessionManager;
    private DeliveryDataFactory deliveryDataFactory;

    @Inject
    public DeliveryRepository(DeliveryDataFactory deliveryDataFactory, PunchSessionManager punchSessionManager) {
        this.deliveryDataFactory = deliveryDataFactory;
        this.punchSessionManager = punchSessionManager;

    }

    @Override
    public Observable<JsonObject> uploadCoordinates(HashMap<String, Object> parameters) {
        return deliveryDataFactory.uploadCoordinates(parameters);
    }

    @Override
    public Observable<PunchResponse> punch(PunchData punchData) {
        return deliveryDataFactory.punch(punchData);
    }

    @Override
    public Observable<PunchData> getPunchStatus() {

        if (punchSessionManager.isPunchAvailable()
                && !TextUtils.isEmpty(punchSessionManager.getPunch().getDate())
                && DateFormatter.compareWithCurrentDate(punchSessionManager.getPunch().getDate())) {
            return Observable.just(punchSessionManager.getPunch());
        } else {
            return deliveryDataFactory.getPunchStatus().map(new Func1<PunchResponse, PunchData>() {
                @Override
                public PunchData call(PunchResponse punchResponse) {
                    PunchData punchData = new PunchData();
                    punchData.setAttendance(punchResponse.getData().getAttendance());
                    punchData.setDate(punchResponse.getData().getDate());
                    return punchData;
                }
            }).doOnNext(savePunchData());
        }
    }

    private Action1<? super PunchData> savePunchData() {
        return new Action1<PunchData>() {
            @Override
            public void call(PunchData punchData) {
                punchSessionManager.savePunch(punchData);
            }
        };

    }
}
