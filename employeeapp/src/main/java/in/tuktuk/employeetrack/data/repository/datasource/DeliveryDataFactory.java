package in.tuktuk.employeetrack.data.repository.datasource;

import com.google.gson.JsonObject;

import java.util.HashMap;

import javax.inject.Inject;

import in.tuktuk.employeetrack.view.model.PunchResponse;
import frameworks.punchsession.PunchData;
import rx.Observable;

public class DeliveryDataFactory {

    DeliveryCloudDataSource deliveryCloudDataSource;

    @Inject
    public DeliveryDataFactory(DeliveryCloudDataSource deliveryCloudDataSource) {
        this.deliveryCloudDataSource = deliveryCloudDataSource;
    }

    public Observable<JsonObject> uploadCoordinates(HashMap<String, Object> parameters) {
        return deliveryCloudDataSource.uploadCoordinates(parameters);
    }

    public Observable<PunchResponse> punch(PunchData punchData) {
        return deliveryCloudDataSource.punch(punchData);
    }

    public Observable<PunchResponse> getPunchStatus() {
        return deliveryCloudDataSource.getPunchStatus();
    }
}
