package in.tuktuk.employeetrack.data.net;

import com.google.gson.JsonObject;

import java.util.HashMap;

import in.tuktuk.employeetrack.view.model.PunchResponse;
import frameworks.punchsession.PunchData;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import rx.Observable;

public interface DeliveryAPI {

    @POST(DeliveryAPIURL.COORDINATES_UPLOAD_API)
    Observable<JsonObject> uploadCoordinates(@Body HashMap<String, Object> parameters);

    @POST(DeliveryAPIURL.ATTENDANCE)
    Observable<PunchResponse> punch(@Body PunchData punchData);

    @GET(DeliveryAPIURL.GET_PUNCH_STATUS)
    Observable<PunchResponse> getPunchStatus();
}
