package in.tuktuk.employeetrack.data.repository.datasource;

import com.google.gson.JsonObject;

import java.util.HashMap;

import javax.inject.Inject;

import in.tuktuk.employeetrack.data.net.DeliveryAPI;
import in.tuktuk.employeetrack.view.model.PunchResponse;
import frameworks.punchsession.PunchData;
import rx.Observable;

public class DeliveryCloudDataSource{
    DeliveryAPI deliveryAPI;

    @Inject
    public DeliveryCloudDataSource(DeliveryAPI deliveryAPI) {
        this.deliveryAPI = deliveryAPI;
    }

    public Observable<JsonObject> uploadCoordinates(HashMap<String, Object> parameters) {
        return deliveryAPI.uploadCoordinates(parameters);
    }

    public Observable<PunchResponse> punch(PunchData punchData) {
        return deliveryAPI.punch(punchData);
    }

    public Observable<PunchResponse> getPunchStatus() {
        return deliveryAPI.getPunchStatus();
    }
}
