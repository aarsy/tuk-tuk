package frameworks.punchsession;

import android.content.Context;

import frameworks.dbhandler.PrefManager;
import frameworks.utils.GsonFactory;

public class PunchSessionManager {
    private static final String KEY_PUNCH_DETAILS= "KEY_PUNCH_DETAILS";
    Context mContext;
    PunchData punchData;

    public PunchSessionManager(Context mContext) {
        this.mContext = mContext;
    }

    public void savePunch(PunchData punchData) {
        this.punchData=punchData;
        PrefManager mPrefManager;
        mPrefManager = new PrefManager(mContext);
        mPrefManager.putString(KEY_PUNCH_DETAILS, GsonFactory.getGson().toJson(punchData));
    }

    public PunchData getPunch() {
        if(punchData==null) {
            PrefManager mPrefManager;
            mPrefManager = new PrefManager(mContext);
            return GsonFactory.getGson().fromJson(mPrefManager.getString(KEY_PUNCH_DETAILS), PunchData.class);
        }else{
            return punchData;
        }
    }

    public void clearPunch() {
        PrefManager mPrefManager;
        mPrefManager = new PrefManager(mContext);
        mPrefManager.putString(KEY_PUNCH_DETAILS, null);

    }

    public boolean isPunchAvailable() {
        if (getPunch() == null) {
            return false;
        }
        return true;
    }
}
