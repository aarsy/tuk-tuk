package frameworks.utils;

import android.content.res.Resources;
import android.util.DisplayMetrics;

public class DimensionUtils {

    public static float convertDpToPixel(float dp) {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return Math.round(px);
    }
}
