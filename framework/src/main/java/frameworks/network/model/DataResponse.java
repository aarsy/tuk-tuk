package frameworks.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class DataResponse<T> {

    @SerializedName(value="data")
    @Expose
    private T data;


    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
