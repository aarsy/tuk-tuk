package frameworks.converter;

public class DistanceConverter {

    public static String getMiles(int meters) {
        return String.format("%.2f", meters * 0.000621371192);
    }

    public static long getMeters(double miles) {
        return Math.round(miles * 1609.344);
    }
}
