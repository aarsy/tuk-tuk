package frameworks.formatter;

import net.danlew.android.joda.JodaTimeAndroid;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateFormatter {


    public static String formatToYesterdayOrTodayOrTomorrow(String date) {
        DateTime dateTime = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").parseDateTime(date);
        DateTime today = new DateTime();
        DateTime yesterday = today.minusDays(1);
        DateTime tomorrow = today.plusDays(1);
        DateTime twoHours = dateTime.plusHours(2);
        DateTimeFormatter timeFormatter = DateTimeFormat.forPattern("hh:mm a");
        DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("MMM dd");

        if (dateTime.toLocalDate().equals(today.toLocalDate())) {
            return "Today, " + timeFormatter.print(dateTime) + " - " + timeFormatter.print(twoHours);
        } else if (dateTime.toLocalDate().equals(yesterday.toLocalDate())) {
            return "Yesterday, " + timeFormatter.print(dateTime) + " - " + timeFormatter.print(twoHours);
        } else if (dateTime.toLocalDate().equals(tomorrow.toLocalDate())) {
            return "Tomorrow, " + timeFormatter.print(dateTime) + " - " + timeFormatter.print(twoHours);
        } else {
            return dateFormatter.print(dateTime) + ", " + timeFormatter.print(dateTime) + " - " + timeFormatter.print(twoHours);
        }
    }


    public static String formatToTwoHrsDifference(String date) {
        DateTime dateTime = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").parseDateTime(date);
        DateTime twoHours = dateTime.plusHours(2);
        DateTimeFormatter timeFormatter = DateTimeFormat.forPattern("hh:mm a");
        return timeFormatter.print(dateTime) + " - " + timeFormatter.print(twoHours);

    }

    public static String formatToTimeDate(String date) {   //Returns value as 3:16 AM / September, 27
        DateTime dateTime = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").parseDateTime(date);
        DateTimeFormatter timeFormatter = DateTimeFormat.forPattern("hh:mm a");
        DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("MMMM, dd");
        return timeFormatter.print(dateTime) + " / " + dateFormatter.print(dateTime);

    }
    public static String formatToTimeDateNew(String date) {   //Returns value as 3:16 AM / September, 27
        DateTime dateTime = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").parseDateTime(date);
        DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("MMM dd, yyyy hh:mm a");
        return dateFormatter.print(dateTime);

    }

    public static boolean compareWithCurrentDate(String date) {
        DateTime dateTime = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSS").parseDateTime(date);
        DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        date=dateFormatter.print(dateTime);
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        String currentDate=simpleDateFormat.format(new Date());
        return currentDate.equals(date);

    }

    public static String getCurrentDateTime(){
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("YYYY:MM:dd hh:mm:ss", Locale.getDefault());
        return simpleDateFormat.format(new Date());
    }
}
