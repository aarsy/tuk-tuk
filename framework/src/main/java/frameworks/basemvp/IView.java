package frameworks.basemvp;

import android.app.ProgressDialog;
import android.content.Intent;

/**
 * Created by sandeep.g9 on 3/7/2017.
 */

public interface IView extends IBaseView {


    ProgressDialog showProgressBar();

    ProgressDialog showProgressBar(String message);

    void hideProgressBar();

    void showToast(String message);

    void showSnackBar(String message);

    void startActivity(Intent intent);

    void startActivityForResult(Intent intent, int requestcode);

    void setResult(int result);

    void setResult(int result, Intent data);


    void finish();

    void addEmptyLayout() ;

}
