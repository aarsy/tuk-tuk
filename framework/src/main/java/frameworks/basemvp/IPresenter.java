package frameworks.basemvp;

import android.content.Intent;

/**
 * Created by sandeep.g9 on 3/7/2017.
 */

public interface IPresenter<T extends IView> {
    void onViewStarted();

    void attachView(T view);

    void onViewCreated();

    boolean onActivityResult(int requestCode, int resultCode, Intent data);

    void detachView();

    T getView();

}
