package frameworks.appsession;

import java.io.Serializable;

/**
 * Created by g1.sandeep on 8/22/2017.
 */

public class UserInfo implements Serializable {
    private int id;
    private int role;
    private String email;
    private String name;
    private String phone_number;
    private String avatar;
    private int rating;

    public int getId() {
        return id;
    }

    public int getRole() {
        return role;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public String getAvatar() {
        return avatar;
    }

    public int getRating() {
        return rating;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return
                "ProfileStatusResponse{" +
                        "id = '" + id + '\'' +
                        ",role = '" + role + '\'' +
                        ",email = '" + email + '\'' +
                        ",name = '" + name + '\'' +
                        ",phone_number = '" + phone_number + '\'' +
                        ",avatar = '" + avatar + '\'' +
                        ",rating = '" + rating + '\'' +
                        "}";
    }

}
