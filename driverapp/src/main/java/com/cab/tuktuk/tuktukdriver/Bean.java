package com.cab.tuktuk.tuktukdriver;

import android.app.Application;
import android.location.Location;
import android.support.multidex.MultiDex;

import com.cab.tuktuk.tuktukdriver.data.net.HomeURL;
import com.cab.tuktuk.tuktukdriver.view.activity.SplashScreenActivity;
import com.crashlytics.android.Crashlytics;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.softminesol.locations.locationmanager.LocationManagerService;
import com.softminesol.propertysurvey.CommonBaseUrl;

import frameworks.AppBaseApplication;
import frameworks.appsession.AppSessionManager;
import frameworks.basemvp.AppBaseActivity;
import frameworks.routers.ILocationRouter;
import frameworks.routers.ILoginInterceptor;
import in.tuktuk.usersession.login.view.LoginActivity;
import in.tuktuk.usersession.registerDriver.data.net.UserRegisterURL;
import in.tuktuk.usersession.userSession.data.net.LoginAPIURL;
import io.fabric.sdk.android.Fabric;

/**
 * Created by USER on 11/30/2017.
 */

public class Bean extends AppBaseApplication implements ILoginInterceptor, ILocationRouter {


    AppSessionManager sessionValue;
    // public String baseURL = "http://gigcabs.com/";
    //public String baseURL = "https://nationproducts.in/ethro-cab/";
    //Zango Cab URL  :    http://nationproducts.in/zango/olddata/

    public static String baseURL = "http://nationproducts.in/tuktukride/olddata/";
    //public String baseURL = "http://www.ethrocab.com/olddata/";
    public String driverId = "";
    public String phone = "";
    public String password = "";
    public String notificationId = "";
    public String image = "";
    public String otp = "";
    public String name = "";
    public String latitude = "";
    public String longitude = "";

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        Fabric.with(this, new Crashlytics());
        sessionValue = new AppSessionManager(this);
        generateAppBaseUrl();
        in.tuktuk.usersession.login.view.LoginActivity.userType=LoginActivity.USER_TYPE_CUSTOMER;
        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(this));

    }


    private void generateAppBaseUrl() {
        LoginAPIURL.BASE_URL = CommonBaseUrl.BASE_URL;
        UserRegisterURL.BASE_URL=CommonBaseUrl.BASE_URL;
        HomeURL.BASE_URL=CommonBaseUrl.BASE_URL;
    }

    @Override
    public void startLogin() {
        mApplication.getBaseContext().startActivity(SplashScreenActivity.getIntent(mApplication.getBaseContext()));
    }

    @Override
    public void logout() {
        sessionValue.clearSession();
        startLogin();
    }


    @Override
    public void onLocationChanged(Location location, AppBaseActivity appBaseActivity) {
        LocationManagerService locationManagerService = LocationManagerService.getInstance();
        locationManagerService.setCurrentLocation(location);
    }
}
