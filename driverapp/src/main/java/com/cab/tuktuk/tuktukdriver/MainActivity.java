package com.cab.tuktuk.tuktukdriver;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.cab.tuktuk.tuktukdriver.di.DaggerHomeComponent;
import com.cab.tuktuk.tuktukdriver.driverStatusPOJO.StatusBean;
import com.cab.tuktuk.tuktukdriver.view.presenter.MainActivityContract;
import com.cab.tuktuk.tuktukdriver.view.presenter.MainActivityPresenter;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.Objects;

import javax.inject.Inject;

import de.hdodenhof.circleimageview.CircleImageView;
import frameworks.basemvp.AppBaseActivity;
import github.nisrulz.easydeviceinfo.base.EasyLocationMod;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class MainActivity extends AppBaseActivity<MainActivityContract.Presenter> implements MainActivityContract.View {
    AHBottomNavigation bottom;
    Toolbar toolbar;
    DrawerLayout drawer;
    TextView refer, training, money, news, care, manage, logout, user_name;
    static SharedPreferences pref;

    SharedPreferences.Editor edit;
    static CircleImageView image;

    @Inject
    MainActivityPresenter mPresenter;

    public static Intent getIntent(Context context){
        return new Intent(context, MainActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter.getUserActivationStatus();
        user_name = (TextView) findViewById(R.id.user_name);
        refer = (TextView) findViewById(R.id.refer);
        training = (TextView) findViewById(R.id.training);
        money = (TextView) findViewById(R.id.money);
        news = (TextView) findViewById(R.id.news);
        care = (TextView) findViewById(R.id.care);
        manage = (TextView) findViewById(R.id.manageProfile);
        logout = (TextView) findViewById(R.id.logout);
        pref = getApplication().getSharedPreferences("pref", MODE_PRIVATE);
        edit = pref.edit();
        image = (CircleImageView) findViewById(R.id.profile);
        user_name.setText(pref.getString("user_name", ""));

        loadImage();


        manage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                FragmentManager fm = getSupportFragmentManager();

                FragmentTransaction fp = fm.beginTransaction();
                while (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportFragmentManager().popBackStackImmediate();
                }
                ProfileFragment fragment = new ProfileFragment();
                fp.replace(R.id.replace, fragment);
                fp.addToBackStack(null);
                fp.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                fp.commit();
                // toolbar.setTitle("Profile");
                // toolbar.setTitleTextColor(Color.BLACK);

                drawer.closeDrawer(GravityCompat.START);


            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                edit.remove("phone");
                edit.remove("password");
                edit.remove("driverId");
                edit.apply();

                Intent i = new Intent(MainActivity.this, LoginActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        });


        bottom = (AHBottomNavigation) findViewById(R.id.bottom_navigation);
        bottom.setDefaultBackgroundColor(Color.parseColor("#222222"));
        bottom.setAccentColor(Color.parseColor("#b1cb31"));
        bottom.setInactiveColor(Color.parseColor("#ffffff"));
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);


        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        // getSupportActionBar().setLogo(R.mipmap.cab_logo);
        // getSupportActionBar().setDisplayUseLogoEnabled(false);

        drawer = (DrawerLayout) findViewById(R.id.drawer);


        refer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction ft = fragmentManager.beginTransaction();
                while (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportFragmentManager().popBackStackImmediate();
                }
                Refer fragment = new Refer();
                ft.replace(R.id.replace, fragment);
                ft.addToBackStack(null);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
                // toolbar.setTitle("Profile");
                // toolbar.setTitleTextColor(Color.BLACK);

                drawer.closeDrawer(GravityCompat.START);

            }
        });

        training.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction ft = fragmentManager.beginTransaction();
                while (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportFragmentManager().popBackStackImmediate();
                }
                Training fragment = new Training();
                ft.replace(R.id.replace, fragment);
                ft.addToBackStack(null);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
                // toolbar.setTitle("Transaction History");
                // toolbar.setTitleTextColor(Color.BLACK);

                drawer.closeDrawer(GravityCompat.START);

            }
        });

        money.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction ft = fragmentManager.beginTransaction();
                while (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportFragmentManager().popBackStackImmediate();
                }
                VoxoxMoney fragment = new VoxoxMoney();
                ft.replace(R.id.replace, fragment);
                ft.addToBackStack(null);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
                // toolbar.setTitle("The Concept");
                //toolbar.setTitleTextColor(Color.BLACK);

                drawer.closeDrawer(GravityCompat.START);

            }
        });

        news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction ft = fragmentManager.beginTransaction();
                while (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportFragmentManager().popBackStackImmediate();
                }
                PartnerNews fragment = new PartnerNews();
                ft.replace(R.id.replace, fragment);
                ft.addToBackStack(null);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
                //toolbar.setTitle("Notification");
                //toolbar.setTitleTextColor(Color.BLACK);

                drawer.closeDrawer(GravityCompat.START);

            }
        });

        care.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction ft = fragmentManager.beginTransaction();
                while (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportFragmentManager().popBackStackImmediate();
                }
                PartnerCare fragment = new PartnerCare();
                ft.replace(R.id.replace, fragment);
                ft.addToBackStack(null);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                ft.commit();
                // toolbar.setTitle("Rate The App");
                //toolbar.setTitleTextColor(Color.BLACK);

                drawer.closeDrawer(GravityCompat.START);

            }
        });


        AHBottomNavigationItem item1 =
                new AHBottomNavigationItem("Duty", R.drawable.deliverysvg);

        AHBottomNavigationItem item2 =
                new AHBottomNavigationItem("Account", R.drawable.calculatorsvg);

        AHBottomNavigationItem item3 =
                new AHBottomNavigationItem("Incentives", R.drawable.coinssvg);

        AHBottomNavigationItem item4 =
                new AHBottomNavigationItem("Performance", R.drawable.medalsvg);

        AHBottomNavigationItem item5 =
                new AHBottomNavigationItem("More", R.drawable.moresvg);

        bottom.addItem(item1);
        bottom.addItem(item2);
        bottom.addItem(item3);
        bottom.addItem(item4);
        bottom.addItem(item5);

        bottom.setBehaviorTranslationEnabled(false);

        bottom.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);

        bottom.setDefaultBackgroundColor(Color.parseColor("#222222"));
        bottom.setAccentColor(Color.parseColor("#b1cb31"));
        bottom.setInactiveColor(Color.parseColor("#ffffff"));
        bottom.setCurrentItem(0);


        bottom.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {

                switch (position) {
                    case 0:

                        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

                        while (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                            getSupportFragmentManager().popBackStackImmediate();
                        }

                        BookRideFragment frag1 = new BookRideFragment();
                        ft.replace(R.id.replace, frag1);
                        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                        ft.commit();

                        return true;
                    case 1:

                        FragmentTransaction ft2 = getSupportFragmentManager().beginTransaction();

                        while (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                            getSupportFragmentManager().popBackStackImmediate();
                        }

                        SupportFragment frag3 = new SupportFragment();
                        ft2.replace(R.id.replace, frag3);
                        //ft2.addToBackStack(null);
                        ft2.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                        ft2.commit();

                        return true;
                    case 2:

                        FragmentTransaction ft1 = getSupportFragmentManager().beginTransaction();

                        while (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                            getSupportFragmentManager().popBackStackImmediate();
                        }

                        //Incentives frag2 = new Incentives();
                        Scheme frag2 = new Scheme();
                        ft1.replace(R.id.replace, frag2);
                        // ft1.addToBackStack(null);
                        ft1.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                        ft1.commit();


                        return true;
                    case 3:


                        FragmentTransaction ft3 = getSupportFragmentManager().beginTransaction();

                        while (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                            getSupportFragmentManager().popBackStackImmediate();
                        }

                        RideHistoryFragment frag4 = new RideHistoryFragment();
                        ft3.replace(R.id.replace, frag4);
                        //ft3.addToBackStack(null);
                        ft3.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                        ft3.commit();

                        return true;
                    case 4:


                        if (drawer.isDrawerOpen(GravityCompat.START)) {
                            drawer.closeDrawer(GravityCompat.START);
                        } else {
                            drawer.openDrawer(GravityCompat.START);
                        }


                        /*FragmentTransaction ft5 = getSupportFragmentManager().beginTransaction();

                        while (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                            getSupportFragmentManager().popBackStackImmediate();
                        }

                        NotificationFragment frag5 = new NotificationFragment();
                        ft5.replace(R.id.replace, frag5);
                        ft5.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                        ft5.commit();*/

                        return false;
                }

                return false;
            }
        });
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft1 = fm.beginTransaction();
        BookRideFragment frag2 = new BookRideFragment();
        ft1.replace(R.id.replace, frag2);
        //ft.addToBackStack(null);
        ft1.commit();
    }

    @Override
    protected void initInjector() {
        DaggerHomeComponent.builder().baseAppComponent(((Bean) getApplication()).getBaseAppComponent()).build().inject(this);
        mPresenter.attachView(this);

    }

    @Override
    public int getViewToCreate() {
        return R.layout.activity_main;
    }

    @Override
    public MainActivityContract.Presenter getPresenter() {
        return mPresenter;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_notification) {

            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            while (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportFragmentManager().popBackStackImmediate();
            }
            NotificationFragment fragment = new NotificationFragment();
            ft.replace(R.id.replace, fragment);
            ft.addToBackStack(null);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
            ft.commit();

        }


        return super.onOptionsItemSelected(item);
    }


    public void offDuty() {
        String id = pref.getString("driverId", "");
        //bar.setVisibility(View.VISIBLE);
        EasyLocationMod easyLocationMod = new EasyLocationMod(MainActivity.this);

        if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        double[] l = easyLocationMod.getLatLong();
        String latii = String.valueOf(l[0]);
        String longi = String.valueOf(l[1]);

        final Bean b = (Bean) this.getApplicationContext();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(b.baseURL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Allapi cr = retrofit.create(Allapi.class);
        Call<StatusBean> call = cr.status(id, "off", latii, longi);
        call.enqueue(new Callback<StatusBean>() {
            @Override
            public void onResponse(Call<StatusBean> call, Response<StatusBean> response) {
                if (Objects.equals(response.body().getStatus(), 1)) {

                    Toast.makeText(MainActivity.this, "Your duty is off ", Toast.LENGTH_SHORT).show();


                } else {
                    Toast.makeText(MainActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<StatusBean> call, Throwable t) {

                //bar.setVisibility(View.GONE);
            }
        });
    }

    public static void loadImage() {

        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisk(true).resetViewBeforeLoading(true).build();

        ImageLoader loader = ImageLoader.getInstance();
        loader.displayImage(pref.getString("image", ""), image, options);
    }


    @Override
    public void onBackPressed() {
        offDuty();
        super.onBackPressed();
    }

    @Override
    public void showDeactivateDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage(getString(R.string.account_pending_for_activation))
                .setNeutralButton("", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).setCancelable(false);
        AlertDialog alertDialog=builder.create();
        alertDialog.show();
    }
}
