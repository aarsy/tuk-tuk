package com.cab.tuktuk.tuktukdriver.data.net;



import com.cab.tuktuk.tuktukdriver.data.model.UserActivationStatusResponse;

import frameworks.network.model.DataResponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import rx.Observable;

public interface HomeAPI {

    @GET(HomeURL.HelperUrl.GET_USER_ACTIVATION_STATUS)
    Observable<Response<DataResponse<UserActivationStatusResponse>>> getUserActivationStatus();

}
