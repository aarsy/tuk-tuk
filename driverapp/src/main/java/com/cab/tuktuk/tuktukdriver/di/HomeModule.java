package com.cab.tuktuk.tuktukdriver.di;

import com.cab.tuktuk.tuktukdriver.data.net.HomeAPI;
import com.cab.tuktuk.tuktukdriver.data.repository.HomeRepository;
import com.cab.tuktuk.tuktukdriver.data.repository.datasource.HomeDataFactory;
import com.cab.tuktuk.tuktukdriver.domain.IHomeRepository;

import dagger.Module;
import dagger.Provides;
import frameworks.network.interceptor.AppAuthInterceptor;
import frameworks.network.interceptor.ErrorResponseInterceptor;
import frameworks.network.model.BaseResponseError;
import in.tuktuk.usersession.registerDriver.data.net.UserRegisterURL;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;

@Module
public class HomeModule {

    @Provides
    Retrofit provideRetrofit(OkHttpClient okHttpClient,
                             Retrofit.Builder retrofitBuilder) {
        return retrofitBuilder.baseUrl(UserRegisterURL.BASE_URL).client(okHttpClient).build();
    }

    @Provides
    OkHttpClient provideOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor, AppAuthInterceptor appAuthInterceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .addInterceptor(appAuthInterceptor)
                .addInterceptor(new ErrorResponseInterceptor(BaseResponseError.class))
                .build();
    }

    @Provides
    @HomeScope
    HomeAPI provideHomeApi(Retrofit retrofit) {
        return retrofit.create(HomeAPI.class);
    }

    @Provides
    @HomeScope
    IHomeRepository getHomeReposiroty(HomeDataFactory homeDataFactory){
        return new HomeRepository(homeDataFactory);
    }
}
