
package com.cab.tuktuk.tuktukdriver.GetImageResult;

import java.io.Serializable;

import com.cab.tuktuk.tuktukdriver.*;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GEtImageRespoance implements Serializable
{

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private com.cab.tuktuk.tuktukdriver.GetImageResult.Data data;
    private final static long serialVersionUID = -7697863145828215207L;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public com.cab.tuktuk.tuktukdriver.GetImageResult.Data getData() {
        return data;
    }

    public void setData(com.cab.tuktuk.tuktukdriver.GetImageResult.Data data) {
        this.data = data;
    }

}
