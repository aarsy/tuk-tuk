package com.cab.tuktuk.tuktukdriver.data.net;


import com.softminesol.propertysurvey.CommonBaseUrl;

public class HomeURL {
    public static String BASE_URL = CommonBaseUrl.BASE_URL;
    public interface HelperUrl {
        String GET_USER_ACTIVATION_STATUS ="getStatus" ;
    }
}
