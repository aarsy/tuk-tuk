package com.cab.tuktuk.tuktukdriver.view;

import android.location.Location;
import android.support.multidex.MultiDex;

import com.cab.tuktuk.tuktukdriver.data.net.HomeURL;
import com.cab.tuktuk.tuktukdriver.view.activity.SplashScreenActivity;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.softminesol.locations.locationmanager.LocationManagerService;
import com.softminesol.propertysurvey.CommonBaseUrl;

import frameworks.AppBaseApplication;
import frameworks.appsession.AppSessionManager;
import frameworks.basemvp.AppBaseActivity;
import frameworks.routers.ILocationRouter;
import frameworks.routers.ILoginInterceptor;
import in.tuktuk.usersession.login.view.LoginActivity;
import in.tuktuk.usersession.registerDriver.data.net.UserRegisterURL;
import in.tuktuk.usersession.userSession.data.net.LoginAPIURL;


public class TuktukDriverApp extends AppBaseApplication implements ILoginInterceptor, ILocationRouter {
    AppSessionManager sessionValue;
    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        //Fabric.with(this, new Crashlytics());
        sessionValue = new AppSessionManager(this);
        generateAppBaseUrl();
        LoginActivity.userType=LoginActivity.USER_TYPE_DRIVER;
        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(this));
    }

    private void generateAppBaseUrl() {
        LoginAPIURL.BASE_URL=CommonBaseUrl.BASE_URL;
        UserRegisterURL.BASE_URL=CommonBaseUrl.BASE_URL;
        HomeURL.BASE_URL=CommonBaseUrl.BASE_URL;
    }

    @Override
    public void startLogin() {
        mApplication.getBaseContext().startActivity(SplashScreenActivity.getIntent(mApplication.getBaseContext()));
    }

    @Override
    public void logout() {
        sessionValue.clearSession();
        startLogin();
    }


    @Override
    public void onLocationChanged(Location location, AppBaseActivity appBaseActivity) {
        LocationManagerService locationManagerService = LocationManagerService.getInstance();
        locationManagerService.setCurrentLocation(location);
    }

}
