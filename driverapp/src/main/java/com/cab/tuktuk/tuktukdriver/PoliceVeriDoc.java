package com.cab.tuktuk.tuktukdriver;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.cab.tuktuk.tuktukdriver.GetImageResult.GEtImageRespoance;
import com.cab.tuktuk.tuktukdriver.RemoveImagePoJO.RemoveImageStatus;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

public class PoliceVeriDoc extends AppCompatActivity {
    ArrayList<String> permissionToRequest;
    ArrayList<String> permissions =new ArrayList<>();
    ArrayList<String> permissionsRejected=new ArrayList<>();
    private final static int ALL_PERMISSIONS_RESULT=101;
    Uri fileUri,fileUri1 = null;
    String picturePathuser;

    ImageView DriverLicenceFront_img,backimage;

    LinearLayout uploadButttonFront,backuploadimage,back;


    ProgressBar progress ;
    String type;
    int AdharCardFrontStatus,AdharCardBackStatus,pan_card_status,driving_licencefront_status,driving_licenceback_status,police_verification_status;

    String id;
    Button submit,remove_frontbutton;
    Bundle bundle;
    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_police_veri_doc);
        permissionToRequest = findUnAskedPermission(permissions);
        permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);

        back=findViewById(R.id.back);
        remove_frontbutton=findViewById(R.id.remove_frontbutton);
        bundle=getIntent().getExtras();

        bundle=getIntent().getExtras();
        id= bundle.getString("id");

        progress=findViewById(R.id.progress);
        backimage=findViewById(R.id.backimage);
        submit=findViewById(R.id.submit);
        progress.setVisibility(View.GONE);
        DriverLicenceFront_img=findViewById(R.id.DriverLicenceFront_img);
        uploadButttonFront=findViewById(R.id.uploadButttonFront);
        backuploadimage=findViewById(R.id.backuploadimage);
        remove_frontbutton.setVisibility(View.GONE);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED ) {

            }
            else
            {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
        }
        else
        {

        }


        remove_frontbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              type="policeverification";
                Removeimage(type);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PoliceVeriDoc.this.finish();
            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PoliceVeriDoc.this.finish();
            }
        });



        uploadButttonFront.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                type ="policeverification";
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (permissionToRequest.size() > 0) {
                        requestPermissions(permissionToRequest.toArray(new String[permissionToRequest.size()]),
                                ALL_PERMISSIONS_RESULT);
                        Log.d(ContentValues.TAG, "Permission requests");

                    }
                }

                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, 2);

            }
        });


        backuploadimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                type ="policeverification ";
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (permissionToRequest.size() > 0) {
                        requestPermissions(permissionToRequest.toArray(new String[permissionToRequest.size()]),
                                ALL_PERMISSIONS_RESULT);
                        Log.d(ContentValues.TAG, "Permission requests");

                    }
                }

                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, 3);

            }
        });
        getimagestatus();
    }


    private ArrayList<String> findUnAskedPermission(ArrayList<String> wanted)
    {
        ArrayList result= new ArrayList();
        for(String perm : wanted)
        {
            if(!hasPermission(perm))
            {
                result.add(perm);
            }
        }
        return result;
    }

    private boolean hasPermission(String perm)
    {
        if(canAskPermission())
        {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            {
                return (getApplicationContext().checkSelfPermission(perm)== PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }


    private boolean canAskPermission()
    {
        return (Build.VERSION.SDK_INT >Build.VERSION_CODES.LOLLIPOP_MR1);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 2 && resultCode == RESULT_OK && null != data) {
            fileUri = data.getData();
            picturePathuser = getPath(getApplicationContext(), fileUri);
            if (picturePathuser != null) {
                if (picturePathuser != "") {
                    Bitmap bitmap = BitmapFactory.decodeFile(picturePathuser);
                    Log.e("bittmap", picturePathuser);
                    DriverLicenceFront_img.setImageBitmap(bitmap);
                    type = "policeverification";
                    uploadImage(type);
                    if (bitmap != null)
                        // profile_image.setImageBitmap(bitmap);
                        try {
                            // frontsidencodedimage = encodeImage(bitmap);
                            // Log.e("bitmapp", String.valueOf(bitmap));
                        }catch (Exception ee)
                        {
                        }
                }
            }

            super.onActivityResult(requestCode, resultCode, data);
        }

        else if (requestCode == 3 && resultCode == RESULT_OK && null != data) {
            fileUri1 = data.getData();
            picturePathuser = getPath(getApplicationContext(), fileUri1);
            if (picturePathuser != null) {
                if (picturePathuser != "") {
                    Bitmap bitmap1 = BitmapFactory.decodeFile(picturePathuser);
                    Log.e("bittmap", picturePathuser);
                    backimage.setImageBitmap(bitmap1);
                    type = "policeverification";
                    uploadImage(type);
                    if (bitmap1 != null)
                        // profile_image.setImageBitmap(bitmap);
                        try {
                            // frontsidencodedimage = encodeImage(bitmap);
                            // Log.e("bitmapp", String.valueOf(bitmap));
                        }catch (Exception ee)
                        {
                        }
                }
            }

            super.onActivityResult(requestCode, resultCode, data);
        }
    }
    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String getPath(final Context context, final Uri uri)
    {

        // check here to KITKAT or new version
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {

            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/"
                            + split[1];
                }
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"),
                        Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};

                return getDataColumn(context, contentUri, selection,
                        selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return "";
    }
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri
                .getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri
                .getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri
                .getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri
                .getAuthority());
    }
    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs)
    {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};

        try {
            cursor = context.getContentResolver().query(uri, projection,
                    selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return "";
    }

    private String encodeImage(Bitmap bm)
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG,100,baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);

        return encImage;
    }



    public void uploadImage(String type)
    {

        progress.setVisibility(View.VISIBLE);
        Allapi apiInterface = ApiClient.getClient().create(Allapi.class);
        File file = new File(picturePathuser);
        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse(getApplicationContext().getContentResolver().getType(fileUri)),
                        file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("image",file.getName(), requestFile);
        RequestBody type1 = RequestBody.create(MediaType.parse("text/plain"),type );
        RequestBody driver_id = RequestBody.create(MediaType.parse("text/plain"),id);

        Log.e("imageeeee", type1+""+driver_id+"");

        Call<GEtImageRespoance> responseBodyCall = apiInterface.DoCUpload(type1,driver_id,body);
        responseBodyCall.enqueue(new Callback<GEtImageRespoance>() {
            @Override
            public void onResponse(Call<GEtImageRespoance> call, retrofit2.Response<GEtImageRespoance> response) {

               //
                // Log.e("imageupload", response.body().toString());

                progress.setVisibility(View.GONE);
                try {

                    progress.setVisibility(View.GONE);
                } catch (Exception e) {



                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<GEtImageRespoance> call, Throwable t) {
                Log.e("imageuploadff", t + "");
                progress.setVisibility(View.GONE);
                // Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void getimagestatus()
    {
        Allapi apiInterface = ApiClient.getClient().create(Allapi.class);
        //File file = new File(picturePathuser);
        // create RequestBody instance from file
      /*  RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse(getApplicationContext().getContentResolver().getType(fileUri)),
                        file
                );*/


        RequestBody driver_id = RequestBody.create(MediaType.parse("text/plain"),id);
        Call<GEtImageRespoance> responseBodyCall = apiInterface.getImage(driver_id);
        responseBodyCall.enqueue(new Callback<GEtImageRespoance>() {
            @Override
            public void onResponse(Call<GEtImageRespoance> call, retrofit2.Response<GEtImageRespoance> response) {

                //Log.e("imageupload", response.body().toString());

                try {


                    AdharCardFrontStatus =  response.body().getData().getAdharCardFrontStatus();
                    AdharCardBackStatus =  response.body().getData().getAdharCardBackStatus();
                    pan_card_status =  response.body().getData().getPanCardStatus();
                    driving_licencefront_status =  response.body().getData().getDrivingLicencefrontStatus();
                    driving_licenceback_status =  response.body().getData().getDrivingLicencebackStatus();
                    police_verification_status =  response.body().getData().getPoliceVerificationStatus();


                  //  Log.e("statuss",AdharCardFrontStatus+" -adf "+AdharCardBackStatus+" -adb "+""+pan_card_status+" -pc "+driving_licencefront_status+" -dlf "+driving_licenceback_status+" -dlb  "+police_verification_status+" -pvs ");

                    if (driving_licenceback_status==1 &&  driving_licencefront_status==1)
                    {

                    }


                    if (police_verification_status==1)
                    {
                        remove_frontbutton.setVisibility(View.VISIBLE);

                    }
  if (police_verification_status!=1)
                    {
                        DriverLicenceFront_img.setBackgroundResource(R.drawable.noimage);

                    }

                    if (pan_card_status==1)
                    {
                        //pancardimageright.setVisibility(View.VISIBLE);
                        //pancardimageright.setBackgroundResource(R.drawable.success);
                    }


                    if (AdharCardFrontStatus==1 &&  AdharCardBackStatus==1)
                    {
                        // adharverifuimagrith.setVisibility(View.VISIBLE);
                        // adharverifuimagrith.setBackgroundResource(R.drawable.success);
                    }








                } catch (Exception e) {



                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<GEtImageRespoance> call, Throwable t) {
                Log.e("imageuploadff", t + "");

                // Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void Removeimage(String type)
    {
        Allapi apiInterface = ApiClient.getClient().create(Allapi.class);
        RequestBody driver_id = RequestBody.create(MediaType.parse("text/plain"),"26");
        RequestBody type1 = RequestBody.create(MediaType.parse("text/plain"),type);
        Call<RemoveImageStatus> responseBodyCall = apiInterface.RemoveImage(type1,driver_id);
        responseBodyCall.enqueue(new Callback<RemoveImageStatus>() {
            @Override
            public void onResponse(Call<RemoveImageStatus> call, retrofit2.Response<RemoveImageStatus> response) {

                //Log.e("imageupload", response.body().toString());

                try {
                   /* AdharCardFrontStatus =  response.body().getData().getAdharCardFrontStatus();
                    AdharCardBackStatus =  response.body().getData().getAdharCardBackStatus();
                    pan_card_status =  response.body().getData().getPanCardStatus();
                    driving_licencefront_status =  response.body().getData().getDrivingLicencefrontStatus();
                    driving_licenceback_status =  response.body().getData().getDrivingLicencebackStatus();
                    police_verification_status =  response.body().getData().getPoliceVerificationStatus();*/


                   // Log.e("statuss",AdharCardFrontStatus+" -adf "+AdharCardBackStatus+" -adb "+""+pan_card_status+" -pc "+driving_licencefront_status+" -dlf "+driving_licenceback_status+" -dlb  "+police_verification_status+" -pvs ");

                    if (driving_licenceback_status==1 &&  driving_licencefront_status==1)
                    {

                    }


                    if (police_verification_status==1)
                    {


                    }

                    if (pan_card_status==1)
                    {
                        //pancardimageright.setVisibility(View.VISIBLE);
                        //pancardimageright.setBackgroundResource(R.drawable.success);
                    }


                    if (AdharCardFrontStatus==1 &&  AdharCardBackStatus==1)
                    {
                        // adharverifuimagrith.setVisibility(View.VISIBLE);
                        // adharverifuimagrith.setBackgroundResource(R.drawable.success);
                    }

                } catch (Exception e) {



                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<RemoveImageStatus> call, Throwable t) {
               // Log.e("imageuploadff", t + "");

                // Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


}
