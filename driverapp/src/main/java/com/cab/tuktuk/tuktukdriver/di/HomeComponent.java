package com.cab.tuktuk.tuktukdriver.di;


import com.cab.tuktuk.tuktukdriver.MainActivity;

import dagger.Component;
import frameworks.di.component.BaseAppComponent;


@HomeScope
@Component(modules = HomeModule.class, dependencies = BaseAppComponent.class)
public interface HomeComponent {
    void inject(MainActivity mainActivity);
}
