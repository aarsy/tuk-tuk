
package com.cab.tuktuk.tuktukdriver.RemoveImagePoJO;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data implements Serializable
{

    @SerializedName("DriverId")
    @Expose
    private String driverId;
    @SerializedName("adhar_card_front_status")
    @Expose
    private Integer adharCardFrontStatus;
    @SerializedName("adhar_card_back_status")
    @Expose
    private Integer adharCardBackStatus;
    @SerializedName("pan_card_status")
    @Expose
    private Integer panCardStatus;
    @SerializedName("driving_licencefront_status")
    @Expose
    private Integer drivingLicencefrontStatus;
    @SerializedName("driving_licenceback_status")
    @Expose
    private Integer drivingLicencebackStatus;
    @SerializedName("police_verification_status")
    @Expose
    private Integer policeVerificationStatus;
    private final static long serialVersionUID = -7114799878882769649L;

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public Integer getAdharCardFrontStatus() {
        return adharCardFrontStatus;
    }

    public void setAdharCardFrontStatus(Integer adharCardFrontStatus) {
        this.adharCardFrontStatus = adharCardFrontStatus;
    }

    public Integer getAdharCardBackStatus() {
        return adharCardBackStatus;
    }

    public void setAdharCardBackStatus(Integer adharCardBackStatus) {
        this.adharCardBackStatus = adharCardBackStatus;
    }

    public Integer getPanCardStatus() {
        return panCardStatus;
    }

    public void setPanCardStatus(Integer panCardStatus) {
        this.panCardStatus = panCardStatus;
    }

    public Integer getDrivingLicencefrontStatus() {
        return drivingLicencefrontStatus;
    }

    public void setDrivingLicencefrontStatus(Integer drivingLicencefrontStatus) {
        this.drivingLicencefrontStatus = drivingLicencefrontStatus;
    }

    public Integer getDrivingLicencebackStatus() {
        return drivingLicencebackStatus;
    }

    public void setDrivingLicencebackStatus(Integer drivingLicencebackStatus) {
        this.drivingLicencebackStatus = drivingLicencebackStatus;
    }

    public Integer getPoliceVerificationStatus() {
        return policeVerificationStatus;
    }

    public void setPoliceVerificationStatus(Integer policeVerificationStatus) {
        this.policeVerificationStatus = policeVerificationStatus;
    }

}
