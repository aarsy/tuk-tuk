package com.cab.tuktuk.tuktukdriver.data.repository.datasource;

import com.cab.tuktuk.tuktukdriver.data.model.UserActivationStatusResponse;
import com.cab.tuktuk.tuktukdriver.data.net.HomeAPI;

import javax.inject.Inject;

import frameworks.network.mapper.DataResponseMapper;
import rx.Observable;

public class HomeCloudDataSource {
    HomeAPI homeAPI;

    @Inject
    public HomeCloudDataSource(HomeAPI homeAPI) {
        this.homeAPI= homeAPI;
    }

    public Observable<UserActivationStatusResponse> getUserActivationStatus() {
        return homeAPI.getUserActivationStatus().map(new DataResponseMapper<UserActivationStatusResponse>());
    }
}
