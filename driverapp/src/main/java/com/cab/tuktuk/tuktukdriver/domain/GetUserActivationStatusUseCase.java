package com.cab.tuktuk.tuktukdriver.domain;

import com.cab.tuktuk.tuktukdriver.data.model.UserActivationStatusResponse;

import javax.inject.Inject;

import frameworks.network.usecases.RequestParams;
import frameworks.network.usecases.UseCase;
import rx.Observable;

public class GetUserActivationStatusUseCase extends UseCase<UserActivationStatusResponse> {

    private final IHomeRepository homeRepository;

    @Inject
    public GetUserActivationStatusUseCase(IHomeRepository homeRepository) {
        this.homeRepository=homeRepository;
    }

    @Override
    public Observable<UserActivationStatusResponse> createObservable(RequestParams requestParams) {
        return homeRepository.getUserActivationStatus();
    }
}
