package com.cab.tuktuk.tuktukdriver.data.model;

import com.google.gson.annotations.SerializedName;

public class UserActivationStatusResponse {
    @SerializedName("UserStatus")
    private UserStatus userStatus;

    public UserStatus getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(UserStatus userStatus) {
        this.userStatus = userStatus;
    }
}
