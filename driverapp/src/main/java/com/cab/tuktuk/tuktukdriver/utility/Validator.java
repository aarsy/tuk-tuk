package com.cab.tuktuk.tuktukdriver.utility;

import android.text.TextUtils;
import android.util.Patterns;

/**
 * Created by sandeepgoyal on 12/05/18.
 */

public class Validator {
    public static boolean validateEmail(String email) {
        return (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches());
    }

    public static boolean validateMobile(String mobile) {
        return Patterns.PHONE.matcher(mobile).matches();
    }
    public static boolean validateName(String name) {
        return name.length()>1;
    }
    public static boolean validatePassword(String password) {
        return password.length()>6;
    }
}
