package com.cab.tuktuk.tuktukdriver.view.presenter;

import android.text.TextUtils;

import com.cab.tuktuk.tuktukdriver.data.model.UserActivationStatusResponse;
import com.cab.tuktuk.tuktukdriver.domain.GetUserActivationStatusUseCase;

import javax.inject.Inject;

import frameworks.basemvp.AppBasePresenter;
import rx.Subscriber;

public class MainActivityPresenter extends AppBasePresenter<MainActivityContract.View> implements MainActivityContract.Presenter {

    private final GetUserActivationStatusUseCase getUserActivationStatusUseCase;

    @Inject
    public MainActivityPresenter(GetUserActivationStatusUseCase getUserActivationStatusUseCase) {
        this.getUserActivationStatusUseCase = getUserActivationStatusUseCase;
    }

    @Override
    public void getUserActivationStatus() {
        getUserActivationStatusUseCase.execute(new Subscriber<UserActivationStatusResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onNext(UserActivationStatusResponse statusResponse) {
                if (statusResponse != null
                        && statusResponse.getUserStatus() != null
                        && "Deactivate".equalsIgnoreCase(statusResponse.getUserStatus().getStatus()))
                    getView().showDeactivateDialog();
            }
        });
    }
}
