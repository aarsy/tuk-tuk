
package com.cab.tuktuk.tuktukdriver.RemoveImagePoJO;

import java.io.Serializable;

import com.cab.tuktuk.tuktukdriver.Data;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RemoveImageStatus implements Serializable
{

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private Data data;
    private final static long serialVersionUID = -6093164837600240773L;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
