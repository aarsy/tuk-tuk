package com.cab.tuktuk.tuktukdriver.view.presenter;

import frameworks.basemvp.IPresenter;
import frameworks.basemvp.IView;

public interface MainActivityContract {

    interface View extends IView{

        void showDeactivateDialog();

    }
    interface Presenter extends IPresenter<View>{

        void getUserActivationStatus();
    }
}
