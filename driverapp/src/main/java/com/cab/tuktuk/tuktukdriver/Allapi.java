package com.cab.tuktuk.tuktukdriver;


import com.cab.tuktuk.tuktukdriver.AchievementsPOJO.AchievementsBean;
import com.cab.tuktuk.tuktukdriver.BookingDetailsPOJO.BookingDetailsBean;
import com.cab.tuktuk.tuktukdriver.CurrentOperatorBillPOJO.CurrentBillBean;
import com.cab.tuktuk.tuktukdriver.EarningByDayPOJO.EarningByDayBean;
import com.cab.tuktuk.tuktukdriver.EarningDetailsByDayPOJO.EarningDetailsBean;
import com.cab.tuktuk.tuktukdriver.GetBookingStatus.BookingStatusBean;
import com.cab.tuktuk.tuktukdriver.GetImageResult.GEtImageRespoance;
import com.cab.tuktuk.tuktukdriver.GetProfilePOJO.GetProfileBean;
import com.cab.tuktuk.tuktukdriver.IncentiveSchemePOJO.IncentiveSchemeBean;
import com.cab.tuktuk.tuktukdriver.IncentivesEarnedPojo.IncentivesEarnedBean;
import com.cab.tuktuk.tuktukdriver.OperatorBillPOJO.OperatorBillBean;
import com.cab.tuktuk.tuktukdriver.RemoveImagePoJO.RemoveImageStatus;
import com.cab.tuktuk.tuktukdriver.ScorePOJO.ScoreBean;
import com.cab.tuktuk.tuktukdriver.StartRidePOJO.StartRideBean;
import com.cab.tuktuk.tuktukdriver.TodaySummaryPOJO.SummaryBean;
import com.cab.tuktuk.tuktukdriver.acceptDenyPOJO.acceptDenyBean;
import com.cab.tuktuk.tuktukdriver.currentBalancePOJO.cuyrrentBalanceBean;
import com.cab.tuktuk.tuktukdriver.driverNotificationPOJO.notificationBean;
import com.cab.tuktuk.tuktukdriver.driverStatusPOJO.StatusBean;
import com.cab.tuktuk.tuktukdriver.finishRidePOJO.finishRideBean;
import com.cab.tuktuk.tuktukdriver.loginPOJO.loginBean;
import com.cab.tuktuk.tuktukdriver.rideStatusPOJO.rideStatusBean;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;

/**
 * Created by USER on 11/30/2017.
 */

public interface Allapi {



    @Multipart
    @POST("api/driver_login.php")
    Call<loginBean> login(
            @Part("phone") String m,
            @Part("password") String n

    );

    @Multipart
    @POST("api/driver_get_notification.php")
    Call<notificationBean> notify(
            @Part("driverId") String m,
            @Part("latitude") String n,
            @Part("longitude") String o

    );


    @Multipart
    @POST("api/update_driver_status.php")
    Call<StatusBean> status(
            @Part("driverId") String m,
            @Part("status") String n,
            @Part("latitude") String a,
            @Part("longitude") String b

    );

    @Multipart
    @POST("api/driver_accept_deny.php")
    Call<acceptDenyBean> accept(
            @Part("driverid") String m,
            @Part("notificationId") String o,
            @Part("status") String n

    );

    @Multipart
    @POST("api/get_driver_status.php")
    Call<rideStatusBean> rideStatus(
            @Part("driverId") String m,
            @Part("bookingId") String o,
            @Part("latitude") String n,
            @Part("longitude") String p

    );


    @Multipart
    @POST("api/driver_start_ride.php")
    Call<StartRideBean> rideStart(
            @Part("driverId") String m,
            @Part("bookingId") String o,
            @Part("latitude") String n,
            @Part("longitude") String p

    );

    @Multipart
    @POST("api/driver_finished_ride.php")
    Call<finishRideBean> rideFinish(
            @Part("driverId") String m,
            @Part("bookingId") String o,
            @Part("latitude") String n,
            @Part("longitude") String p

    );


    @Multipart
    @POST("api/driver_get_profile.php")
    Call<GetProfileBean> profile(
            @Part("driverId") String m
    );

    @Multipart
    @POST("api/incentive-scheme.php")
    Call<IncentiveSchemeBean> scheme(
            @Part("driverId") String m
    );

    @Multipart
    @POST("api/driver_incentive_history.php")
    Call<IncentivesEarnedBean> earned(
            @Part("driverId") String m
    );


    @Multipart
    @POST("api/driver-operator-bill.php")
    Call<OperatorBillBean> operatorBill(
            @Part("driverId") String m
    );

    @Multipart
    @POST("api/driver-operator-billById.php")
    Call<BookingDetailsBean> details(
            @Part("billId") String m
    );

    @Multipart
    @POST("api/driver-earn-list.php")
    Call<EarningByDayBean> earningByDay(
            @Part("driverId") String m
    );

    @Multipart
    @POST("api/driver-earn-byday.php")
    Call<EarningDetailsBean> earningDetails(
            @Part("driverId") String m,
            @Part("dayId") String n
    );


    @Multipart
    @POST("api/driver-achievement.php")
    Call<AchievementsBean> achieve(
            @Part("driverId") String m
    );

    @Multipart
    @POST("api/driver-voxox-score.php")
    Call<ScoreBean> score(
            @Part("driverId") String m
    );

    @Multipart
    @POST("api/driver-today-summary.php")
    Call<SummaryBean> summary(
            @Part("driverId") String m,
            @Part("date") String n
    );


    @Multipart
    @POST("api/driver-current-operator-bill.php")
    Call<CurrentBillBean> current(
            @Part("driverId") String m
    );

    @Multipart
    @POST("api/driver-current-balance.php")
    Call<cuyrrentBalanceBean> currentBalamce(
            @Part("driverId") String m
    );

    @Multipart
    @POST("api/get_booking_status.php")
    Call<BookingStatusBean> getBooking(
            @Part("driverId") String m
    );





    @Multipart
    @POST("api/add_driver.php")
    Call<RespoanceResult> saveDataProfile(
            @Part("name") RequestBody name,
            @Part("email") RequestBody email,
            @Part("mobile") RequestBody mobile,
            @Part("password") RequestBody password,
            @Part("gender") RequestBody gender,
            @Part("address") RequestBody address,
            @Part("city") RequestBody city,
            @Part("state") RequestBody state,
            @Part("country") RequestBody country,
            @Part("pincode") RequestBody pincode,
            @Part("adhar_card_no") RequestBody adhar_card_no,
            @Part("pan_card_no") RequestBody pan_card_no,
            @Part("driving_licence_no") RequestBody driving_licence_no,
            @Part MultipartBody.Part body);


    @Multipart
    @PUT("edit")
    Call<GEtImageRespoance> DoCUpload(
            @Part("type") RequestBody type,
            @Part("driver_id") RequestBody driver_id,
            @Part MultipartBody.Part body);

    @Multipart
    @POST("api/remove_driver_credential_image.php")
    Call<RemoveImageStatus> RemoveImage(
            @Part("type") RequestBody type,
            @Part("driver_id") RequestBody driver_id);


    @Multipart
    @POST("api/get_driver_images_verification.php")
    Call<GEtImageRespoance> getImage(
            @Part("driver_id") RequestBody driver_id);

}
