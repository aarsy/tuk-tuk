package com.cab.tuktuk.tuktukdriver.data.repository;




import com.cab.tuktuk.tuktukdriver.data.model.UserActivationStatusResponse;
import com.cab.tuktuk.tuktukdriver.data.repository.datasource.HomeDataFactory;
import com.cab.tuktuk.tuktukdriver.domain.IHomeRepository;

import javax.inject.Inject;

import rx.Observable;


public class HomeRepository implements IHomeRepository {
    HomeDataFactory homeDataFactory;


    @Inject
    public HomeRepository(HomeDataFactory homeDataFactory) {
        this.homeDataFactory= homeDataFactory;

    }

    @Override
    public Observable<UserActivationStatusResponse> getUserActivationStatus() {
        return homeDataFactory.getDataSource().getUserActivationStatus();
    }
}
