package com.cab.tuktuk.tuktukdriver.domain;


import com.cab.tuktuk.tuktukdriver.data.model.UserActivationStatusResponse;

import rx.Observable;

public interface IHomeRepository {

    Observable<UserActivationStatusResponse> getUserActivationStatus();

}
