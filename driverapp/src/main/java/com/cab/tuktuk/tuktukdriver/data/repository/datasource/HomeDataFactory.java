package com.cab.tuktuk.tuktukdriver.data.repository.datasource;

import javax.inject.Inject;

public class HomeDataFactory {

    HomeCloudDataSource homeCloudDataSource;

    @Inject
    public HomeDataFactory(HomeCloudDataSource homeCloudDataSource) {
        this.homeCloudDataSource = homeCloudDataSource;
    }

    public HomeCloudDataSource getDataSource() {
        return homeCloudDataSource;
    }

}
