package com.cab.ethrocab.ethro;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.ethrocab.ethro.UpdatePhonePOJO.UpdatePhoneBean;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class PhoneUpdate extends AppCompatActivity {

    ProgressBar bar;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    EditText number;
    Button update;
    ConnectionDetector cd;

    Toolbar toolbar;

    TextView referral;

    String refer = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_update);
        number = (EditText) findViewById(R.id.numberUpdate);
        update = (Button) findViewById(R.id.update);
        bar = (ProgressBar) findViewById(R.id.progress);
        cd = new ConnectionDetector(getApplication());


        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.arrow);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });
        toolbar.setTitle("Update Phone");


        referral = (TextView)findViewById(R.id.referral);

        pref = getSharedPreferences("pref", Context.MODE_PRIVATE);
        edit = pref.edit();


        referral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(PhoneUpdate.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(true);
                dialog.setContentView(R.layout.promo_code_popup);
                dialog.show();


                final EditText text = (EditText)dialog.findViewById(R.id.editText);
                Button apply = (Button)dialog.findViewById(R.id.button2);

                text.setText(refer);

                apply.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        String t = text.getText().toString();

                        if (t.length() > 0)
                        {
                            refer = t;
                            referral.setText("Referral Code applied");
                            dialog.dismiss();
                        }
                        else
                        {
                            refer = t;
                            referral.setText("Have a Referral Code?");
                            dialog.dismiss();
                        }


                    }
                });


            }
        });


        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (cd.isConnectingToInternet()){
                    String p = number.getText().toString();

                    if (!TextUtils.isEmpty(p)) {

                        bar.setVisibility(View.VISIBLE);
                        final Bean b = (Bean) getApplicationContext();
                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl(b.baseURL)
                                .addConverterFactory(ScalarsConverterFactory.create())
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();
                        Allapi cr = retrofit.create(Allapi.class);
                        Call<UpdatePhoneBean> call = cr.updateph(b.userId, p , refer);


                        call.enqueue(new Callback<UpdatePhoneBean>() {
                            @Override
                            public void onResponse(Call<UpdatePhoneBean> call, Response<UpdatePhoneBean> response) {
                                if (Objects.equals(response.body().getStatus(), "1")) {

                                    Toast.makeText(PhoneUpdate.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                    b.phone = response.body().getData().getPhone();

                                    Log.d("bhai ka number", response.body().getData().getPhone());

                                    edit.putString("userId", response.body().getData().getUserId());
                                    edit.putString("phone", response.body().getData().getPhone());
                                    edit.apply();

                                    Intent intent = new Intent(PhoneUpdate.this, MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                    bar.setVisibility(View.GONE);


                                } else {
                                    Toast.makeText(PhoneUpdate.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                    bar.setVisibility(View.GONE);
                                    Log.d("bhai ka number", response.body().getData().getPhone());
                                }
                            }

                            @Override
                            public void onFailure(Call<UpdatePhoneBean> call, Throwable t) {
                                bar.setVisibility(View.GONE);
                                Log.d("skfjshdfhsbdfhsbsdb",t.toString());

                            }
                        });


                    } else {
                        number.setError("Field is empty");
                        number.requestFocus();
                    }

                }else {
                    Toast.makeText(PhoneUpdate.this, "No Internet Connection", Toast.LENGTH_SHORT).show();

                }

            }
        });
    }
}
