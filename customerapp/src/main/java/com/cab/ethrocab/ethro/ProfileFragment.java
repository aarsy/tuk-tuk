package com.cab.ethrocab.ethro;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.ethrocab.ethro.UpdatePicPOJO.UpdatePicBean;
import com.cab.ethrocab.ethro.getOwnPromoPOJO.getOwnPromoBean;
import com.facebook.login.LoginManager;
//import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.mvc.imagepicker.ImagePicker;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;
import static com.facebook.FacebookSdk.getApplicationContext;


public class ProfileFragment extends Fragment implements GoogleApiClient.OnConnectionFailedListener {
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

//    TextView resetPass, editProfile, logout, userName, contact, email, editContact, sup , faq;
//    SharedPreferences pref;
//    GoogleApiClient googleApiClient;
//    SharedPreferences.Editor edit;
//    ImageView select;
//    ProgressBar bar;
//    CircleImageView image;
//    private static final int GALLERY_REQUEST = 1;
//    File newFile;
//
//    TextView refer;
//
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        // Inflate the layout for this fragment
//        View view = inflater.inflate(R.layout.fragment_profile, container, false);
//        pref = getContext().getSharedPreferences("pref", MODE_PRIVATE);
//        edit = pref.edit();
//
//        refer = (TextView) view.findViewById(R.id.refer);
//
//        //resetPass = (TextView) view.findViewById(R.id.resetPassword);
//        editProfile = (TextView) view.findViewById(R.id.editProfile);
//        logout = (TextView) view.findViewById(R.id.logout);
//        userName = (TextView) view.findViewById(R.id.userName);
//        contact = (TextView) view.findViewById(R.id.contact);
//        email = (TextView) view.findViewById(R.id.email);
//        select = (ImageView) view.findViewById(R.id.select);
//        bar = (ProgressBar) view.findViewById(R.id.progress);
//        image = (CircleImageView) view.findViewById(R.id.profileImage);
//        editContact = (TextView) view.findViewById(R.id.number);
//        sup = (TextView) view.findViewById(R.id.support);
//        faq = (TextView) view.findViewById(R.id.faq);
//
//
//
//        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisk(true).resetViewBeforeLoading(false).build();
//
//
//
//        ImageLoader loader = ImageLoader.getInstance();
//
//
//        loader.displayImage(pref.getString("image", ""), image , options);
//        userName.setText(pref.getString("name", ""));
//        contact.setText(pref.getString("phone", ""));
//        email.setText(pref.getString("email", ""));
//
//        sup.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent ini = new Intent(getContext(), Support.class);
//                startActivity(ini);
//            }
//        });
//
//        faq.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent in = new Intent(getContext(), FAQs.class);
//                startActivity(in);
//            }
//        });
//
//        editContact.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent in = new Intent(getContext(), PhoneUpdate2.class);
//                startActivity(in);
//            }
//        });
//
//        select.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                selectImage();
//
////                Intent galleryIntent = new Intent(
////                        Intent.ACTION_PICK,
////                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
////                startActivityForResult(galleryIntent, GALLERY_REQUEST);
//
//            }
//        });
//
//
//      /*  userName.setText(pref.getString("name",""));
//        contact.setText(pref.getString("phone",""));
//        email.setText(pref.getString("email",""));*/
//
//
//        refer.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//
//                Dialog dialog = new Dialog(getActivity());
//                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                dialog.setCancelable(true);
//                dialog.setContentView(R.layout.share_referral);
//                dialog.show();
//
//
//                final TextView code = (TextView) dialog.findViewById(R.id.textView4);
//                Button share = (Button) dialog.findViewById(R.id.button3);
//                final ProgressBar progress = (ProgressBar) dialog.findViewById(R.id.progressBar3);
//
//
//                progress.setVisibility(View.VISIBLE);
//
//                final Bean b = (Bean) getApplicationContext();
//                Retrofit retrofit = new Retrofit.Builder()
//                        .baseUrl(b.baseURL)
//                        .addConverterFactory(ScalarsConverterFactory.create())
//                        .addConverterFactory(GsonConverterFactory.create())
//                        .build();
//                Allapi cr = retrofit.create(Allapi.class);
//
//                Call<getOwnPromoBean> call = cr.getOwnPromo(b.userId);
//
//
//                call.enqueue(new Callback<getOwnPromoBean>() {
//                    @Override
//                    public void onResponse(Call<getOwnPromoBean> call, Response<getOwnPromoBean> response) {
//
//                        try {
//                            code.setText(response.body().getData().getCouponCode());
//                        }catch (Exception e)
//                        {
//                            e.printStackTrace();
//                        }
//
//
//
//                        progress.setVisibility(View.GONE);
//                    }
//
//                    @Override
//                    public void onFailure(Call<getOwnPromoBean> call, Throwable t) {
//
//                        progress.setVisibility(View.GONE);
//
//                    }
//                });
//
//
//
//                share.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                        Intent sendIntent = new Intent();
//                        sendIntent.setAction(Intent.ACTION_SEND);
//                        sendIntent.putExtra(Intent.EXTRA_TEXT, code.getText().toString() + ", Apply this Promo Code to get Free Wallet money");
//                        sendIntent.setType("text/plain");
//                        startActivity(sendIntent);
//
//                    }
//                });
//
//
//            }
//        });
//
//
//        buildGoogleApiClient();
//
//
//        logout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                // pref = getContext().getSharedPreferences("pref", MODE_PRIVATE);
//                // edit = pref.edit();
//
//                String ty = pref.getString("Type", "");
//
//                Log.d("asdas", ty);
//
//                if (Objects.equals(pref.getString("Type", ""), "google")) {
//                    Log.d("htyhyh", "jkbkb");
//
//                    if (googleApiClient.isConnected()) {
//                        signOut();
//                    }
//                } else if (Objects.equals(pref.getString("Type", ""), "facebook")) {
//
//                    LoginManager.getInstance().logOut();
//                    edit.remove("phone");
//                    edit.remove("userId");
//                    edit.remove("name");
//                    edit.remove("image");
//                    edit.remove("email");
//                    edit.remove("facebook");
//                    edit.apply();
//                    Intent i = new Intent(getContext(), loginActivity.class);
//                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(i);
//                    // overridePendingTransition(0,0);
//                    getActivity().finish();
//
//                } else {
//                    if (Objects.equals(pref.getString("Type", ""), "signIn")) {
//                        edit.remove("phone");
//                        edit.remove("userId");
//                        edit.remove("name");
//                        edit.remove("image");
//                        edit.remove("email");
//                       /* edit.remove("name");
//                        edit.remove("email");*/
//                        edit.apply();
//                        Intent i = new Intent(getContext(), loginActivity.class);
//                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                        startActivity(i);
//                        getActivity().finish();
//                    }
//                }
//
//
//            }
//        });
//
//
//     /*   resetPass.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent i = new Intent(getContext(), resetPassword.class);
//                startActivity(i);
//            }
//        });
//*/
//        editProfile.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(getContext(), UserNameUpdate.class);
//                startActivity(intent);
//            }
//        });
//
//
//        return view;
//    }
//
//    @Override
//    public void onStart() {
//        super.onStart();
//
//        googleApiClient.connect();
//
//    }
//
//    @Override
//    public void onStop() {
//        super.onStop();
//        if (googleApiClient.isConnected()) {
//            googleApiClient.disconnect();
//        }
//    }
//
//    private void signOut() {
//        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(
//                new ResultCallback<Status>() {
//                    @SuppressLint("CommitPrefEdits")
//                    @Override
//                    public void onResult(@NonNull Status status) {
//
//
//                        if (status.isSuccess()) {
//
//                            edit.remove("phone");
//                            edit.remove("userId");
//                            edit.remove("name");
//                            edit.remove("image");
//                            edit.remove("email");
//                            edit.remove("google");
//                            edit.apply();
//                            Intent i = new Intent(getContext(), loginActivity.class);
//                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                            startActivity(i);
//                            // overridePendingTransition(0,0);
//                            getActivity().finish();
//
//                        }
//
//
//                    }
//                });
//    }
//
//    private synchronized void buildGoogleApiClient() {
//
//        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                .requestEmail()
//                .build();
//
//        googleApiClient = new GoogleApiClient.Builder(getContext())
//                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
//                .build();
//    }
//
//    @Override
//    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
//
//    }
//
////    @Override
////    public void onActivityResult(final int requestCode, final int resultCode, Intent data) {
////
////
////
////        super.onActivityResult(requestCode, resultCode, data);
////
////
////
////
////
////
////
////
////        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
////
////            Uri uri = data.getData();
////
////
////
////            MultipartBody.Part body = null;
////
////
////            String mCurrentPhotoPath = getPath(getContext(), uri);
////
////            File file = new File(mCurrentPhotoPath);
////
////
////            RequestBody reqFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
////
////            body = MultipartBody.Part.createFormData("image", file.getName(), reqFile);
////
////            String id = pref.getString("userId", "");
////
////            bar.setVisibility(View.VISIBLE);
////            final Bean b = (Bean) getApplicationContext();
////            Retrofit retrofit = new Retrofit.Builder()
////                    .baseUrl(b.baseURL)
////                    .addConverterFactory(ScalarsConverterFactory.create())
////                    .addConverterFactory(GsonConverterFactory.create())
////                    .build();
////            Allapi cr = retrofit.create(Allapi.class);
////            Call<UpdatePicBean> call = cr.pic(id, body);
////            Log.d("idhai yeh", id);
////            call.enqueue(new Callback<UpdatePicBean>() {
////                @Override
////                public void onResponse(Call<UpdatePicBean> call, Response<UpdatePicBean> response) {
////                    if (Objects.equals(response.body().getStatus(), "1")) {
////                        Log.d("success hai", "sdfsfhdhfduhf");
////                        Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
////                        bar.setVisibility(View.GONE);
////
////                        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
////                                .cacheOnDisk(true).resetViewBeforeLoading(false).build();
////
////                        ImageLoader loader = ImageLoader.getInstance();
////
////
////
////                        loader.displayImage(response.body().getData().getImage(), image, options);
////                        Log.d("Image Url", response.body().getData().getImage());
////                        edit.putString("image", response.body().getData().getImage());
////                        edit.apply();
////
////
////                    } else {
////                        Log.d("else wala", "kjgdgd");
////                        Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
////                        bar.setVisibility(View.GONE);
////                    }
////                }
////
////                @Override
////                public void onFailure(Call<UpdatePicBean> call, Throwable t) {
////                    Log.d("fail ho gyi", t.toString());
////                    bar.setVisibility(View.GONE);
////                }
////            });
////
////
////        }
//
//
//
//
//    private static String getPath(final Context context, final Uri uri) {
//        final boolean isKitKatOrAbove = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
//
//        // DocumentProvider
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            if (isKitKatOrAbove && DocumentsContract.isDocumentUri(context, uri)) {
//                // ExternalStorageProvider
//                if (isExternalStorageDocument(uri)) {
//                    final String docId = DocumentsContract.getDocumentId(uri);
//                    final String[] split = docId.split(":");
//                    final String type = split[0];
//
//                    if ("primary".equalsIgnoreCase(type)) {
//                        return Environment.getExternalStorageDirectory() + "/" + split[1];
//                    }
//
//                    // TODO handle non-primary volumes
//                }
//                // DownloadsProvider
//                else if (isDownloadsDocument(uri)) {
//
//                    final String id = DocumentsContract.getDocumentId(uri);
//                    final Uri contentUri = ContentUris.withAppendedId(
//                            Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
//
//                    return getDataColumn(context, contentUri, null, null);
//                }
//                // MediaProvider
//                else if (isMediaDocument(uri)) {
//                    final String docId = DocumentsContract.getDocumentId(uri);
//                    final String[] split = docId.split(":");
//                    final String type = split[0];
//
//                    Uri contentUri = null;
//                    if ("image".equals(type)) {
//                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
//                    } else if ("video".equals(type)) {
//                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
//                    } else if ("audio".equals(type)) {
//                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
//                    }
//
//                    final String selection = "_id=?";
//                    final String[] selectionArgs = new String[]{
//                            split[1]
//                    };
//
//                    return getDataColumn(context, contentUri, selection, selectionArgs);
//                }
//            }
//            // MediaStore (and general)
//            else if ("content".equalsIgnoreCase(uri.getScheme())) {
//                return getDataColumn(context, uri, null, null);
//            }
//            // File
//            else if ("file".equalsIgnoreCase(uri.getScheme())) {
//                return uri.getPath();
//            }
//        }
//
//        return null;
//    }
//
//    private static boolean isExternalStorageDocument(Uri uri) {
//        return "com.android.externalstorage.documents".equals(uri.getAuthority());
//    }
//
//    /**
//     * @param uri The Uri to check.
//     * @return Whether the Uri authority is DownloadsProvider.
//     */
//    private static boolean isDownloadsDocument(Uri uri) {
//        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
//    }
//
//    /**
//     * @param uri The Uri to check.
//     * @return Whether the Uri authority is MediaProvider.
//     */
//    private static boolean isMediaDocument(Uri uri) {
//        return "com.android.providers.media.documents".equals(uri.getAuthority());
//    }
//
//    private static String getDataColumn(Context context, Uri uri, String selection,
//                                        String[] selectionArgs) {
//
//        Cursor cursor = null;
//        final String column = "_data";
//        final String[] projection = {
//                column
//        };
//
//        try {
//            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
//                    null);
//            if (cursor != null && cursor.moveToFirst()) {
//                final int column_index = cursor.getColumnIndexOrThrow(column);
//                return cursor.getString(column_index);
//            }
//        } finally {
//            if (cursor != null)
//                cursor.close();
//        }
//        return null;
//    }
//
//    File f;
//
//    private void selectImage() {
//
//
//        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
//
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//
//        builder.setTitle("Add Photo!");
//
//        builder.setItems(options, new DialogInterface.OnClickListener() {
//
//            @Override
//
//            public void onClick(DialogInterface dialog, int item) {
//
//                if (options[item].equals("Take Photo"))
//
//                {
//
//                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//
//                    f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
//
//                    intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(getContext(), getContext().getApplicationContext().getPackageName() + ".my.package.name.provider", f) );
//                    //pic = f;
////Uri.fromFile(f)
//                    startActivityForResult(intent, 1);
//
//
//                } else if (options[item].equals("Choose from Gallery"))
//
//                {
//
//                    Intent galleryIntent = new Intent(
//                            Intent.ACTION_PICK,
//                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//
//                    startActivityForResult(galleryIntent, 2);
//
//
//                } else if (options[item].equals("Cancel")) {
//
//                    dialog.dismiss();
//
//                }
//
//            }
//
//        });
//
//        builder.show();
//
//    }
//
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//
//        super.onActivityResult(requestCode, resultCode, data);
//
//        if (resultCode == RESULT_OK) {
//
//            if (requestCode == 1) {
//                MultipartBody.Part body = null;
//
//
//                    OutputStream outFile = null;
//
//                    RequestBody reqFile = RequestBody.create(MediaType.parse("multipart/form-data"), f);
//
//                    body = MultipartBody.Part.createFormData("image", f.getName(), reqFile);
//
//                    String id = pref.getString("userId", "");
//
//                    bar.setVisibility(View.VISIBLE);
//                    final Bean b = (Bean) getApplicationContext();
//                    Retrofit retrofit = new Retrofit.Builder()
//                            .baseUrl(b.baseURL)
//                            .addConverterFactory(ScalarsConverterFactory.create())
//                            .addConverterFactory(GsonConverterFactory.create())
//                            .build();
//                    Allapi cr = retrofit.create(Allapi.class);
//                    Call<UpdatePicBean> call = cr.pic(id, body);
//                    Log.e("idhai yeh", "Error");
//                    Log.d("idhai yeh", id);
//                    call.enqueue(new Callback<UpdatePicBean>() {
//                        @Override
//                        public void onResponse(Call<UpdatePicBean> call, Response<UpdatePicBean> response) {
//                            if (Objects.equals(response.body().getStatus(), "1")) {
//                                Log.e("RESPONSE","Success");
//                                Log.e("success hai", "sdfsfhdhfduhf");
//                                Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
//                                bar.setVisibility(View.GONE);
//
//                                DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
//                                        .cacheOnDisk(true).resetViewBeforeLoading(false).build();
//
//                                ImageLoader loader = ImageLoader.getInstance();
//
//
//
//                                loader.displayImage(response.body().getData().getImage(), image, options);
//                                Log.e("Image Url", response.body().getData().getImage());
//                                edit.putString("image", response.body().getData().getImage());
//                                edit.apply();
//
//
//                            } else {
//                                Log.d("else wala", "kjgdgd");
//                                Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
//                                bar.setVisibility(View.GONE);
//                            }
//                        }
//
//                        @Override
//                        public void onFailure(Call<UpdatePicBean> call, Throwable t) {
//                            Log.e("RESPONSE","Success"+t);
//                            Log.d("fail ho gyi", t.toString());
//
//                            bar.setVisibility(View.GONE);
//                        }
//                    });
//
//
//
//
//
//
//
//            } else if (requestCode == 2) {
//                Uri uri = data.getData();
//
//
//
//                MultipartBody.Part body = null;
//
//
//                String mCurrentPhotoPath = getPath(getContext(), uri);
//
//                File file = new File(mCurrentPhotoPath);
//
//                Log.e("File"," "+file);
//                RequestBody reqFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
//
//                body = MultipartBody.Part.createFormData("image", file.getName(), reqFile);
//
//                String id = pref.getString("userId", "");
//
//                bar.setVisibility(View.VISIBLE);
//                final Bean b = (Bean) getApplicationContext();
//                Retrofit retrofit = new Retrofit.Builder()
//                        .baseUrl(b.baseURL)
//                        .addConverterFactory(ScalarsConverterFactory.create())
//                        .addConverterFactory(GsonConverterFactory.create())
//                        .build();
//                Allapi cr = retrofit.create(Allapi.class);
//                Call<UpdatePicBean> call = cr.pic(id, body);
//                Log.d("idhai yeh", id);
//                call.enqueue(new Callback<UpdatePicBean>() {
//                    @Override
//                    public void onResponse(Call<UpdatePicBean> call, Response<UpdatePicBean> response) {
//                        if (Objects.equals(response.body().getStatus(), "1")) {
//                            Log.d("success hai", "sdfsfhdhfduhf");
//                            Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
//                            bar.setVisibility(View.GONE);
//
//                            DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
//                                    .cacheOnDisk(true).resetViewBeforeLoading(false).build();
//
//                            ImageLoader loader = ImageLoader.getInstance();
//
//
//
//                            loader.displayImage(response.body().getData().getImage(), image, options);
//                            Log.d("Image Url", response.body().getData().getImage());
//                            edit.putString("image", response.body().getData().getImage());
//                            edit.apply();
//
//
//                        } else {
//                            Log.d("else wala", "kjgdgd");
//                            Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
//                            bar.setVisibility(View.GONE);
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<UpdatePicBean> call, Throwable t) {
//                        Log.d("fail ho gyi", t.toString());
//                        bar.setVisibility(View.GONE);
//                    }
//                });
//
//
//            }
//
//                //    linear.removeAllViews();
//
//
//
//
//
//
//            }
//        }
    }

