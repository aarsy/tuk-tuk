package com.cab.ethrocab.ethro.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;


import com.cab.ethrocab.ethro.MainActivity;
import com.cab.ethrocab.ethro.R;

import frameworks.appsession.AppSessionManager;
import in.tuktuk.usersession.login.view.CustomerLoginActivity;
import in.tuktuk.usersession.login.view.LoginActivity;

public class SplashScreenActivity extends AppCompatActivity{

    public static Intent getIntent(Context context) {
        Intent i = new Intent(context, SplashScreenActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        return i;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        final AppSessionManager sessionValue;
        sessionValue = new AppSessionManager(this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(sessionValue.getSession()!=null){
                    startHome();
                }else{
                    startLogin();
                }
            }
        }, 2000);
    }

    private void startHome() {
        startActivity(MainActivity.getIntent(this));
        finish();
    }


    public void startLogin() {

        startActivityForResult(CustomerLoginActivity.getIntentWithoutFlags(this), 100);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==100) {
            if (resultCode == RESULT_OK) {
                startActivity(MainActivity.getIntent(this));
            }
        }
        finish();
    }
}
