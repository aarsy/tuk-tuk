package com.cab.ethrocab.ethro;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cab.ethrocab.ethro.rideHistoryPOJO.History;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by faizan on 12/13/2017.
 */

public class historyCardAdapter extends RecyclerView.Adapter<historyCardAdapter.MyViewHolder> {

    Context context;
    List<History> list = new ArrayList<>();


    public historyCardAdapter(Context context, List<History>list)
    {

        this.context = context;
        this.list = list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(context).inflate(R.layout.historycard , parent , false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        final History item = list.get(position);
        holder.date.setText(item.getDate());
        holder.fare.setText(item.getPrice());
        holder.rideType.setText(item.getCabTypeName());
        holder.carNumber.setText(item.getCabNumber());
        if(item.getBookingStatus().equalsIgnoreCase("0")) {
            holder.status.setText("Cancelled");
            holder.status.setTextColor(Color.RED);
        }
        if(item.getBookingStatus().equalsIgnoreCase("1")) {
            holder.status.setText("Done");
            holder.status.setTextColor(Color.GREEN);
        }
        if(item.getBookingStatus().equalsIgnoreCase("2")) {
            holder.status.setText("Ongoing");
            holder.status.setTextColor(Color.BLUE);
        }
        if(item.getBookingStatus().equalsIgnoreCase("3")) {
            holder.status.setText("Started");
            holder.status.setTextColor(Color.CYAN);
        }
        String pickLat = item.getPickupLat();
        String pickLng = item.getPickupLong();
        String dropLat = item.getDropLat();
        String dropLng = item.getDropLong();

        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> listAdresses = geocoder.getFromLocation(Double.parseDouble(pickLat), Double.parseDouble(pickLng), 1);
            if (null != listAdresses && listAdresses.size() > 0) {
                String address = listAdresses.get(0).getAddressLine(0);
                holder.pickupLocation.setText(address.toString());
                Log.d("addre", address.toString());
            }


        }catch (Exception e){
            e.printStackTrace();
        }

        try {
            List<Address> listAdresses2 = geocoder.getFromLocation(Double.parseDouble(dropLat), Double.parseDouble(dropLng), 1);
            if (null != listAdresses2 && listAdresses2.size() > 0) {
                String address = listAdresses2.get(0).getAddressLine(0);
                holder.dropLocation.setText(address.toString());
                Log.d("addre", address.toString());
            }


        }catch (Exception e){
            e.printStackTrace();
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.final_dialog);
                dialog.setCancelable(true);
                dialog.show();
                ImageView image = (ImageView) dialog.findViewById(R.id.profileImage);
                TextView rate = (TextView) dialog.findViewById(R.id.rate);
                TextView name = (TextView) dialog.findViewById(R.id.name);
                TextView num = (TextView) dialog.findViewById(R.id.number);
                TextView ser1 = (TextView) dialog.findViewById(R.id.searchBar1);
                TextView ser2 = (TextView) dialog.findViewById(R.id.searchBar2);
                TextView price = (TextView) dialog.findViewById(R.id.price);
                num.setText(item.getCabNumber());
                price.setText(item.getPrice());
                rate.setText(item.getDriverRate());
                name.setText(item.getDriverName());
                ser1.setText(holder.pickupLocation.getText().toString());
                ser2.setText(holder.dropLocation.getText().toString());

                ImageLoader loader = ImageLoader.getInstance();
                loader.displayImage(item.getImage(),image);
                Log.d("Image", item.getImage());
            }
        });
    }

    public void setgrid( List<History>list){


        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView date, pickupLocation, dropLocation, fare, rideType, carNumber, status;

        public MyViewHolder(View itemView) {
            super(itemView);



            date = (TextView) itemView.findViewById(R.id.date);
            pickupLocation = (TextView) itemView.findViewById(R.id.pickupLocation);
            dropLocation = (TextView) itemView.findViewById(R.id.dropLocation);
            fare = (TextView) itemView.findViewById(R.id.fare);
            rideType = (TextView) itemView.findViewById(R.id.rideType);
            carNumber = (TextView) itemView.findViewById(R.id.carNumber);
            status = (TextView) itemView.findViewById(R.id.status);

        }
    }
}
