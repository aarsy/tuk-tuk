package com.cab.ethrocab.ethro;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by sahil.kashyap on 2/21/2017.
 */

public class ResponseActivity extends Activity {
    LinearLayout layResponse;
    TextView ag_id, me_id, order_no, amount, country, currency, date, time, ag_ref, pg_ref, status, resp_code, resp_mesg;
    TextView pg_id, bankname, paymode, emi;
    String[] allResp;
    String[] txnsResp;
    String msg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_response);
        Intent intent = getIntent();
        String response = intent.getStringExtra("RESPONSE");
        String responsePG = intent.getStringExtra("PGRESPONSE");
        msg = intent.getStringExtra("MESSAGE");


        allResp = response.split("\\|");
        txnsResp = responsePG.split("\\|");


        initialize();

    }

    private void initialize() {
        // layResponse = (LinearLayout) findViewById(R.id.lay_response);

        //Transaction Details
        ag_id = (TextView) findViewById(R.id.ag_id);
        me_id = (TextView) findViewById(R.id.me_id);
        order_no = (TextView) findViewById(R.id.order_id);
        amount = (TextView) findViewById(R.id.amount);
        country = (TextView) findViewById(R.id.country);
        currency = (TextView) findViewById(R.id.currency);
        date = (TextView) findViewById(R.id.date);
        time = (TextView) findViewById(R.id.time);
        ag_ref = (TextView) findViewById(R.id.ag_ref);
        pg_ref = (TextView) findViewById(R.id.pg_ref);
        status = (TextView) findViewById(R.id.status);
        resp_code = (TextView) findViewById(R.id.status_code);
        resp_mesg = (TextView) findViewById(R.id.message);

        if (allResp.length > 2) {
            ag_id.setText(allResp[0]);
            me_id.setText(allResp[1]);
            order_no.setText(allResp[2]);
            amount.setText(allResp[3]);
            country.setText(allResp[4]);
            currency.setText(allResp[5]);
            date.setText(allResp[6]);
            time.setText(allResp[7]);
            ag_ref.setText(allResp[8]);
            pg_ref.setText(allResp[9]);
            status.setText(allResp[10]);
            resp_code.setText(allResp[11]);
            resp_mesg.setText(allResp[12]);
        } else {
            ag_id.setText("");
            me_id.setText("");
            order_no.setText("");
            amount.setText("");
            country.setText("");
            currency.setText("");
            date.setText("");
            time.setText("");
            ag_ref.setText("");
            pg_ref.setText("");
            status.setText("");
            resp_code.setText("");
            resp_mesg.setText("");
        }


        //PG DETAILS
//        pg_id = (TextView) findViewById(R.id.pg_id);
//        bankname = (TextView) findViewById(R.id.bank_name);
//        paymode = (TextView) findViewById(R.id.mode);
//        emi = (TextView) findViewById(R.id.emi);
//
//        pg_id.setText(txnsResp[0]);
//        bankname.setText(txnsResp[1]);
//        paymode.setText(txnsResp[2]);
        //emi.setText(txnsResp[3]);


    }
}
