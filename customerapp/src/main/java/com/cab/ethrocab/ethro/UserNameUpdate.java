package com.cab.ethrocab.ethro;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.cab.ethrocab.ethro.UpdateNamePOJO.UpdateNameBean;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class UserNameUpdate extends AppCompatActivity {

    ProgressBar bar;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    EditText name;
    Button update;
    ConnectionDetector cd;

    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_name_update);

        name = (EditText) findViewById(R.id.nameUpdate);
        update = (Button) findViewById(R.id.update);
        bar = (ProgressBar) findViewById(R.id.progress);
        cd = new ConnectionDetector(getApplication());

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.arrow);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });
        toolbar.setTitle("Update Username");


        pref = getSharedPreferences("pref", Context.MODE_PRIVATE);
        edit = pref.edit();


        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (cd.isConnectingToInternet()) {
                    String p = name.getText().toString();
                    if (!TextUtils.isEmpty(p)) {

                        bar.setVisibility(View.VISIBLE);
                        final Bean b = (Bean) getApplicationContext();
                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl(b.baseURL)
                                .addConverterFactory(ScalarsConverterFactory.create())
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();
                        Allapi cr = retrofit.create(Allapi.class);
                        Call<UpdateNameBean> call = cr.update(b.userId, p);
                        Log.d("userIDBhai ki", b.userId);
                        call.enqueue(new Callback<UpdateNameBean>() {
                            @Override
                            public void onResponse(Call<UpdateNameBean> call, Response<UpdateNameBean> response) {

                                if (Objects.equals(response.body().getStatus(), "1")) {

                                    Toast.makeText(UserNameUpdate.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                    edit.putString("userId", response.body().getData().getUserId());
                                    edit.putString("name", response.body().getData().getName());
                                    edit.putString("phone", response.body().getData().getPhone());
                                    edit.putString("email", response.body().getData().getEmail());
                                    Log.d("bhai ka name", response.body().getData().getName());
                                    Log.d("bhai ka number", response.body().getData().getPhone());
                                    Log.d("bhai ka email", response.body().getData().getEmail());
                                    edit.apply();
                                    b.name = response.body().getData().getName();
                                    bar.setVisibility(View.GONE);

                                    Intent i = new Intent(UserNameUpdate.this, MainActivity.class);
                                    startActivity(i);
                                    finish();

                                } else {
                                    Toast.makeText(UserNameUpdate.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                    bar.setVisibility(View.GONE);
                                }
                            }

                            @Override
                            public void onFailure(Call<UpdateNameBean> call, Throwable t) {
                                bar.setVisibility(View.GONE);

                            }
                        });

                    } else {
                        name.setError("Feild is Empty");
                        name.requestFocus();
                    }
                } else {
                    Toast.makeText(UserNameUpdate.this, "No Internet Connection", Toast.LENGTH_SHORT).show();

                }


            }
        });
    }
}
