package com.cab.ethrocab.ethro;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import java.util.Random;

public class AvantgardeSdkUsageActivity extends Activity {


    Button bt_payment;
    String order_no;
    String cost;
    private String[] countries = {"IND", "USA", "UKA"};
    private String[] currency = {"Indian Rupee", "US Dollar", "British Pound"};
    private String[] currnecy_code = {"INR", "USD", "GBR"};
    private String[] pay_mode = {"Select Paymode", "Net Banking",
            "Credit Card", "Debit Card", "Prepaid Card", "Wallet",
            "Credit Card Emi"};
    private String[] expiry_month = {"Select Month", "1", "2", "3", "4", "5",
            "6", "7", "8", "9", "10", "11", "12"};
    private String[] expiry_year = {"Select Year", "2016", "2017", "2018",
            "2019", "2020"};
    private String[] user_logged = {"Select Option", "yes", "no"};
    private String dataDecrypted;
    SharedPreferences sharedpreferences;

    public int gen() {
        Random r = new Random(System.currentTimeMillis());
        return ((1 + r.nextInt(2)) * 10000 + r.nextInt(10000));
    }

    Spinner country_spinner, currecy_spinner, paymode_spinner,
            expirymonth_spinner, expiry_year_spinner, user_logged_spinner;
    EditText edit_ordernumber,  edit_transaction_type,
            edit_success_url, edit_fialure_url, edit_channel;
    TextView edit_amount;
    EditText edit_payment_gateway_id, edit_schem, edit_emi_months;
    EditText edit_card_no, edit_cvv, edit_card_name;
    EditText edit_UDF1, edit_UDF2, edit_UDF3, edit_UDF4, edit_UDF5;
    EditText edit_bill_address, edit_bill_state, edit_bill_zip, edit_bill_city, edit_bill_country;
    EditText edit_ship_address, edit_ship_state, edit_ship_zip, edit_address_count, edit_ship_city, edit_ship_country, edit_ship_days;
    EditText edit_itemcount, edit_item_category, edit_item_value;
    EditText edit_cust_name, edit_mobile_number, edit_emailid, edit_uniqueid;

    String country_code = "", currecy_type = "", paymode = "", expMonth = "", expYear = "", isuserlogged = "";
    String me_id = "201809010001";
    //String me_id = "201809290001";//Paygate India Pvt LTd
    
    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        order_no = "" + gen();
        edit_ordernumber.setText(order_no);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.avantgardesdkusagescreen);

        order_no = "" + gen();
        edit_ordernumber = (EditText) findViewById(R.id.edit_Ordere_Number);
        edit_amount = (TextView) findViewById(R.id.edit_amount);
        edit_transaction_type = (EditText) findViewById(R.id.edit_Transaction);
        edit_success_url = (EditText) findViewById(R.id.edit_Sucess_Url);
        edit_fialure_url = (EditText) findViewById(R.id.edit_Failure);
        edit_channel = (EditText) findViewById(R.id.edit_channel);

        edit_payment_gateway_id = (EditText) findViewById(R.id.edit_paymentgateway_id);
        edit_payment_gateway_id.setText("123");
        edit_schem = (EditText) findViewById(R.id.edit_scheme);
        edit_emi_months = (EditText) findViewById(R.id.edit_emi_months);

        //card details
        edit_card_no = (EditText) findViewById(R.id.edit_card_no);
        edit_cvv = (EditText) findViewById(R.id.edit_cvv);
        edit_card_name = (EditText) findViewById(R.id.edit_card_name);


        //Customer Details
        edit_cust_name = (EditText) findViewById(R.id.edit_ship_address);
        edit_emailid = (EditText) findViewById(R.id.edit_emailid);
        edit_uniqueid = (EditText) findViewById(R.id.edit_uniqueid);
        edit_mobile_number = (EditText) findViewById(R.id.edit_mobile_number);


        //Shipping Details
        edit_ship_address = (EditText) findViewById(R.id.edit_ship_address);
        edit_ship_state = (EditText) findViewById(R.id.edit_ship_state);
        edit_ship_zip = (EditText) findViewById(R.id.edit_ship_zip);
        edit_address_count = (EditText) findViewById(R.id.edit_address_count);
        edit_ship_city = (EditText) findViewById(R.id.edit_ship_city);
        edit_ship_country = (EditText) findViewById(R.id.edit_ship_country);
        edit_ship_days = (EditText) findViewById(R.id.edit_ship_days);

        //Billing Details
        edit_bill_address = (EditText) findViewById(R.id.edit_bill_address);
        edit_bill_state = (EditText) findViewById(R.id.edit_bill_state);
        edit_bill_zip = (EditText) findViewById(R.id.edit_bill_zip);
        edit_bill_city = (EditText) findViewById(R.id.edit_bill_city);
        edit_bill_country = (EditText) findViewById(R.id.edit_bill_country);

        //Item Details
        edit_itemcount = (EditText) findViewById(R.id.edit_itemcount);
        edit_item_category = (EditText) findViewById(R.id.edit_item_category);
        edit_item_value = (EditText) findViewById(R.id.edit_item_value);


        //Other details
        edit_UDF1 = (EditText) findViewById(R.id.edit_UDF1);
        edit_UDF2 = (EditText) findViewById(R.id.edit_UDF2);
        edit_UDF3 = (EditText) findViewById(R.id.edit_UDF3);
        edit_UDF4 = (EditText) findViewById(R.id.edit_UDF4);
        edit_UDF5 = (EditText) findViewById(R.id.edit_UDF5);

        edit_ordernumber.setText(order_no);
        edit_amount.setText(""+getIntent().getStringExtra("totalprice"));
        //edit_amount.setText("1.0");
        edit_transaction_type.setText("SALE");
        edit_fialure_url.setText("https://test.avantgardepayments.com/prod-transaction/MerchantFailure.php");
        edit_success_url.setText("https://test.avantgardepayments.com/prod-transaction/MerchantSuccess.php");
        edit_channel.setText("MOBILE");

        currecy_spinner = (Spinner) findViewById(R.id.currency_spinner);
        country_spinner = (Spinner) findViewById(R.id.class_room_spinner);
        paymode_spinner = (Spinner) findViewById(R.id.paymode_spinner);
        expirymonth_spinner = (Spinner) findViewById(R.id.expirymonth_spinner);
        expiry_year_spinner = (Spinner) findViewById(R.id.expiry_year_spinner);
        user_logged_spinner = (Spinner) findViewById(R.id.user_logged_spinner);


        ArrayAdapter<String> adapter_state = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, countries);
        country_spinner.setAdapter(adapter_state);
        ArrayAdapter<String> adapter_currecny = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, currency);
        currecy_spinner.setAdapter(adapter_currecny);
        ArrayAdapter<String> adapter_paymode = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, pay_mode);
        paymode_spinner.setAdapter(adapter_paymode);
        ArrayAdapter<String> adapter_expirymonth = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, expiry_month);
        expirymonth_spinner.setAdapter(adapter_expirymonth);
        ArrayAdapter<String> adapter_expiryyear = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, expiry_year);
        expiry_year_spinner.setAdapter(adapter_expiryyear);
        ArrayAdapter<String> adapter_userlogged = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, user_logged);
        user_logged_spinner.setAdapter(adapter_userlogged);


        country_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub
                country_code = countries[arg2];
                currecy_type = currnecy_code[arg2];
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }

        });
        currecy_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub
                country_code = countries[arg2];
                currecy_type = currnecy_code[arg2];
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }

        });
        paymode_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub
                if (arg2 != 0)
                    paymode = pay_mode[arg2];
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }

        });
        expiry_year_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub
                //if (arg2 != 0)
                    //expYear = expiry_year[arg2];
                expYear = "20";
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }

        });
        expirymonth_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub
//                if (arg2 != 0)
//                    expMonth = expiry_month[arg2];
                expMonth = "2";
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }

        });

        user_logged_spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub
                if (arg2 != 0)
                    isuserlogged = user_logged[arg2];
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }

        });
        bt_payment = (Button) findViewById(R.id.bt_payment);
        bt_payment.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                Intent i = new Intent(AvantgardeSdkUsageActivity.this, PaymentGateActivity.class);
                i.putExtra("ME_ID", me_id);
                i.putExtra("TXN_DETAILS", "" + transactionDetails());//important
                i.putExtra("PG_DETAILS", "" + PaymentGatewayDetails());
                i.putExtra("CARD_DETAILS", "" + cardDetails());
                i.putExtra("CUSTOMER_DETAILS", "" + customerDetails());
                i.putExtra("BILLING_DETAILS", "" + billingDetails());
                i.putExtra("SHIPPING_DETAILS", "" + shippingDetails());
                i.putExtra("ITEM_DETAILS", "" + itemDetails());
                i.putExtra("OTHER_DETAILS", "" + otherDetails());
                i.putExtra("totalprice",""+getIntent().getStringExtra("totalprice"));
                i.putExtra("aid",""+getIntent().getStringExtra("address_id"));
//                i.putExtra("date",""+getIntent().getStringExtra("date"));
//                i.putExtra("slots",""+getIntent().getStringExtra("slots"));
                i.putExtra("desc",""+getIntent().getStringExtra("desc"));


                startActivityForResult(i, 32);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            String str = data.getStringExtra("MESSAGE");
            String response = data.getStringExtra("RESPONSE");
            if (response != null || !response.equals("")) {
                String responsePG = data.getStringExtra("PGRESPONSE");
                Intent intent = new Intent(AvantgardeSdkUsageActivity.this, ResponseActivity.class);
                intent.putExtra("RESPONSE", response);
                intent.putExtra("PGRESPONSE", responsePG);
                startActivity(intent);
                //String text = new String(resp);
                Log.wtf("!!!!RESPONSE", response);

            }
            order_no = "" + gen();
            edit_ordernumber.setText(order_no);
            Toast.makeText(getApplicationContext(), str, Toast.LENGTH_LONG)
                    .show();
        }
    }


    public String transactionDetails() {
        String s1, s2, s3, s4, s5, s6, s7, s8;
        s1 = edit_ordernumber.getText().toString().trim();
        s2 = edit_amount.getText().toString().trim();
        s3 = country_code;
        s4 = currecy_type;
        s5 = edit_transaction_type.getText().toString().trim();
        s6 = edit_success_url.getText().toString().trim();
        s7 = edit_fialure_url.getText().toString().trim();
        s8 = edit_channel.getText().toString().trim();

        String txn_details = "paygate" + "|" + me_id + "|" + s1 + "|" + s2 + "|" + s3 + "|" + s4 + "|" + s5 + "|" + s6 + "|" + s7 + "|" + s8;

        return txn_details;
    }

    public String PaymentGatewayDetails() {
        String s1, s2, s3, s4;
        s1 = edit_payment_gateway_id.getText().toString().trim();
        s2 = paymode;
        s3 = edit_schem.getText().toString().trim();
        s4 = edit_emi_months.getText().toString().trim();

        String pgDetails = s1 + "|" + s2 + "|" + s3 + "|" + s4;
        if (s1.length() == 0 && s2.length() == 0 && s3.length() == 0 && s3.length() == 0) {
            return "|||";
        } else {
            return pgDetails;
        }
    }

    public String cardDetails() {

        String s1, s2, s3, s4, s5;
        s1 = edit_card_no.getText().toString().trim();
        s2 = expMonth;
        s3 = expYear;
        s4 = edit_cvv.getText().toString().trim();
        s5 = edit_card_name.getText().toString().trim();

        String cardDetails = s1 + "|" + s2 + "|" + s3 + "|" + s4 + "|" + s5;
        if (s1.length() == 0 && s2.length() == 0 && s3.length() == 0 && s4.length() == 0 && s5.length() == 0) {
            return "||||";
        } else {
            return cardDetails;
        }
    }

    public String customerDetails() {
        String s1, s2, s3, s4, s5;
        sharedpreferences = getSharedPreferences("User_id", MODE_PRIVATE);
        edit_cust_name.setText(""+getIntent().getStringExtra("nane"));
        edit_mobile_number.setText(""+getIntent().getStringExtra("phone"));
        edit_emailid.setText(""+getIntent().getStringExtra("email"));
        edit_uniqueid.setText("wallet");
        s1 = edit_cust_name.getText().toString().trim();
        s2 = edit_emailid.getText().toString().trim();
        s3 = edit_mobile_number.getText().toString().trim();
        s4 = edit_uniqueid.getText().toString().trim();
        s5 = isuserlogged;

        String customer_details = s1 + "|" + s2 + "|" + s3 + "|" + s4 + "|" + s5;
        if (s1.length() == 0 && s2.length() == 0 && s3.length() == 0 && s4.length() == 0 && s5.length() == 0) {
            return "||||";
        } else {
            return customer_details;
        }
    }

    public String billingDetails() {
        String s1, s2, s3, s4, s5;
        edit_bill_address.setText(""+getIntent().getStringExtra("aid"));
        edit_bill_city.setText(""+getIntent().getStringExtra("aid"));
        edit_bill_state.setText(""+getIntent().getStringExtra("aid"));
        edit_bill_country.setText(""+getIntent().getStringExtra("aid"));
        edit_bill_zip.setText(""+getIntent().getStringExtra("aid"));
        s1 = edit_bill_address.getText().toString().trim();
        s2 = edit_bill_city.getText().toString().trim();
        s3 = edit_bill_state.getText().toString().trim();
        s4 = edit_bill_country.getText().toString().trim();
        s5 = edit_bill_zip.getText().toString().trim();

        String billing_details = s1 + "|" + s2 + "|" + s3 + "|" + s4 + "|" + s5;
        if (s1.length() == 0 && s2.length() == 0 && s3.length() == 0 && s4.length() == 0 && s5.length() == 0) {
            return "||||";
        } else {
            return billing_details;
        }
    }

    public String shippingDetails() {
        String s1, s2, s3, s4, s5, s6, s7;
        s1 = edit_ship_address.getText().toString().trim();
        s2 = edit_ship_city.getText().toString().trim();
        s3 = edit_ship_state.getText().toString().trim();
        s4 = edit_ship_country.getText().toString().trim();
        s5 = edit_ship_zip.getText().toString().trim();
        s6 = edit_ship_days.getText().toString().trim();
        s7 = edit_address_count.getText().toString().trim();

        String shipping_details = s1 + "|" + s2 + "|" + s3 + "|" + s4 + "|" + s5 + "|" + s6 + "|" + s7;
        if (s1.length() == 0 && s2.length() == 0 && s3.length() == 0 && s4.length() == 0 && s5.length() == 0 && s6.length() == 0 && s7.length() == 0) {
            return "||||||";
        } else {
            return shipping_details;
        }
    }

    public String itemDetails() {
        String s1, s2, s3;
        s1 = edit_itemcount.getText().toString().trim();
        s2 = edit_item_value.getText().toString().trim();
        s3 = edit_item_category.getText().toString().trim();

        String itemdetails = s1 + "|" + s2 + "|" + s3;
        if (s1.length() == 0 && s2.length() == 0 && s3.length() == 0 && s3.length() == 0) {
            return "Grocery|1|Grocery";
        } else {
            return itemdetails;
        }
    }

    public String otherDetails() {
        String s1, s2, s3, s4, s5;
        s1 = edit_UDF1.getText().toString().trim();
        s2 = edit_UDF2.getText().toString().trim();
        s3 = edit_UDF3.getText().toString().trim();
        s4 = edit_UDF4.getText().toString().trim();
        s5 = edit_UDF5.getText().toString().trim();

        String otherdetails = s1 + "|" + s2 + "|" + s3 + "|" + s4 + "|" + s5;
        if (s1.length() == 0 && s2.length() == 0 && s3.length() == 0 && s4.length() == 0 && s5.length() == 0) {
            return "||||";
        } else {
            return otherdetails;
        }
    }
}
