package com.cab.ethrocab.ethro;

import android.location.Location;
import android.support.multidex.MultiDex;

import com.cab.ethrocab.ethro.view.SplashScreenActivity;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.softminesol.locations.locationmanager.LocationManagerService;
import com.softminesol.propertysurvey.CommonBaseUrl;

import frameworks.AppBaseApplication;
import frameworks.appsession.AppSessionManager;
import frameworks.basemvp.AppBaseActivity;
import frameworks.routers.ILocationRouter;
import frameworks.routers.ILoginInterceptor;
import in.tuktuk.usersession.login.view.LoginActivity;
import in.tuktuk.usersession.registerDriver.data.net.UserRegisterURL;
import in.tuktuk.usersession.userSession.data.net.LoginAPIURL;

/**
 * Created by USER on 11/30/2017.
 */

public class Bean extends AppBaseApplication implements ILoginInterceptor, ILocationRouter {


    //   public String baseURL = "http://www.nationproducts.in/tuktuk/olddata/";
    AppSessionManager sessionValue;

    public String baseURL = "http://nationproducts.in/zango/olddata/";

    public String userId = "";
    public String phone = "";
    public String email = "";
    public String image = "";
    public String otp = "";
    public String name = "";
    public String latitude = "";
    public String lngitudeo = "";


    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        sessionValue = new AppSessionManager(this);
        generateAppBaseUrl();
        in.tuktuk.usersession.login.view.LoginActivity.userType = LoginActivity.USER_TYPE_CUSTOMER;
        ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(this));

    }


    private void generateAppBaseUrl() {
        LoginAPIURL.BASE_URL = CommonBaseUrl.BASE_URL;
        UserRegisterURL.BASE_URL = CommonBaseUrl.BASE_URL;
    }

    @Override
    public void startLogin() {
        mApplication.getBaseContext().startActivity(SplashScreenActivity.getIntent(mApplication.getBaseContext()));
    }

    @Override
    public void logout() {
        sessionValue.clearSession();
        startLogin();
    }


    @Override
    public void onLocationChanged(Location location, AppBaseActivity appBaseActivity) {
        LocationManagerService locationManagerService = LocationManagerService.getInstance();
        locationManagerService.setCurrentLocation(location);
    }

}
