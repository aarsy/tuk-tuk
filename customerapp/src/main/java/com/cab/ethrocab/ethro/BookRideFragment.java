package com.cab.ethrocab.ethro;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Telephony;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.TransportMode;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.util.DirectionConverter;
import com.cab.ethrocab.ethro.EstimatedFarePOJO.EstimatedBean;
import com.cab.ethrocab.ethro.EstimatedTimePOJO.TimeBean;
import com.cab.ethrocab.ethro.NearByPOJO.Estimated;
import com.cab.ethrocab.ethro.NearByPOJO.NearByBean;
import com.cab.ethrocab.ethro.cancelBookingPOJO.cancelBookingBean;
import com.cab.ethrocab.ethro.checkBookingPOJO.checkBookingBean;
import com.cab.ethrocab.ethro.dataPOJO.dataBea;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;
import github.nisrulz.easydeviceinfo.base.EasyLocationMod;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.CAMERA_SERVICE;
import static com.facebook.FacebookSdk.CALLBACK_OFFSET_PROPERTY;
import static com.facebook.FacebookSdk.getApplicationContext;
import static com.facebook.FacebookSdk.getOnProgressThreshold;
import static com.facebook.FacebookSdk.isDebugEnabled;


public class BookRideFragment extends Fragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, DirectionFinderListener, DriverFinderListener , DirectionCallback{

    TextView cancel, searchBar1, searchBarTv, searchBar2;
    SupportMapFragment mSupportMapFragment;
    TextView searchBar;
    RelativeLayout searchBarMain, carLayout, confirmLayout;
    RecyclerView cabTypeList;
    LinearLayoutManager manager;
    ConnectionDetector cd;
    LinearLayout paymentType;
    boolean isLocationEnabled=false;
    private List<Marker> originMarkers = new ArrayList<>();
    private List<Marker> destinationMarkers = new ArrayList<>();
    private List<Polyline> polylinePaths = new ArrayList<>();
    private List<PolylineOptions> polylinePaths2 = new ArrayList<>();

    TextView onRide;
    String driverNumber;
    List<Estimated>cabList;
    LinearLayout cabConfirm;
    int cabPosition = 0;
    String selectedId = "0";
    Button continueBtn, continuebtnfirst;
    FloatingActionButton mylocationButton;
    private int PLACE_PICKER_REQUEST = 1;
    private int PLACE_PICKER_REQUEST2 = 2;

    String filterId = "0";

    String TAG = "TAG:VOXOX";

    Timer bookingTimer;

    Call<checkBookingBean> bookingAPI;

    SharedPreferences pref;
    SharedPreferences.Editor edit;

    GoogleMap map;

    ImageButton backToType;

    String userId;

    String estPrice;

    Location currentLocation;
    Location pickUpLocation;
    Location dropLocation;

    String pickUpLat ="";//= "28.504950";
    String pickUpLng ="";// "77.299930";

    String dropLat = "";
    String dropLng = "";

    TextView price, e_time;

    CircleImageView driverImage;

    GoogleApiClient googleApiClient;
    private int PLAY_SERVICES_REQUEST = 32;
    private int REQUEST_CHECK_SETTINGS = 33;

    CabAdapter adapter;

    ProgressBar progress;

    LatLngBounds.Builder builder;
    LatLngBounds bounds;

    TextView wallet;

    LinearLayout driverLayout, findingLayout;
    ProgressBar findingProgress;
    TextView findingText;
    Button retry , cancelRide;

    String bookingId;

    TextView cabType, cabName, cabNumber, otp, driverName, driverRating, callDriver;
    RatingBar rating;

    LocationManager locationManager;

    String cabURL;
    private String serverKey = "AIzaSyAhAl4GmL6ZajLOFuRxP1J49IF7Wzx5snY";
    private LatLng origin;
    private LatLng destination ;




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_book_ride, container, false);
           // getloctionenable();
        onRide = (TextView)view.findViewById(R.id.on_ride);
        cabList = new ArrayList<>();
        cabType = (TextView) view.findViewById(R.id.cab_type);
        cabName = (TextView) view.findViewById(R.id.cab_name);
        cabNumber = (TextView) view.findViewById(R.id.cab_number);
        otp = (TextView) view.findViewById(R.id.otp);
        driverName = (TextView) view.findViewById(R.id.driverName);
        driverRating = (TextView) view.findViewById(R.id.driverRating);
        rating = (RatingBar) view.findViewById(R.id.rating);
        callDriver = (TextView) view.findViewById(R.id.call_driver);

        driverImage = (CircleImageView) view.findViewById(R.id.profileImage);

        driverLayout = (LinearLayout) view.findViewById(R.id.driver_layout);
        findingLayout = (LinearLayout) view.findViewById(R.id.finding_layout);
        findingProgress = (ProgressBar) view.findViewById(R.id.finding_progress);
        findingText = (TextView) view.findViewById(R.id.finding_text);
        retry = (Button) view.findViewById(R.id.retry);
        cancelRide = (Button) view.findViewById(R.id.cancel_ride);


        manager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);

        paymentType = (LinearLayout) view.findViewById(R.id.payment_type);
        wallet = (TextView) view.findViewById(R.id.wallet);

//        adapter = new CabAdapter(getContext(), cabList);


        userId = getArguments().getString("userId");

        //Log.d("userId", userId);

        backToType = (ImageButton) view.findViewById(R.id.back_to_type);

        price = (TextView) view.findViewById(R.id.price);
        e_time = (TextView) view.findViewById(R.id.e_time);


        progress = (ProgressBar) view.findViewById(R.id.progress);

        searchBarMain = (RelativeLayout) view.findViewById(R.id.searchBarMain);
        carLayout = (RelativeLayout) view.findViewById(R.id.carLayout);
        cabConfirm = (LinearLayout) view.findViewById(R.id.cabConfirm);
        //cabSelection = (LinearLayout) view.findViewById(R.id.cabSelection);
        continueBtn = (Button) view.findViewById(R.id.continuebtn);
        confirmLayout = (RelativeLayout) view.findViewById(R.id.confirmLayout);
        continuebtnfirst = (Button) view.findViewById(R.id.continuebtnFirst);
        searchBar1 = (TextView) view.findViewById(R.id.searchBar1);
        searchBar2 = (TextView) view.findViewById(R.id.searchBar2);
        cd = new ConnectionDetector(getContext());

        cabTypeList = (RecyclerView) view.findViewById(R.id.cab_type_list);


        cabTypeList.setLayoutManager(manager);

        mylocationButton = (FloatingActionButton) view.findViewById(R.id.fab);


        locationManager = (LocationManager)Objects.requireNonNull(getActivity()). getSystemService(Context.LOCATION_SERVICE);

        if (locationManager != null) {
            if( !locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ) {

                getloctionenable();
            }else {
                getlocationouser();

            }
        }

        mylocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (locationManager != null) {
                    if( !locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ) {

                        getloctionenable();
                    }else {
                        getlocationouser();

                    }
                }

            }
        });

        if (checkPlayServices()) {
            googleApiClient = new GoogleApiClient.Builder(getContext())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API).build();

            googleApiClient.connect();

            LocationRequest mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(9000);
            mLocationRequest.setFastestInterval(3000);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(mLocationRequest);

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());

            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult locationSettingsResult) {

                    final Status status = locationSettingsResult.getStatus();

                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // All location settings are satisfied. The client can initialize location requests here

                            /*if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return;
                            }
                            currentLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);


                            pickUpLat = currentLocation.getLatitude();*/


                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);

                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            break;
                    }
                }
            });

        }

        // searchBarTv = (TextView) view.findViewById(R.id.sbt1);
        searchBar1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(builder.build((Activity) getContext()), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }

            }
        });

        searchBar2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PlacePicker.IntentBuilder builder2 = new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(builder2.build((Activity) getContext()), PLACE_PICKER_REQUEST2);
                } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });

        cancel = view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.booking_dialog);
                dialog.setCancelable(true);
                dialog.show();
                TextView cancel = dialog.findViewById(R.id.cancel);
                final RadioGroup group = (RadioGroup) dialog.findViewById(R.id.group);
                TextView no = (TextView) dialog.findViewById(R.id.no);

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        RadioButton rb = (RadioButton) dialog.findViewById(group.getCheckedRadioButtonId());

                        progress.setVisibility(View.VISIBLE);

                        final Bean b = (Bean) getContext().getApplicationContext();


                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl(b.baseURL)
                                .addConverterFactory(ScalarsConverterFactory.create())
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();
                        Allapi cr = retrofit.create(Allapi.class);

                        Call<cancelBookingBean> call = cr.cancelBooking(userId, bookingId, rb.getText().toString());

                        call.enqueue(new Callback<cancelBookingBean>() {
                            @Override
                            public void onResponse(Call<cancelBookingBean> call, Response<cancelBookingBean> response) {

                               // Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();

                                dialog.dismiss();

                                bookingAPI.cancel();

                                cancelBooking();

                                progress.setVisibility(View.GONE);
                            }

                            @Override
                            public void onFailure(Call<cancelBookingBean> call, Throwable t) {
                                progress.setVisibility(View.GONE);
                            }
                        });

                    }
                });

                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();

                    }
                });

            }
        });

        cancelRide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progress.setVisibility(View.VISIBLE);

                final Bean b = (Bean) getContext().getApplicationContext();

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(b.baseURL)
                        .addConverterFactory(ScalarsConverterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                Allapi cr = retrofit.create(Allapi.class);

                Call<cancelBookingBean> call = cr.cancelBooking(userId, bookingId, "My reason is not listed");

                call.enqueue(new Callback<cancelBookingBean>() {
                    @Override
                    public void onResponse(Call<cancelBookingBean> call, Response<cancelBookingBean> response) {

                        //Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        //dialog.dismiss();

                        bookingAPI.cancel();

                        cancelBooking();

                        progress.setVisibility(View.GONE);
                    }

                    @Override
                    public void onFailure(Call<cancelBookingBean> call, Throwable t) {
                        progress.setVisibility(View.GONE);
                    }
                });

//                final Dialog dialog = new Dialog(getActivity());
//                dialog.setContentView(R.layout.booking_dialog);
//                dialog.setCancelable(true);
//                dialog.show();
//
//                TextView cancel = dialog.findViewById(R.id.cancel);
//                final RadioGroup group = (RadioGroup) dialog.findViewById(R.id.group);
//                TextView no = (TextView) dialog.findViewById(R.id.no);
//
//
//                cancel.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                        if (group.getCheckedRadioButtonId()!= -1){
//
//                        }else {
//                            Toast.makeText(getContext(), "Please choose a reason to cancel", Toast.LENGTH_SHORT).show();
//                        }
//
//                        RadioButton rb = (RadioButton) dialog.findViewById(group.getCheckedRadioButtonId());
//
//
//
//                    }
//                });
//
//                no.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//
//                        dialog.dismiss();
//
//                    }
//                });

            }
        });

        backToType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                carLayout.setVisibility(View.GONE);
                searchBarMain.setVisibility(View.VISIBLE);

                searchBar1.setClickable(true);
                searchBar2.setClickable(true);

                searchBar1.setFocusable(true);
                searchBar2.setFocusable(true);

            }
        });

        continuebtnfirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (cd.isConnectingToInternet()) {

                    if (dropLat.length() > 0 && dropLng.length() > 0) {

                      // nearbyCall.cancel();

                        Log.d("pickuplat", pickUpLat);
                        Log.d("pickuplng", pickUpLng);

                        Log.d("dropLat", dropLat);
                        Log.d("dropLng", dropLng);

                        Log.d("cabType", selectedId);
                        Log.d("usedddrId", userId);
                        Log.e("usedddrId", userId);

                        progress.setVisibility(View.VISIBLE);

                        final Bean b = (Bean) getContext().getApplicationContext();

                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl(b.baseURL)
                                .addConverterFactory(ScalarsConverterFactory.create())
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();
                        Allapi cr = retrofit.create(Allapi.class);

                        Call<EstimatedBean> call = cr.getEstimatedFare(userId, pickUpLat, pickUpLng, dropLat, dropLng, selectedId);

                        call.enqueue(new Callback<EstimatedBean>() {
                            @Override
                            public void onResponse(Call<EstimatedBean> call, Response<EstimatedBean> response) {

                                price.setText("INR " + String.valueOf(response.body().getData().getEstimated().get(0).getPrice()));
                                e_time.setText(String.valueOf(response.body().getData().getDriverArrivalTime()));
                                estPrice = String.valueOf(response.body().getData().getEstimated().get(0).getPrice());
                                carLayout.setVisibility(View.VISIBLE);
                                searchBarMain.setVisibility(View.GONE);
                                searchBar1.setClickable(false);
                                searchBar2.setClickable(false);
                                searchBar1.setFocusable(false);
                                searchBar2.setFocusable(false);

                                progress.setVisibility(View.GONE);
                            }

                            @Override
                            public void onFailure(Call<EstimatedBean> call, Throwable t) {

                                progress.setVisibility(View.GONE);

                            }
                        });


                    } else {
                        Toast.makeText(getContext(), "Enter Drop Location", Toast.LENGTH_SHORT).show();
                    }


                } else {
                    Toast.makeText(getContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();

                }


            }
        });


        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //count = 0;
                bookingStatus(bookingId);
            }
        });


        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (cd.isConnectingToInternet()) {


                    progress.setVisibility(View.VISIBLE);

                    final Bean b = (Bean) getContext().getApplicationContext();

                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(b.baseURL)
                            .addConverterFactory(ScalarsConverterFactory.create())
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    Allapi cr = retrofit.create(Allapi.class);

                    Call<EstimatedBean> call = cr.confirm(userId, pickUpLat, pickUpLng, dropLat, dropLng, selectedId, estPrice, "cash");

                    call.enqueue(new Callback<EstimatedBean>() {
                        @Override
                        public void onResponse(Call<EstimatedBean> call, Response<EstimatedBean> response) {


                            Log.d("bookingId", response.body().getData().getBookingId());


                            //progress.setVisibility(View.VISIBLE);

                            bookingId = response.body().getData().getBookingId();

                            bookingStatus(response.body().getData().getBookingId());


                            carLayout.setVisibility(View.GONE);
                            confirmLayout.setVisibility(View.VISIBLE);

                            progress.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<EstimatedBean> call, Throwable t) {

                            progress.setVisibility(View.GONE);

                        }
                    });

                } else {
                    Toast.makeText(getContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();

                }


            }
        });

        mSupportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.bookRideFragment);
        if (mSupportMapFragment == null) {
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            mSupportMapFragment = SupportMapFragment.newInstance();
            fragmentTransaction.replace(R.id.bookRideFragment, mSupportMapFragment).commit();
        }

        if (mSupportMapFragment != null) {
            mSupportMapFragment.getMapAsync(this);
        }


        paymentType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getContext(), PaymentType.class);
                startActivityForResult(intent, 13);

            }
        });


        return view;

    }


    public void cancelBooking() {

        count = 0;

        dropLat = "";
        dropLng = "";

        searchBar2.setText("Destination Location");

        getNearbyData(pickUpLat , pickUpLng);


        confirmLayout.setVisibility(View.GONE);
        searchBarMain.setVisibility(View.VISIBLE);

        searchBar1.setClickable(true);
        searchBar2.setClickable(true);

        searchBar1.setFocusable(true);
        searchBar2.setFocusable(true);

    }


    private boolean checkPlayServices() {

        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = googleApiAvailability.isGooglePlayServicesAvailable(getActivity());

        if (resultCode != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(resultCode)) {
                googleApiAvailability.getErrorDialog(getActivity(), resultCode,
                        PLAY_SERVICES_REQUEST).show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
            }
            return false;
        }
        return true;
    }


    Call<NearByBean> nearbyCall;


    private void getNearbyData(final String latitude, final String longitude) {


        if (cd.isConnectingToInternet()) {
            progress.setVisibility(View.VISIBLE);

            if (dropLat.length() == 0 && dropLng.length() == 0) {
                try {
                    final Bean b = (Bean) getContext().getApplicationContext();


                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(b.baseURL)
                            .addConverterFactory(ScalarsConverterFactory.create())
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    Allapi cr = retrofit.create(Allapi.class);


                    Log.e("USERID", userId +" "+latitude+" "+ longitude);

                    nearbyCall = cr.nearBy(userId, latitude, longitude);

                    nearbyCall.enqueue(new Callback<NearByBean>() {
                        @Override
                        public void onResponse(Call<NearByBean> call, Response<NearByBean> response) {

                            try {
                                //Log.e("resp", response.body().toString());
                                if (Objects.equals(response.body().getStatus(), "1")) {


                                    //Log.d("messasd", response.body().getMessage());
                                    //Log.d("driver", response.body().getData().get(1).getLatitude());
                                    //builder = new LatLngBounds.Builder();

                                    map.clear();

                                    if (pickUpLat.equalsIgnoreCase("")) {
                                        Marker marker = map.addMarker(new MarkerOptions()
                                                .position(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()))
                                                .icon(bitmapDescriptorFromVector(getContext(), R.drawable.pinred)));

                                        marker.showInfoWindow();


                                    } else {
                                        Marker marker = map.addMarker(new MarkerOptions()
                                                .position(new LatLng(Double.parseDouble(pickUpLat), Double.parseDouble(pickUpLng)))
                                                .icon(bitmapDescriptorFromVector(getContext(), R.drawable.pinred)));

                                        marker.showInfoWindow();
                                    }

                                    LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);


                                    builder = new LatLngBounds.Builder();


                                    for (int i = 0; i < response.body().getData().size(); i++) {


                                        if (Objects.equals(selectedId, response.body().getData().get(i).getCabId())) {

                                            final String lat1 = response.body().getData().get(i).getLatitude();
                                            final String lon1 = response.body().getData().get(i).getLongitude();
                                            LatLng driver1 = new LatLng(Double.parseDouble(lat1), Double.parseDouble(lon1));
                                            final View pin = inflater.inflate(R.layout.marker, null);
                                            pin.layout(0, 0, 100, 100);

                                            final ImageView image = (ImageView) pin.findViewById(R.id.marker_image);

                                            Ion.with(getContext()).load(response.body().getData().get(i).getIconImage()).withBitmap().asBitmap().setCallback(new FutureCallback<Bitmap>() {
                                                @Override
                                                public void onCompleted(Exception e, Bitmap result) {

                                                    try {

                                                        image.setImageBitmap(result);
                                                        Marker marker = map.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(lat1), Double.parseDouble(lon1)))
                                                                .icon(BitmapDescriptorFactory.fromBitmap(getViewBitmap(pin))));
                                                        builder.include(marker.getPosition());

                                                        //markers.put(mar.getId() , item);


                                                    } catch (NullPointerException e1) {
                                                        e1.printStackTrace();
                                                    }


                                                }
                                            });
                                        }

                                    }


                                   //  bounds = builder.build();


                                    //bounds = builder.build();
                                    //CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 250);
                                    //map.animateCamera(cu);
                                    //Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                } else {
                                    //Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                    //Log.d("msg", response.body().getMessage());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            if (pickUpLat.length() == 0) {

                                getNearbyData(pickUpLat, pickUpLng);

                            } else {
                                pickUpLat = String.valueOf(currentLocation.getLatitude());
                                pickUpLng = String.valueOf(currentLocation.getLongitude());

                                getNearbyData(pickUpLat , pickUpLng);

                                //getNearbyData(pickUpLat, pickUpLng);

                            }

                            //Log.d("cabData" , String.valueOf(response.body().getEstimated().size()));

                            //cabCount = response.body().getEstimated().size();
                            adapter = new CabAdapter(getActivity(), cabList);
                            adapter.setGridData(response.body().getEstimated());
                            cabTypeList.setAdapter(adapter);

                           int cabCount = response.body().getEstimated().size();

//
                           // Toast.makeText(getActivity(), "count:-   "+cabCount, Toast.LENGTH_SHORT).show();

                            progress.setVisibility(View.GONE);

                        }


                        @Override
                        public void onFailure(Call<NearByBean> call, Throwable t) {

                            nearbyCall.clone().enqueue(this);
                            progress.setVisibility(View.GONE);

                            t.printStackTrace();

                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


        } else {
            progress.setVisibility(View.GONE);
            Toast.makeText(getContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();

        }

    }

    int count=0;
    private void bookingStatus(final String bookId) {

        final Bean b = (Bean) getApplicationContext();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(b.baseURL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Allapi cr = retrofit.create(Allapi.class);


        bookingAPI = cr.checkBooking(userId, bookId);


        bookingAPI.enqueue(new Callback<checkBookingBean>() {
            @Override
            public void onResponse(Call<checkBookingBean> call, Response<checkBookingBean> response) {


                String status = response.body().getStatus();
                Log.e("Booking Status", status+"/n"+response.body().getMessage()+"/n"+response.body().getData());

                switch (status) {
                    case "0":
                        count++;
                        findingProgress.setVisibility(View.VISIBLE);
                        driverLayout.setVisibility(View.GONE);
                        findingLayout.setVisibility(View.VISIBLE);
                        if (count > 40) {

                            findingProgress.setVisibility(View.GONE);
                            retry.setEnabled(true);
                            findingText.setText("No Ride found, retry");
                            bookingAPI.cancel();

                        } else {
                            findingProgress.setVisibility(View.VISIBLE);
                            retry.setEnabled(false);
                            findingText.setText("Finding Your Ride, please wait");
                            bookingStatus(bookId);
                        }

                        break;

                    case "1":

                        findingLayout.setVisibility(View.GONE);
                        driverLayout.setVisibility(View.VISIBLE);

                        driverNumber = response.body().getData().getDriverPhone();

                        cabType.setText(response.body().getData().getCabType());
                        cabName.setText(response.body().getData().getCabName());
                        cabNumber.setText(response.body().getData().getCabNumber());
                        otp.setText(response.body().getData().getOTP());
                        driverName.setText(response.body().getData().getDriverName());
                        driverRating.setText(response.body().getData().getDriverRating());
                        if(response.body().getData().getDriverRating().equalsIgnoreCase("")){
                            rating.setRating(0);
                        }else{
                            rating.setRating(Float.parseFloat(response.body().getData().getDriverRating()));
                        }

                        callDriver.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
//                                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + driverNumber));
//                                startActivity(intent);
                                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                                callIntent.setData(Uri.parse("tel:"+Uri.encode(driverNumber)));
                                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(callIntent);
                            }
                        });


                        onRide.setVisibility(View.GONE);

                        DisplayImageOptions options = new DisplayImageOptions.Builder().resetViewBeforeLoading(false).cacheOnDisk(true).cacheInMemory(true).build();
                        ImageLoader loader = ImageLoader.getInstance();

                        loader.displayImage(response.body().getData().getDriverImage(), driverImage, options);


                        try {
                            new DriverFinder(BookRideFragment.this, pickUpLat, pickUpLng, response.body().getData().getDriverLat(), response.body().getData().getDriverLong()).execute();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                        bookingStatus(bookId);

                        break;

                    case "2":

                        findingLayout.setVisibility(View.GONE);
                        driverLayout.setVisibility(View.GONE);
                        onRide.setVisibility(View.VISIBLE);

                        bookingStatus(bookId);

                        break;

                    case "3":
                        Intent intent = new Intent(getContext() , RateDriver.class);
                        intent.putExtra("image" , response.body().getData().getDriverImage());
                        intent.putExtra("rating" , response.body().getData().getDriverRating());
                        intent.putExtra("name" , response.body().getData().getDriverName());
                        intent.putExtra("uid" , userId);
                        intent.putExtra("bid" , ""+bookId);
                        intent.putExtra("type" , response.body().getData().getCabName() + " " + response.body().getData().getCabNumber());
                        intent.putExtra("amount" , String.valueOf(response.body().getData().getEstimated()));
                        intent.putExtra("did" , String.valueOf(response.body().getData().getDriverId()));

                        startActivity(intent);

                        getActivity().finish();

                        break;


                }


                progress.setVisibility(View.GONE);


            }

            @Override
            public void onFailure(Call<checkBookingBean> call, Throwable t) {

                bookingAPI.clone().enqueue(this);
                t.printStackTrace();

            }
        });


    }


    public static Bitmap getViewBitmap(View view) {
        //Get the dimensions of the view so we can re-layout the view at its current size
        //and create a bitmap of the same size
        int width = view.getWidth();
        int height = view.getHeight();

        int measuredWidth = View.MeasureSpec.makeMeasureSpec(width, View.MeasureSpec.EXACTLY);
        int measuredHeight = View.MeasureSpec.makeMeasureSpec(height, View.MeasureSpec.EXACTLY);

        //Cause the view to re-layout
        view.measure(measuredWidth, measuredHeight);
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());

        //Create a bitmap backed Canvas to draw the view into
        Bitmap b = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);

        //Now that the view is laid out and we have a canvas, ask the view to draw itself into the canvas
        view.draw(c);

        return b;
    }


    private BitmapDescriptor bitmapDescriptorFromVector(Context context, @DrawableRes int vectorDrawableResourceId) {
        Drawable background = ContextCompat.getDrawable(context, vectorDrawableResourceId);
        background.setBounds(0, 0, background.getIntrinsicWidth(), background.getIntrinsicHeight());
        //Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorDrawableResourceId);
        //vectorDrawable.setBounds(40, 20, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(background.getIntrinsicWidth(), background.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        background.draw(canvas);
        //vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }


    /*public void onClick(View v) {

        switch (v.getId()) {
            case R.id.miniLin:
                mini.setImageResource(R.drawable.mini32);
                mini.setBackgroundResource(R.drawable.backcar);
                micro.setImageResource(R.drawable.microsvg);
                micro.setBackgroundResource(R.drawable.backcarwhite);
                sedan.setImageResource(R.drawable.sedansvg);
                sedan.setBackgroundResource(R.drawable.backcarwhite);
                break;

            case R.id.microLin:
                micro.setImageResource(R.drawable.micro32);
                micro.setBackgroundResource(R.drawable.backcar);
                mini.setImageResource(R.drawable.minisvg);
                mini.setBackgroundResource(R.drawable.backcarwhite);
                sedan.setImageResource(R.drawable.sedansvg);
                sedan.setBackgroundResource(R.drawable.backcarwhite);
                break;

            case R.id.sedanLin:
                sedan.setImageResource(R.drawable.sedan32);
                sedan.setBackgroundResource(R.drawable.backcar);
                mini.setImageResource(R.drawable.minisvg);
                mini.setBackgroundResource(R.drawable.backcarwhite);
                micro.setImageResource(R.drawable.microsvg);
                micro.setBackgroundResource(R.drawable.backcarwhite);
                break;

        }

    }*/

    @Override
    public void onActivityResult(final int requestCode, int resultCode, Intent data) {

        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(getContext(), data);
                StringBuilder stBuilder = new StringBuilder();
                String placename = String.format("%s", place.getName());
                String latitude = String.valueOf(place.getLatLng().latitude);
                String longitude = String.valueOf(place.getLatLng().longitude);
                String address = String.format("%s", place.getAddress());
                stBuilder.append(address);

                pickUpLat = String.valueOf(place.getLatLng().latitude);
                pickUpLng = String.valueOf(place.getLatLng().longitude);

                if (dropLat.length() > 0 && dropLng.length() > 0) {

                    origin = new LatLng(Double.parseDouble(pickUpLat), Double.parseDouble(pickUpLng));
                    destination = new LatLng(Double.parseDouble(dropLat), Double.parseDouble(dropLng));
                    requestDirection();
//                    try {
//                        new DirectionFinder(this, pickUpLat, pickUpLng, dropLat, dropLng).execute();
//                    } catch (UnsupportedEncodingException e) {
//                        e.printStackTrace();
//                    }

                } else {

                    LatLng latLng = new LatLng(Double.parseDouble(pickUpLat), Double.parseDouble(pickUpLng));

                    CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(15.0f).build();
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
                    map.moveCamera(cameraUpdate);
                }

                searchBar1.setText(stBuilder.toString());
                //placeMarkerOnMap(place.getLatLng());

            }
        } else if (requestCode == PLACE_PICKER_REQUEST2) {
            if (resultCode == RESULT_OK) {

               // nearbyCall.cancel();

                Place place2 = PlacePicker.getPlace(getContext(),data);
                StringBuilder stBuilder2 = new StringBuilder();
                String placename = String.format("%s", place2.getName());
                String latitude = String.valueOf(place2.getLatLng().latitude);
                String longitude = String.valueOf(place2.getLatLng().longitude);

                dropLat = String.valueOf(place2.getLatLng().latitude);
                dropLng = String.valueOf(place2.getLatLng().longitude);

                if (pickUpLat.length() > 0 && pickUpLng.length() > 0) {

                    origin = new LatLng(Double.parseDouble(pickUpLat), Double.parseDouble(pickUpLng));
                    destination = new LatLng(Double.parseDouble(dropLat), Double.parseDouble(dropLng));
                    requestDirection();

//                    try {
//                        new DirectionFinder(this, pickUpLat, pickUpLng, dropLat, dropLng).execute();
//                    } catch (UnsupportedEncodingException e) {
//                        e.printStackTrace();
//                    }
                }
                //Log.d("dropLat", latitude);
                //Log.d("dropLat", longitude);
                String address2 = String.format("%s", place2.getAddress());
                //Log.d("Dest", "lisa");
                stBuilder2.append(address2);
                searchBar2.setText(stBuilder2.toString());
                //placeMarkerOnMap2(place2.getLatLng());
            }
        } else if (requestCode == 13 && resultCode == RESULT_OK) {
            wallet.setText(data.getStringExtra("type"));
        }
    }
    protected void placeMarkerOnMap(LatLng location) {
        //MarkerOptions markerOptions = new MarkerOptions().position(location);

        map.clear();
        map.addMarker(new MarkerOptions()
                .position(new LatLng(location.latitude, location.longitude)));
        CameraPosition cameraPosition = new CameraPosition.Builder().target(location).zoom(15.0f).build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        map.moveCamera(cameraUpdate);
    }
    protected void placeMarkerOnMap2(LatLng location) {
        //MarkerOptions markerOptions = new MarkerOptions().position(location);

        // map.clear();
        map.addMarker(new MarkerOptions()
                .position(new LatLng(location.latitude, location.longitude)));
        CameraPosition cameraPosition = new CameraPosition.Builder().target(location).zoom(15.0f).build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        map.moveCamera(cameraUpdate);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setPadding(20, 400, 20, 560);
        //map.setMyLocationEnabled(true);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        currentLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

            if (!pickUpLat.equalsIgnoreCase("")) {



            Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());

            List<Address> addresses = null;
            try {
                addresses = geocoder.getFromLocation(currentLocation.getLatitude(), currentLocation.getLongitude(), 1);

                pickUpLat = String.valueOf(currentLocation.getLatitude());
                pickUpLng = String.valueOf(currentLocation.getLongitude());

                getNearbyData(pickUpLat , pickUpLng);

            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (addresses.size() > 0) {
                    searchBar1.setText(addresses.get(0).getAddressLine(0) + ", " + addresses.get(0).getSubLocality() + ", " + addresses.get(0).getLocality());
                } else {
                    // do your stuff
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            progress.setVisibility(View.VISIBLE);

            final Bean b = (Bean) getApplicationContext();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(b.baseURL)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            Allapi cr = retrofit.create(Allapi.class);


            Call<dataBea> call = cr.getData(userId);

            call.enqueue(new Callback<dataBea>() {
                @Override
                public void onResponse(Call<dataBea> call, Response<dataBea> response) {

                    if (Objects.equals(response.body().getData().getStatusCode(), "3"))
                    {

                        count = 0;
                        searchBarMain.setVisibility(View.VISIBLE);

                        getNearbyData(pickUpLat, pickUpLng);

                        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude())).zoom(15.0f).build();

                        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
                        map.animateCamera(cameraUpdate);
                    }
                    else
                    {
                        count = 0;

                        bookingStatus(response.body().getData().getBookingId());


                        bookingId = response.body().getData().getBookingId();


                        searchBarMain.setVisibility(View.GONE);

                        searchBar1.setClickable(false);
                        searchBar2.setClickable(false);

                        searchBar1.setFocusable(false);
                        searchBar2.setFocusable(false);

                        carLayout.setVisibility(View.GONE);
                        confirmLayout.setVisibility(View.VISIBLE);

                    }



                    progress.setVisibility(View.GONE);


                }

                @Override
                public void onFailure(Call<dataBea> call, Throwable t) {
                    progress.setVisibility(View.GONE);
                }
            });

        }


    }

    @Override
    public void onConnectionSuspended(int i) {

        googleApiClient.connect();

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
    }

    @Override
    public void onDirectionFinderStart() {
        if (originMarkers != null) {
            for (Marker marker : originMarkers) {
                marker.remove();
            }
        }

        if (destinationMarkers != null) {
            for (Marker marker : destinationMarkers) {
                marker.remove();
            }
        }



        if (polylinePaths != null) {
            for (Polyline polyline : polylinePaths) {
                polyline.remove();
            }
        }
    }

    @Override
    public void onDirectionFinderSuccess(List<Route> routes) {
        polylinePaths = new ArrayList<>();
        originMarkers = new ArrayList<>();
        destinationMarkers = new ArrayList<>();



        builder = new LatLngBounds.Builder();

        for (Route route : routes) {
            //map.moveCamera(CameraUpdateFactory.newLatLngZoom(route.startLocation, 16));
            //((TextView) findViewById(R.id.tvDuration)).setText(route.duration.text);
            //((TextView) findViewById(R.id.tvDistance)).setText(route.distance.text);





            //builder.include(marker1.getPosition());


            //marker1.showInfoWindow();





            //marker2.showInfoWindow();


            PolylineOptions polylineOptions = new PolylineOptions().
                    geodesic(true).
                    color(Color.BLACK).
                    width(10);

            for (int i = 0; i < route.points.size(); i++) {
                polylineOptions.add(route.points.get(i));
                builder.include(route.points.get(i));
            }


            //polylinePaths.add(map.addPolyline(polylineOptions));
            polylinePaths2.add(polylineOptions);
        }

        map.clear();


        try {

            Marker marker1 = map.addMarker(new MarkerOptions()
                    .icon(bitmapDescriptorFromVector(getContext(), R.drawable.pinred))
                    .title(routes.get(0).startAddress)
                    .position(routes.get(0).startLocation));

            originMarkers.add(marker1);

        }catch (Exception e)
        {
            e.printStackTrace();
        }


        try {

            Marker marker2 = map.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
                    .title(routes.get(0).endAddress)
                    .position(routes.get(0).endLocation));


            destinationMarkers.add(marker2);

        }catch (Exception e)
        {
            e.printStackTrace();
        }


        bounds = builder.build();

        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 0);


        for (int i = 0 ; i < polylinePaths2.size() ; i++)
        {

            map.addPolyline(polylinePaths2.get(i));

        }


        map.animateCamera(cu);


    }

    @Override
    public void onDriverFinderStart() {
        if (originMarkers != null) {
            for (Marker marker : originMarkers) {
                marker.remove();
            }
        }

        if (destinationMarkers != null) {
            for (Marker marker : destinationMarkers) {
                marker.remove();
            }
        }

        if (polylinePaths != null) {
            for (Polyline polyline : polylinePaths) {
                polyline.remove();
            }
        }
    }

    @Override
    public void onDriverFinderSuccess(List<Route> routes) {

        polylinePaths = new ArrayList<>();
        originMarkers = new ArrayList<>();
        destinationMarkers = new ArrayList<>();

        //map.clear();

        builder = new LatLngBounds.Builder();

        for (Route route : routes) {
            //map.moveCamera(CameraUpdateFactory.newLatLngZoom(route.startLocation, 16));
            //((TextView) findViewById(R.id.tvDuration)).setText(route.duration.text);
            //((TextView) findViewById(R.id.tvDistance)).setText(route.distance.text);


            Marker marker1 = map.addMarker(new MarkerOptions()
                    .icon(bitmapDescriptorFromVector(getContext(), R.drawable.pinred))
                    .title(route.startAddress)
                    .position(route.startLocation));

            originMarkers.add(marker1);


            //builder.include(marker1.getPosition());


            //marker1.showInfoWindow();


            Marker marker2 = map.addMarker(new MarkerOptions()
                    .icon(bitmapDescriptorFromVector(getContext(), R.drawable.cabb))
                    .title(route.endAddress)
                    .position(route.endLocation));


            destinationMarkers.add(marker2);


            //marker2.showInfoWindow();


            PolylineOptions polylineOptions = new PolylineOptions().
                    geodesic(true).
                    color(Color.BLACK).
                    width(10);

            for (int i = 0; i < route.points.size(); i++) {
                polylineOptions.add(route.points.get(i));
                builder.include(route.points.get(i));
            }


            polylinePaths.add(map.addPolyline(polylineOptions));
        }

        bounds = builder.build();

        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 0);

        synchronized (map)
        {

            map.notifyAll();
        }

        map.animateCamera(cu);
    }


    public class CabAdapter extends RecyclerView.Adapter<CabAdapter.ViewHolder> {

        List<Estimated> cabList = new ArrayList<>();
        Context context;

        public CabAdapter(Context context, List<Estimated> cabList) {
            this.context = context;
            this.cabList = cabList;
        }

        public void setGridData(List<Estimated> cabList) {
            this.cabList = cabList;
            notifyDataSetChanged();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.cab_type_list_model, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {

            holder.setIsRecyclable(false);

            final Estimated item = cabList.get(position);

            if(item.getEstimateTime().equalsIgnoreCase("")) {
                holder.time.setText("No Rides");
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
            }else{
                holder.time.setText(item.getEstimateTime());
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        cabPosition = position;
                        selectedId = item.getTypeId();

                        filterId = item.getTypeId();

                        notifyDataSetChanged();

                    }
                });
            }

            if (cabPosition == position) {
                holder.icon.setBackgroundResource(R.drawable.backcar);
                selectedId = item.getTypeId();
                filterId = item.getTypeId();
            } else {
                holder.icon.setBackgroundResource(R.drawable.backcarwhite);
            }


            //if (cabList.size() > cabCount)
            //{

            //Log.d("asdasd", "asasd");

            holder.type.setText(item.getCabType());

            DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                    .cacheOnDisc(true).resetViewBeforeLoading(false).build();

            ImageLoader loader = ImageLoader.getInstance();
            loader.displayImage(item.getIcon(), holder.icon, options);

            Log.e("icon",item.getIcon()+"");

            //cabCount = cabList.size();

            //}
        }

        @Override
        public int getItemCount() {
            return cabList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            TextView time, type;
            ImageView icon;

            public ViewHolder(View itemView) {
                super(itemView);

                time = (TextView) itemView.findViewById(R.id.time);
                type = (TextView) itemView.findViewById(R.id.type);
                icon = (ImageView) itemView.findViewById(R.id.icon);

            }
        }

    }


    public void requestDirection() {
        //Snackbar.make(btnRequestDirection, "Direction Requesting...", Snackbar.LENGTH_SHORT).show();
        GoogleDirection.withServerKey(serverKey)
                .from(origin)
                .to(destination)
                .transportMode(TransportMode.DRIVING)
                .execute(this);


    }

    @Override
    public void onDirectionSuccess(Direction direction, String rawBody) {
        //Snackbar.make(btnRequestDirection, "Success with status : " + direction.getStatus(), Snackbar.LENGTH_SHORT).show();
        if (direction.isOK()) {
            map.clear();
            com.akexorcist.googledirection.model.Route route = direction.getRouteList().get(0);
            map.addMarker(new MarkerOptions().position(origin).icon(bitmapDescriptorFromVector(getContext(), R.drawable.pinred)));
            map.addMarker(new MarkerOptions().position(destination));

            ArrayList<LatLng> directionPositionList = route.getLegList().get(0).getDirectionPoint();
            map.addPolyline(DirectionConverter.createPolyline(getActivity(), directionPositionList, 4, Color.parseColor("#84c224")));
            setCameraWithCoordinationBounds(route);


        } else {

        }
    }

    @Override
    public void onDirectionFailure(Throwable t) {

    }

    private void setCameraWithCoordinationBounds(com.akexorcist.googledirection.model.Route route) {
        LatLng southwest = route.getBound().getSouthwestCoordination().getCoordination();
        LatLng northeast = route.getBound().getNortheastCoordination().getCoordination();
        LatLngBounds bounds = new LatLngBounds(southwest, northeast);
        map.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
    }



    public void getloctionenable()

    {
        locationManager = (LocationManager)getActivity(). getSystemService(Context.LOCATION_SERVICE);

        if( !locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.gpsenable);  // GPS not found
            builder.setMessage(R.string.mess); // Want to enable?
            builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                }
            });
            builder.setNegativeButton("No", null);
            builder.create().show();
            return;
        }

    }
    public boolean isLocationEnabled(){
        String le = Context.LOCATION_SERVICE;
        locationManager = (LocationManager) getActivity().getSystemService(le);
        if(!locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
            return false;
        } else {
            return true;
        }
    }


  public void  getlocationouser() {

        googleApiClient = new GoogleApiClient.Builder(getContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();

        googleApiClient.connect();

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(2000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult locationSettingsResult) {

                final Status status = locationSettingsResult.getStatus();

                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location requests here
                            /*if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return;
                            }
                            currentLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                            pickUpLat = currentLocation.getLatitude();*/

                            if (currentLocation==null){
                                Toast.makeText(getActivity(), "Unable to find lat and log", Toast.LENGTH_LONG).show();
                                progress.setVisibility(View.VISIBLE);
                                getlocationouser();
                            } else {
                                if (pickUpLat.equalsIgnoreCase("")) {
                                    CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude())).zoom(14.0f).build();
                                    CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
                                    map.animateCamera(cameraUpdate);
                                    getloctionenable();
                                    pickUpLat= String.valueOf(currentLocation.getLatitude());
                                    pickUpLng= String.valueOf(currentLocation.getLongitude());
                                    getNearbyData(pickUpLat , pickUpLng);
                                    //getNearbyData(pickUpLat , pickUpLng);
                                } else {
                                    if (dropLat.length() == 0) {
                                        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Double.parseDouble(pickUpLat), Double.parseDouble(pickUpLng))).zoom(14.0f).build();
                                        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
                                        map.animateCamera(cameraUpdate);
                                        getNearbyData(pickUpLat , pickUpLng);
                                    } else {
                                        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 0);
                                        map.animateCamera(cu);
                                    }
                                    //getNearbyData(pickUpLat , pickUpLng);
                                }

                            }

                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
                            } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });
    }
}