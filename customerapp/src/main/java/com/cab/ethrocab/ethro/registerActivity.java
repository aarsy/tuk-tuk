package com.cab.ethrocab.ethro;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.ethrocab.ethro.SignUpPOJO.SignupBean;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class registerActivity extends AppCompatActivity {

    TextView loginText;
    Button register;
    EditText email, phone;

    ProgressBar bar;

    SharedPreferences pref;

    SharedPreferences.Editor edit;
    ConnectionDetector cd;

    TextView referral;

    String refer = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        //getSupportActionBar().hide();

        register = (Button) findViewById(R.id.register);
        pref = getSharedPreferences("pref", Context.MODE_PRIVATE);
        //edit = pref.edit();

        referral = (TextView)findViewById(R.id.referral);

        bar = (ProgressBar) findViewById(R.id.progress);
        email = (EditText) findViewById(R.id.emailId);
        phone = (EditText) findViewById(R.id.phn);
        loginText = findViewById(R.id.loginText);
        cd = new ConnectionDetector(getApplication());
        loginText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });





        referral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(registerActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(true);
                dialog.setContentView(R.layout.promo_code_popup);
                dialog.show();


                final EditText text = (EditText)dialog.findViewById(R.id.editText);
                Button apply = (Button)dialog.findViewById(R.id.button2);

                text.setText(refer);

                apply.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        String t = text.getText().toString();

                        if (t.length() > 0)
                        {
                            refer = t;
                            referral.setText("Referral Code applied");
                            dialog.dismiss();
                        }
                        else
                        {
                            refer = t;
                            referral.setText("Have a Referral Code?");
                            dialog.dismiss();
                        }


                    }
                });


            }
        });






        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cd.isConnectingToInternet()){
                    final String e = email.getText().toString();
                    final String p = phone.getText().toString();
                    if (!TextUtils.isEmpty(e)) {


                        if (!TextUtils.isEmpty(p)) {


                            bar.setVisibility(View.VISIBLE);
                            final Bean b = (Bean) getApplicationContext();
                            Retrofit retrofit = new Retrofit.Builder()
                                    .baseUrl(b.baseURL)
                                    .addConverterFactory(ScalarsConverterFactory.create())
                                    .addConverterFactory(GsonConverterFactory.create())
                                    .build();
                            Allapi cr = retrofit.create(Allapi.class);
                            Call<SignupBean> call = cr.signup(e, p , refer);



                            call.enqueue(new Callback<SignupBean>() {
                                @Override
                                public void onResponse(Call<SignupBean> call, Response<SignupBean> response) {


                                    if (Objects.equals(response.body().getStatus(), "1"))
                                    {
                                        b.name = response.body().getData().getName();
                                        b.userId = response.body().getData().getUserId();
                                        b.phone = response.body().getData().getPhone();
                                        b.otp = response.body().getData().getOtp();

                                        try {
                                            Log.d("OTP", response.body().getData().getOtp());
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }


                                        Intent i = new Intent(registerActivity.this, otpActivity.class);
                                        i.putExtra("OTP", response.body().getData().getOtp());
                                        i.putExtra("Email",email.getText().toString());
                                        i.putExtra("Phone",phone.getText().toString());
                                        startActivity(i);
                                        finish();

                                        Toast.makeText(registerActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                    else
                                    {
                                        Toast.makeText(registerActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                    }


                                    bar.setVisibility(View.GONE);


                                }

                                @Override
                                public void onFailure(Call<SignupBean> call, Throwable t) {

                                    bar.setVisibility(View.GONE);
                                }
                            });


                        } else {
                            phone.setError("Field is Empty");
                            phone.requestFocus();
                        }

                    } else {
                        email.setError("Field is Empty");
                        email.requestFocus();
                    }
                }else {
                    Toast.makeText(registerActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();

                }

            }
        });
    }
}
