package com.cab.ethrocab.ethro;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class FAQData extends AppCompatActivity {


    Toolbar toolbar;
    RecyclerView grid;
    ProgressBar progress;

    String id;

    GridLayoutManager manager;

    FAQAdapter adapter;
    List<subTopicBean> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faqdata);

        list = new ArrayList<>();

        id = getIntent().getStringExtra("id");

        toolbar = findViewById(R.id.toolbar);
        grid = findViewById(R.id.grid);
        progress = findViewById(R.id.progress);

        adapter = new FAQAdapter(this , list);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.arrow);
        toolbar.setTitle("Learn More");
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });

        manager = new GridLayoutManager(this , 1);


        grid.setAdapter(adapter);
        grid.setLayoutManager(manager);


        progress.setVisibility(View.VISIBLE);

        final Bean b = (Bean) getApplicationContext();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(b.baseURL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Allapi cr = retrofit.create(Allapi.class);


        Call<List<subTopicBean>> call = cr.getSubFaqTopics(id);

        call.enqueue(new Callback<List<subTopicBean>>() {
            @Override
            public void onResponse(Call<List<subTopicBean>> call, Response<List<subTopicBean>> response) {



                adapter.setGridData(response.body());



                progress.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<List<subTopicBean>> call, Throwable t) {
                progress.setVisibility(View.GONE);
            }
        });




    }



    class FAQAdapter extends RecyclerView.Adapter<FAQAdapter.ViewHolder>
    {

        List<subTopicBean> list = new ArrayList<>();
        Context context;


        public FAQAdapter(Context context , List<subTopicBean> list)
        {
            this.list = list;
            this.context = context;
        }

        public void setGridData(List<subTopicBean> list)
        {
            this.list = list;
            notifyDataSetChanged();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.faq_model , parent , false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {

            subTopicBean item = list.get(position);


            holder.text.setText(Html.fromHtml(item.getText()));


        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder
        {

            TextView text;

            public ViewHolder(View itemView) {
                super(itemView);

                text = (TextView)itemView.findViewById(R.id.text);

            }
        }

    }



}
