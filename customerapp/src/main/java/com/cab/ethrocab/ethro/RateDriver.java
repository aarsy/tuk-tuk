package com.cab.ethrocab.ethro;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import github.nisrulz.easydeviceinfo.base.EasyLocationMod;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static com.facebook.FacebookSdk.CALLBACK_OFFSET_PROPERTY;
import static com.facebook.FacebookSdk.getApplicationContext;

public class RateDriver extends AppCompatActivity {



    CircleImageView image;
    TextView rat , name , number , amount;
    RatingBar rating;
    Button submit;

    ProgressBar progress;
    ConnectionDetector cd;
    String lat, lon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_driver);


        image = (CircleImageView)findViewById(R.id.profileImage);
        rat = (TextView)findViewById(R.id.rat);
        name = (TextView)findViewById(R.id.name);
        number = (TextView)findViewById(R.id.number);
        amount = (TextView)findViewById(R.id.estimated);
        rating = (RatingBar)findViewById(R.id.rating);
        submit = (Button)findViewById(R.id.submit);
        //rating.setRating(Float.parseFloat(getIntent().getStringExtra("rating")));

        progress = (ProgressBar)findViewById(R.id.progress);

        ImageLoader loader = ImageLoader.getInstance();
        loader.displayImage(getIntent().getStringExtra("image") , image);

        name.setText(getIntent().getStringExtra("name"));
        rat.setText(getIntent().getStringExtra("rating"));
        number.setText(getIntent().getStringExtra("type"));
       // amount.setText(getIntent().getStringExtra("amount"));
        try {

            EasyLocationMod easyLocationMod = new EasyLocationMod(RateDriver.this);
            double[] l = easyLocationMod.getLatLong();
            lat = String.valueOf(l[0]);
            lon = String.valueOf(l[1]);

        }catch (SecurityException se){

        }

        getFare(lat,lon);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                progress.setVisibility(View.VISIBLE);
                final Bean b = (Bean) getApplicationContext();
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(b.baseURL)
                        .addConverterFactory(ScalarsConverterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                Allapi cr = retrofit.create(Allapi.class);


                Call<String> call = cr.rateRide(b.userId , getIntent().getStringExtra("did") , String.valueOf(rating.getRating()));

                call.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {

                        Toast.makeText(RateDriver.this , "Rating Successfully Submitted" , Toast.LENGTH_SHORT).show();

                        progress.setVisibility(View.GONE);

                        Intent intent = new Intent(RateDriver.this , MainActivity.class);
                        startActivity(intent);
                        finish();

                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        progress.setVisibility(View.GONE);
                    }
                });



            }
        });




        //mSupportMapFragment = (SupportMapFragment) getApplicationContext().findFragmentById(R.id.bookRideFragment);
        /*if (mSupportMapFragment == null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            mSupportMapFragment = SupportMapFragment.newInstance();
            fragmentTransaction.replace(R.id.activi, mSupportMapFragment).commit();
        }*/

        /*if (mSupportMapFragment != null) {
            mSupportMapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(final GoogleMap googleMap) {
                    if (googleMap != null) {

                        googleMap.getUiSettings().setAllGesturesEnabled(true);

                        EasyLocationMod easyLocationMod = new EasyLocationMod(RateDriver.this);

                        if (ActivityCompat.checkSelfPermission(RateDriver.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(RateDriver.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }

                        double[] l = easyLocationMod.getLatLong();
                        String lat = String.valueOf(l[0]);
                        String lon = String.valueOf(l[1]);

                        LatLng myLocation = new LatLng(Double.parseDouble(lat), Double.parseDouble(lon));
                        Geocoder geocoder = new Geocoder(RateDriver.this, Locale.getDefault());
                        try {
                            List<Address> listAdresses = geocoder.getFromLocation(Double.parseDouble(lat), Double.parseDouble(lon), 1);
                            if (null != listAdresses && listAdresses.size() > 0) {
                                String address = listAdresses.get(0).getAddressLine(0);
                                String state = listAdresses.get(0).getAdminArea();
                                String country = listAdresses.get(0).getCountryName();
                                String subLocality = listAdresses.get(0).getSubLocality();

                                googleMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(Double.parseDouble(lat), Double.parseDouble(lon)))
                                        .title("" + subLocality + ", " + state + ", " + country + "")
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker)));
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        googleMap.setMyLocationEnabled(false);
                        CameraPosition cameraPosition = new CameraPosition.Builder().target(myLocation).zoom(15.0f).build();
                        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
                        googleMap.moveCamera(cameraUpdate);


                    }

                }

            });


        }*/


    }


    public void getFare(final String lat, final String longi)    {
        final Bean b = (Bean) getApplicationContext();
        Toast.makeText(RateDriver.this,"OK",Toast.LENGTH_SHORT).show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, b.baseURL+"api/user_finished_ride.php", new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String s) {

                Log.e("cashtobepaid",""+s);
                try {
                    JSONObject jsonObject = new JSONObject(s);

                    amount.setText(""+jsonObject.getJSONObject("data").getString("finalPrice"));


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {


            }
        })
        {
            @Override  protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String,String> params = new HashMap<>();
                params.put("userId",""+getIntent().getStringExtra("uid"));
                params.put("bookingId",""+getIntent().getStringExtra("bid"));
                params.put("latitude",""+lat);
                params.put("longitude",""+longi);
                return params;
            }

        };
        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);

        Volley.newRequestQueue(RateDriver.this).add(stringRequest);
        // requestQueue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(RateDriver.this , MainActivity.class);
        startActivity(intent);
        finish();



    }
}
