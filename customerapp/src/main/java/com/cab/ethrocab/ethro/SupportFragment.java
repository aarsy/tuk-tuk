package com.cab.ethrocab.ethro;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cab.ethrocab.ethro.AddWalletPOJO.AddWalletBean;
import com.cab.ethrocab.ethro.GetWalletPOJO.GetWalletBean;
import com.cab.ethrocab.ethro.couponCodePOJO.couponCodeBean;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

import instamojo.library.InstamojoPay;
import instamojo.library.InstapayListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class SupportFragment extends Fragment {

    Toolbar toolbar;

    ProgressBar bar;
    TextView money ;
    EditText amount;
    Button add;
    ConnectionDetector cd;

    TextView promo;

    String am;

    SharedPreferences pref;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_support, container, false);


        pref = getContext().getSharedPreferences("pref" , Context.MODE_PRIVATE);


        promo = (TextView)view.findViewById(R.id.referral);

        bar = (ProgressBar)view.findViewById(R.id.progress);
        money = (TextView)view.findViewById(R.id.money);
        cd = new ConnectionDetector(getContext());
        amount = (EditText)view.findViewById(R.id.amount);
        add = (Button)view.findViewById(R.id.add);


        if (cd.isConnectingToInternet()){

            bar.setVisibility(View.VISIBLE);

            final Bean b = (Bean) getContext().getApplicationContext();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(b.baseURL)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            Allapi cr = retrofit.create(Allapi.class);
            Log.d("UserId",b.userId);
            Call<GetWalletBean> call = cr.wallet(b.userId);
            call.enqueue(new Callback<GetWalletBean>() {
                @Override
                public void onResponse(Call<GetWalletBean> call, Response<GetWalletBean> response) {
                    if (Objects.equals(response.body().getStatus(), "1")){

                        bar.setVisibility(View.GONE);
                        money.setText("₹ " + response.body().getData().getAmount());
                        amount.setText("");

                    }else {
                        bar.setVisibility(View.GONE);
                        Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<GetWalletBean> call, Throwable t) {

                    bar.setVisibility(View.GONE);
                }
            });

        }else {

            Toast.makeText(getContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
        }


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (cd.isConnectingToInternet()){

                    String m = amount.getText().toString();
                    if (!TextUtils.isEmpty(m)){


                        am = m;


                        String email = pref.getString("email" , "");
                        String phone = pref.getString("phone" , "");
                        String name = pref.getString("name" , "");


                        //callInstamojoPay(email , phone , m , "wallet" , name);
//                        Intent in = new Intent(getActivity(),PaymentMainActivity.class);
//                        in.putExtra("nane", name);
//                        in.putExtra("email", email);
//                        in.putExtra("amount", m);
//                        in.putExtra("phone", phone);
//                        getActivity().startActivity(in);

                        Intent i = new Intent(getActivity(),AvantgardeSdkUsageActivity.class);
                        i.putExtra("totalprice",""+m);
                        i.putExtra("nane", name);
                        i.putExtra("email", email);
                        i.putExtra("phone", phone);
                        startActivity(i);


                    }else {
                        amount.setError("Enter some amount");
                        amount.requestFocus();
                    }

                }else {
                    Toast.makeText(getContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });



        promo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(true);
                dialog.setContentView(R.layout.coupon_code_opup);
                dialog.show();

                final EditText text = (EditText)dialog.findViewById(R.id.editText);
                Button apply = (Button)dialog.findViewById(R.id.button2);
                final ProgressBar progress = (ProgressBar)dialog.findViewById(R.id.progressBar2);

                apply.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                        String c = text.getText().toString();

                        if (c.length() > 0)
                        {
                            progress.setVisibility(View.VISIBLE);
                            final Bean b = (Bean) getContext().getApplicationContext();
                            Retrofit retrofit = new Retrofit.Builder()
                                    .baseUrl(b.baseURL)
                                    .addConverterFactory(ScalarsConverterFactory.create())
                                    .addConverterFactory(GsonConverterFactory.create())
                                    .build();
                            Allapi cr = retrofit.create(Allapi.class);


                            Call<couponCodeBean> call = cr.applyCoupon(b.userId , c);

                            call.enqueue(new Callback<couponCodeBean>() {
                                @Override
                                public void onResponse(Call<couponCodeBean> call, Response<couponCodeBean> response) {


                                    if (response.body().getStatus() == 1)
                                    {


                                        bar.setVisibility(View.VISIBLE);


                                        final Bean b = (Bean) getContext().getApplicationContext();


                                        Retrofit retrofit = new Retrofit.Builder()
                                                .baseUrl(b.baseURL)
                                                .addConverterFactory(ScalarsConverterFactory.create())
                                                .addConverterFactory(GsonConverterFactory.create())
                                                .build();
                                        Allapi cr = retrofit.create(Allapi.class);
                                        Log.d("UserId",b.userId);
                                        Call<GetWalletBean> cal1l = cr.wallet(b.userId);
                                        cal1l.enqueue(new Callback<GetWalletBean>() {
                                            @Override
                                            public void onResponse(Call<GetWalletBean> call, Response<GetWalletBean> response) {
                                                if (Objects.equals(response.body().getStatus(), "1")){

                                                    bar.setVisibility(View.GONE);
                                                    money.setText("₹ " + response.body().getData().getAmount());
                                                    amount.setText("");

                                                }else {
                                                    bar.setVisibility(View.GONE);
                                                    Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<GetWalletBean> call, Throwable t) {

                                                bar.setVisibility(View.GONE);
                                            }
                                        });



                                        Toast.makeText(getContext() , response.body().getMessage() , Toast.LENGTH_SHORT).show();
                                        dialog.dismiss();

                                    }
                                    else
                                    {
                                        Toast.makeText(getContext() , response.body().getMessage() , Toast.LENGTH_SHORT).show();
                                    }


                                    progress.setVisibility(View.GONE);

                                }

                                @Override
                                public void onFailure(Call<couponCodeBean> call, Throwable t) {
                                    progress.setVisibility(View.GONE);
                                }
                            });
                        }
                        else
                        {
                            text.setError("Invalid Code");
                        }

                    }
                });

            }
        });



        return view;
    }



    @SuppressLint("CommitPrefEdits")

    private void callInstamojoPay(String email, String phone, String amount, String purpose, String buyername) {
        final Activity activity = getActivity();
        InstamojoPay instamojoPay = new InstamojoPay();
        IntentFilter filter = new IntentFilter("ai.devsupport.instamojo");
        getContext().registerReceiver(instamojoPay, filter);
        JSONObject pay = new JSONObject();
        try {
            pay.put("email", email);
            pay.put("phone", phone);
            pay.put("purpose", purpose);
            pay.put("amount", amount);
            pay.put("name", buyername);
            pay.put("send_sms", true);
            pay.put("send_email", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initListener();
        instamojoPay.start(activity, pay, listener);
    }

    InstapayListener listener;


    private void initListener() {
        listener = new InstapayListener() {
            @Override
            public void onSuccess(String response) {

                bar.setVisibility(View.VISIBLE);





                /*Toast.makeText(getContext().getApplicationContext(), response, Toast.LENGTH_LONG)
                        .show();*/
            }

            @Override
            public void onFailure(int code, String reason) {
                try {
                    Toast.makeText(getContext().getApplicationContext(), "Failed: " + reason, Toast.LENGTH_LONG)
                            .show();
                }catch (Exception e)
                {
                    e.printStackTrace();
                }

            }
        };
    }



}
