package com.cab.ethrocab.ethro;


import com.cab.ethrocab.ethro.AddWalletPOJO.AddWalletBean;
import com.cab.ethrocab.ethro.EstimatedFarePOJO.EstimatedBean;
import com.cab.ethrocab.ethro.EstimatedTimePOJO.TimeBean;
import com.cab.ethrocab.ethro.GetWalletPOJO.GetWalletBean;
import com.cab.ethrocab.ethro.NearByPOJO.NearByBean;
import com.cab.ethrocab.ethro.SignInPOJO.SignInBean;
import com.cab.ethrocab.ethro.SignUpPOJO.SignupBean;
import com.cab.ethrocab.ethro.SocialPOJO.SocialBean;
import com.cab.ethrocab.ethro.UpdateNamePOJO.UpdateNameBean;
import com.cab.ethrocab.ethro.UpdatePhonePOJO.UpdatePhoneBean;
import com.cab.ethrocab.ethro.UpdatePicPOJO.UpdatePicBean;
import com.cab.ethrocab.ethro.VerifyOtpPOJO.VerifyBean;
import com.cab.ethrocab.ethro.cancelBookingPOJO.cancelBookingBean;
import com.cab.ethrocab.ethro.checkBookingPOJO.checkBookingBean;
import com.cab.ethrocab.ethro.couponCodePOJO.couponCodeBean;
import com.cab.ethrocab.ethro.dataPOJO.dataBea;
import com.cab.ethrocab.ethro.getOwnPromoPOJO.getOwnPromoBean;
import com.cab.ethrocab.ethro.rideHistoryPOJO.RideHistoryBean;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by USER on 11/30/2017.
 */

public interface Allapi {

    @Multipart
    @POST("api/sign_up.php")
    Call<SignupBean> signup(
            @Part("email") String m,
            @Part("phone") String P,
            @Part("referralCode") String r

    );

    @Multipart
    @POST("api/varify_code.php")
    Call<VerifyBean> otp(
            @Part("otp") String mo,
            @Part("userId") String Po
    );


    @Multipart
    @POST("api/mobile_signin.php")
    Call<SignInBean> signin(
            @Part("phone") String mo
    );


    @Multipart
    @POST("api/socialsign_up.php")
    Call<SocialBean> social(
            @Part("email") String mo,
            @Part("pid") String mp,
            @Part("name") String mn

    );

    @Multipart
    @POST("api/nearbycab.php")
    Call<NearByBean> nearBy(
            @Part("userId") String mo,
            @Part("latitude") String mp,
            @Part("longitude") String mn

    );

    @Multipart
    @POST("api/estimateTime.php")
    Call<TimeBean> time(
            @Part("userId") String mo,
            @Part("pickupLat") String mp,
            @Part("pickupLong") String mn,
            @Part("cabType") String m
    );

    @Multipart
    @POST("api/estimateFare.php")
    Call<EstimatedBean> getEstimatedFare(
            @Part("userId") String userid,
            @Part("pickupLat") String pickuplat,
            @Part("pickupLong") String pickuplong,
            @Part("dropLat") String droplat,
            @Part("dropLong") String droplong,
            @Part("cabType") String cabtype
    );


    @Multipart
    @POST("api/bookingConfirm.php")
    Call<EstimatedBean> confirm(
            @Part("userId") String userid,
            @Part("pickupLat") String pickuplat,
            @Part("pickupLong") String pickuplong,
            @Part("dropLat") String droplat,
            @Part("dropLong") String droplong,
            @Part("cabType") String cabtype,
            @Part("estimatedPrice") String estimatedPrice,
            @Part("paymentMode") String paymentMode
    );

    @Multipart
    @POST("api/getBookingStatus.php")
    Call<dataBea> getData(
            @Part("userId") String mo);

    @Multipart
    @POST("api/checkBooking.php")
    Call<checkBookingBean> checkBooking(
            @Part("userId") String userId,
            @Part("bookingId") String bookingId
    );


    @Multipart
    @POST("api/cancelBooking.php")
    Call<cancelBookingBean> cancelBooking(
            @Part("userId") String userId,
            @Part("bookingId") String bookingId,
            @Part("reason") String reson
    );


    @Multipart
    @POST("api/rateRide.php")
    Call<String> rateRide(
            @Part("userId") String userId,
            @Part("driverId") String driverId,
            @Part("rating") String rating
    );


    @Multipart
    @POST("api/update_user_info.php")
    Call<UpdateNameBean> update(
            @Part("userId") String mo,
            @Part("name") String mp
    );


    @Multipart
    @POST("api/add_coupon_value.php")
    Call<couponCodeBean> applyCoupon(
            @Part("userId") String mo,
            @Part("code") String mp
    );


    @Multipart
    @POST("api/update_phone.php")
    Call<UpdatePhoneBean> updateph(
            @Part("userId") String mo,
            @Part("phone") String mp,
            @Part("referralCode") String r
    );


    @Multipart
    @POST("api/get-refer-code.php")
    Call<getOwnPromoBean> getOwnPromo(
            @Part("userId") String mo
    );


    @Multipart
    @POST("api/subTopics.php")
    Call<List<subTopicBean>> getSubTopics(
            @Part("topicId") String mo
    );


    @Multipart
    @POST("api/subFaqTopics.php")
    Call<List<subTopicBean>> getSubFaqTopics(
            @Part("topicId") String mo
    );


    @GET("api/getTopics.php")
    Call<List<topicBean>> getTopics();


    @GET("api/getFaqTopics.php")
    Call<List<topicBean>> getFaqTopics();


    @Multipart
    @POST("api/ride_history.php")
    Call<RideHistoryBean> history(
            @Part("userId") String mo
    );

    @Multipart
    @POST("api/getwallet.php")
    Call<GetWalletBean> wallet(
            @Part("userId") String mo
    );


    @Multipart
    @POST("api/topic_query.php")
    Call<String> submitQuery(
            @Part("userId") String mo,
            @Part("topicId") String t,
            @Part("subTopicId") String s
    );


    @Multipart
    @POST("api/addUserWallet.php")
    Call<AddWalletBean> addWallet(
            @Part("userId") String mo,
            @Part("amount") String m
    );

    @Multipart
    @POST("api/update_pic.php")
    Call<UpdatePicBean> pic(
            @Part("userId") String userId,
            @Part MultipartBody.Part file
    );

}
