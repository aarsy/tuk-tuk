package com.cab.ethrocab.ethro;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.cab.ethrocab.ethro.rideHistoryPOJO.History;
import com.cab.ethrocab.ethro.rideHistoryPOJO.RideHistoryBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RideHistoryFragment extends Fragment {

    RecyclerView grid;
    historyCardAdapter adapter;
    GridLayoutManager manager;
    List<History> list;
    ProgressBar bar;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    String userId;
    ConnectionDetector cd;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_ride_history , container , false);

        grid = (RecyclerView) view.findViewById(R.id.grid);
        bar = (ProgressBar) view.findViewById(R.id.progress);

        grid = view.findViewById(R.id.grid);
        manager = new GridLayoutManager(getContext() , 1);
        list = new ArrayList<>();
        adapter = new historyCardAdapter(getContext(),list);
        grid.setAdapter(adapter);
        grid.setLayoutManager(manager);
        userId = getArguments().getString("userId");
        cd = new ConnectionDetector(getContext());

        if (cd.isConnectingToInternet()){

            bar.setVisibility(View.VISIBLE);

            Bean b = (Bean) getContext().getApplicationContext();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(b.baseURL)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            Allapi cr = retrofit.create(Allapi.class);
            Call<RideHistoryBean> call = cr.history(userId);
            Log.d("UserId", userId);
            call.enqueue(new Callback<RideHistoryBean>() {
                @Override
                public void onResponse(Call<RideHistoryBean> call, Response<RideHistoryBean> response) {
                    if (Objects.equals(response.body().getStatus(), 1)){
                        // Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        adapter.setgrid(response.body().getData().getHistory());

                    }
                    bar.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<RideHistoryBean> call, Throwable t) {

                    bar.setVisibility(View.GONE);
                }
            });


        }else {
            Toast.makeText(getContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();

        }


        return view;

    }


}
