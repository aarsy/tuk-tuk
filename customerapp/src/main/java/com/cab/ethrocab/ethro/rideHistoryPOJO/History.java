package com.cab.ethrocab.ethro.rideHistoryPOJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by faizan on 2/13/2018.
 */

public class History {
    @SerializedName("pickupLat")
    @Expose
    private String pickupLat;
    @SerializedName("pickupLong")
    @Expose
    private String pickupLong;
    @SerializedName("dropLat")
    @Expose
    private String dropLat;
    @SerializedName("dropLong")
    @Expose
    private String dropLong;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("cabTypeId")
    @Expose
    private String cabTypeId;
    @SerializedName("cabTypeName")
    @Expose
    private String cabTypeName;
    @SerializedName("cabNumber")
    @Expose
    private String cabNumber;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("bookingStatus")
    @Expose
    private String bookingStatus;
    @SerializedName("driverName")
    @Expose
    private String driverName;
    @SerializedName("driverRate")
    @Expose
    private String driverRate;
    @SerializedName("image")
    @Expose
    private String image;

    public String getPickupLat() {
        return pickupLat;
    }

    public void setPickupLat(String pickupLat) {
        this.pickupLat = pickupLat;
    }

    public String getPickupLong() {
        return pickupLong;
    }

    public void setPickupLong(String pickupLong) {
        this.pickupLong = pickupLong;
    }

    public String getDropLat() {
        return dropLat;
    }

    public void setDropLat(String dropLat) {
        this.dropLat = dropLat;
    }

    public String getDropLong() {
        return dropLong;
    }

    public void setDropLong(String dropLong) {
        this.dropLong = dropLong;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCabTypeId() {
        return cabTypeId;
    }

    public void setCabTypeId(String cabTypeId) {
        this.cabTypeId = cabTypeId;
    }

    public String getCabTypeName() {
        return cabTypeName;
    }

    public void setCabTypeName(String cabTypeName) {
        this.cabTypeName = cabTypeName;
    }

    public String getCabNumber() {
        return cabNumber;
    }

    public void setCabNumber(String cabNumber) {
        this.cabNumber = cabNumber;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverRate() {
        return driverRate;
    }

    public void setDriverRate(String driverRate) {
        this.driverRate = driverRate;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}
