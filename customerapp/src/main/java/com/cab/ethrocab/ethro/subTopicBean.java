package com.cab.ethrocab.ethro;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class subTopicBean {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("topicId")
    @Expose
    private String topicId;
    @SerializedName("text")
    @Expose
    private String text;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
