package com.cab.ethrocab.ethro;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class FAQs extends AppCompatActivity {

    Toolbar toolbar;
    ProgressBar progress;
    RecyclerView grid;
    GridLayoutManager manager;
    List<topicBean> list;
    FAQAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faqs);

        list = new ArrayList<>();

        progress = findViewById(R.id.progress);
        toolbar = findViewById(R.id.toolbar);
        grid = findViewById(R.id.grid);


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.arrow);
        toolbar.setTitle("FAQs");
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });


        manager = new GridLayoutManager(this , 1);


        adapter = new FAQAdapter(this , list);

        grid.setAdapter(adapter);
        grid.setLayoutManager(manager);





        progress.setVisibility(View.VISIBLE);

        final Bean b = (Bean) getApplicationContext();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(b.baseURL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Allapi cr = retrofit.create(Allapi.class);


        Call<List<topicBean>> call = cr.getFaqTopics();


        call.enqueue(new Callback<List<topicBean>>() {
            @Override
            public void onResponse(Call<List<topicBean>> call, Response<List<topicBean>> response) {


                adapter.setGridData(response.body());
                progress.setVisibility(View.GONE);


            }

            @Override
            public void onFailure(Call<List<topicBean>> call, Throwable t) {
                progress.setVisibility(View.GONE);
            }
        });





    }


    class FAQAdapter extends RecyclerView.Adapter<FAQAdapter.ViewHolder>
    {

        Context context;
        List<topicBean> list = new ArrayList<>();



        public FAQAdapter(Context context , List<topicBean> list)
        {
            this.context = context;
            this.list = list;
        }


        public void setGridData(List<topicBean> list)
        {
            this.list = list;
            notifyDataSetChanged();
        }


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.faq_model , parent , false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {

            final topicBean item = list.get(position);

            holder.text.setText(item.getText());

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context , FAQData.class);
                    intent.putExtra("id" , item.getId());
                    context.startActivity(intent);
                }
            });

        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder
        {

            TextView text;

            public ViewHolder(View itemView) {
                super(itemView);

                text = (TextView)itemView.findViewById(R.id.text);

            }
        }

    }


}
