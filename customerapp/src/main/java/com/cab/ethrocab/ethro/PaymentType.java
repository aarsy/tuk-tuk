package com.cab.ethrocab.ethro;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.cab.ethrocab.ethro.GetWalletPOJO.GetWalletBean;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class PaymentType extends AppCompatActivity {

    Toolbar toolbar;
    RadioGroup group;
    Button submit;
    ConnectionDetector cd;
    String amnt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_type);

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        group = (RadioGroup)findViewById(R.id.group);
        submit = (Button)findViewById(R.id.submit);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(R.drawable.arrow);

        cd = new ConnectionDetector(this);

        toolbar.setTitle("Payment Options");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        if (cd.isConnectingToInternet()){


            // bar.setVisibility(View.VISIBLE);


            final Bean b = (Bean) getApplicationContext();


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(b.baseURL)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            Allapi cr = retrofit.create(Allapi.class);
            Log.d("UserId",b.userId);
            Call<GetWalletBean> call = cr.wallet(b.userId);
            call.enqueue(new Callback<GetWalletBean>() {
                @Override
                public void onResponse(Call<GetWalletBean> call, Response<GetWalletBean> response) {
                    if (Objects.equals(response.body().getStatus(), "1")) {

                        amnt = response.body().getData().getAmount();
                        //amnt = "0";
                                //Toast.makeText(PaymentType.this, ""+amnt, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<GetWalletBean> call, Throwable t) {

                    //bar.setVisibility(View.GONE);
                }
            });

        }else {

            Toast.makeText(PaymentType.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final RadioButton btn = (RadioButton)findViewById(group.getCheckedRadioButtonId());

                Log.d("checkedId" , btn.getText().toString());
                if(btn.getText().toString().equalsIgnoreCase("wallet")) {

                    if(amnt.equalsIgnoreCase("")||amnt.equalsIgnoreCase("0.0")||amnt.equalsIgnoreCase("0.00")||amnt.equalsIgnoreCase("0")){

                        Toast.makeText(PaymentType.this, "No money in wallet!!", Toast.LENGTH_SHORT).show();

                        Intent resultIntent = new Intent();
                        resultIntent.putExtra("type", "Cash");
                        setResult(Activity.RESULT_OK, resultIntent);
                        finish();

                    }else{
                        Intent resultIntent = new Intent();
                        resultIntent.putExtra("type", btn.getText().toString());
                        setResult(Activity.RESULT_OK, resultIntent);
                        finish();
                    }

                }else{
                    Intent resultIntent = new Intent();
                    resultIntent.putExtra("type", btn.getText().toString());
                    setResult(Activity.RESULT_OK, resultIntent);
                    finish();
                }

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent resultIntent = new Intent();
        resultIntent.putExtra("type", "");

        setResult(Activity.RESULT_CANCELED, resultIntent);
        finish();

    }
}
