package com.cab.ethrocab.ethro;

import java.util.List;

/**
 * Created by Mukul on 3/7/2018.
 */

public interface DriverFinderListener {
    void onDriverFinderStart();
    void onDriverFinderSuccess(List<Route> route);
}
