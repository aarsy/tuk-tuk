package com.cab.ethrocab.ethro;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.UrlQuerySanitizer;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cab.ethrocab.ethro.AddWalletPOJO.AddWalletBean;
import com.cab.ethrocab.ethro.GetWalletPOJO.GetWalletBean;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.Key;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

@TargetApi(Build.VERSION_CODES.GINGERBREAD)
public class PaymentGateActivity extends Activity {

    String amt ;

    private static final String ENCRYPTION_IV = "0123456789abcdef";
    private static final String PADDING = "AES/CBC/PKCS5Padding";
    private static final String ALGORITHM = "AES";
    private static final String CHARTSET = "UTF-8";
    private static final String ENCRYPT_KEY = "HFPjzSOlvVOIFucx/Ej6S6uHPLfRXFbzgw8BJCspItk=";//testing
    //private static final String ENCRYPT_KEY = "ihxMgtTf8b0tVAYdNeYgnQQiFBwvBRr72i4onTlGAA4=";//Paygate India Pvt Ltd

    String orderid="";
    String user_id;
    SharedPreferences sharedPreferences;


    //private static final String mainurl = "https://test.avantgardepayments.com/agcore/payment";//Testing Url
    private static final String mainurl = "https://www.avantgardepayments.com/agcore/payment";//Live Url
//    private static final String successurl = "https://test.avantgardepayments.com/test-transaction/MerchantSuccess.php";
//    private static final String failureurl = "https://test.avantgardepayments.com/test-transaction/MerchantFailure.php";
    private static final String successurl = "https://test.avantgardepayments.com/prod-transaction/MerchantSuccess.php";
    private static final String failureurl = "https://test.avantgardepayments.com/prod-transaction/MerchantFailure.php";

    WebView webview;
    ProgressDialog progressDialog;
    RequestQueue requestQueue;
    RequestQueue requestQueue1;
    String ME_ID, txndetails, pgdetails, carddetails, custdetails, billdetails, shipdetails, itemdetails, otherdetails;
    String test_id = "phani";

    private static AlgorithmParameterSpec makeIv() {
        try {
            return new IvParameterSpec(ENCRYPTION_IV.getBytes(CHARTSET));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void webview_ClientPost(WebView webView, String url, Collection<Map.Entry<String, String>> postData) {
        StringBuilder sb = new StringBuilder();

        sb.append("<html><head></head>");
        sb.append("<body onload='form1.submit()'>");
        sb.append(String.format("<form id='form1' action='%s' method='%s'>", url, "post"));
        for (Map.Entry<String, String> item : postData) {
            sb.append(String.format("<input name='%s' type='hidden' value='%s' />", item.getKey(), item.getValue()));
        }
        sb.append("</form></body></html>");

        webView.loadData(sb.toString(), "text/html", "UTF-8");
    }

    @SuppressLint("TrulyRandom")
    public static void handleSSLHandshake() {
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }

                @Override
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                @Override
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }};

            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String arg0, SSLSession arg1) {
                    return true;
                }
            });
        } catch (Exception ignored) {
        }
    }

    private Key makeKey(String encryptionKey) {
        try {
            byte[] key = Base64.decodeBase64(encryptionKey.getBytes());
            return new SecretKeySpec(key, ALGORITHM);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public String encrypt(String textToEncrypt, String key) {
        try {

            Cipher cipher = Cipher.getInstance(PADDING);
            cipher.init(Cipher.ENCRYPT_MODE, makeKey(key), makeIv());
            return new String(Base64.encodeBase64(cipher.doFinal(textToEncrypt.getBytes())));

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public String decrypt(String textToDecrypt, String key) {
        String DEcrypted = "";
        try {

            byte[] resp = Base64.decodeBase64(textToDecrypt.getBytes());
            Cipher cipher = Cipher.getInstance(PADDING);
            cipher.init(Cipher.DECRYPT_MODE, makeKey(key), makeIv());
            byte[] decryptedAESBytes = new byte[0];
            decryptedAESBytes = cipher.doFinal(resp);
            DEcrypted = new String(decryptedAESBytes, "UTF-8");
            return DEcrypted;

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.paymentgateact);
        requestQueue1 = Volley.newRequestQueue(this);
        sharedPreferences = getSharedPreferences("User_id", MODE_PRIVATE);
        user_id = sharedPreferences.getString("user_id","");
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        String me_id = this.getIntent().getStringExtra("ME_ID");
        String tx_details = this.getIntent().getStringExtra("TXN_DETAILS");
        String pg_details = this.getIntent().getStringExtra("PG_DETAILS");
        String card_details = this.getIntent().getStringExtra("CARD_DETAILS");
        String cust_details = this.getIntent().getStringExtra("CUSTOMER_DETAILS");
        String bill_details = this.getIntent().getStringExtra("BILLING_DETAILS");
        String ship_details = this.getIntent().getStringExtra("SHIPPING_DETAILS");
        String item_details = this.getIntent().getStringExtra("ITEM_DETAILS");
        String other_details = this.getIntent().getStringExtra("OTHER_DETAILS");
        amt = ""+getIntent().getStringExtra("totalprice");

        System.out.println("ME_ID :" + me_id);
        System.out.println("TXN_DETAILS :" + tx_details);
        System.out.println("PG_DETAILS :" + pg_details);
        System.out.println("CARD_DETAILS :" + card_details);
        System.out.println("CUSTOMER_DETAILS :" + cust_details);
        System.out.println("BILLING_DETAILS :" + bill_details);
        System.out.println("SHIPPING_DETAILS :" + ship_details);
        System.out.println("ITEM_DETAILS :" + item_details);
        System.out.println("OTHER_DETAILS :" + other_details);

        //Production
        productionCall();

        //Testing
        // testingCall();

        callCheckout(me_id, tx_details, pg_details, card_details, cust_details, bill_details, ship_details, item_details, other_details);

    }


    private void productionCall() {
        try {
            requestQueue = getPinnedRequestQueue(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void testingCall() {
        handleSSLHandshake();
        requestQueue = new Volley().newRequestQueue(this);
    }

    private void callCheckout(String me_id, String txnde, String pgde, String cardde, String custde, String buildde, String shipde, String itemde, String otherde) {
        final ProgressDialog pDialog;
        pDialog = new ProgressDialog(PaymentGateActivity.this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Please Wait...");
        pDialog.show();
        encodeParams(me_id, txnde, pgde, cardde, custde, buildde, shipde, itemde, otherde);

        StringRequest jsonObjRequest = new StringRequest(Request.Method.POST,
                mainurl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            webview = (WebView) findViewById(R.id.web_view);
                            webview.getSettings().setJavaScriptEnabled(true);
                            webview.getSettings().setDomStorageEnabled(true);
                            webview.getSettings().setLoadWithOverviewMode(true);
                            webview.getSettings().setUseWideViewPort(true);
                            webview.getSettings().setMinimumFontSize(20);
                            webview.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);

                            webview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
                            if (Build.VERSION.SDK_INT >= 19) {
                                webview.setLayerType(View.LAYER_TYPE_HARDWARE, null);
                            } else {
                                webview.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                            }

                            progressDialog = new ProgressDialog(PaymentGateActivity.this);
                            progressDialog.setMessage("Please wait...");
                            webview.loadDataWithBaseURL(mainurl, response, "text/html", "UTF-8", null);
                            webview.setWebViewClient(new WebViewController());

                        }

                        pDialog.dismiss();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("volley", "Error: " + error.getMessage());
                error.printStackTrace();
                pDialog.dismiss();
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("me_id", ME_ID);
                params.put("txn_details", txndetails);
                params.put("pg_details", pgdetails);
                params.put("card_details", carddetails);
                params.put("cust_details", custdetails);
                params.put("bill_details", billdetails);
                params.put("ship_details", shipdetails);
                params.put("item_details", itemdetails);
                params.put("other_details", otherdetails);
                return params;
            }

        };
        requestQueue.add(jsonObjRequest);
//        Volley.newRequestQueue(this).add(jsonObjRequest);
    }

    private void encodeParams(String me_id, String txnde, String pgde, String cardde, String custde, String buildde, String shipde, String itemde, String otherde) {
        ME_ID = me_id;
        txndetails = encrypt(txnde, ENCRYPT_KEY);
        pgdetails = encrypt(pgde, ENCRYPT_KEY);
        carddetails = encrypt(cardde, ENCRYPT_KEY);
        custdetails = encrypt(custde, ENCRYPT_KEY);
        billdetails = encrypt(buildde, ENCRYPT_KEY);
        shipdetails = encrypt(shipde, ENCRYPT_KEY);
        itemdetails = encrypt(itemde, ENCRYPT_KEY);
        otherdetails = encrypt(otherde, ENCRYPT_KEY);

    }

    private RequestQueue getPinnedRequestQueue(Context context) throws CertificateException, IOException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {

        CertificateFactory cf = CertificateFactory.getInstance("X.509");

        // Generate the certificate using the certificate file under res/raw/cert.cer
        InputStream caInput = new BufferedInputStream(context.getResources().openRawResource(R.raw.pgcertificate));
        final Certificate ca = cf.generateCertificate(caInput);
        caInput.close();

        // Create a KeyStore containing our trusted CAs
        String keyStoreType = KeyStore.getDefaultType();
        KeyStore trusted = KeyStore.getInstance(keyStoreType);
        trusted.load(null, null);
        trusted.setCertificateEntry("ca", ca);

        // Create a TrustManager that trusts the CAs in our KeyStore
        String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
        tmf.init(trusted);

        // Create an SSLContext that uses our TrustManager
        SSLContext sslContext = SSLContext.getInstance("TLSV1.2");
        sslContext.init(null, tmf.getTrustManagers(), null);

        javax.net.ssl.SSLSocketFactory sf = sslContext.getSocketFactory();
        if (Build.VERSION.SDK_INT >= 17 && Build.VERSION.SDK_INT < 22) {
            sf = new TLS12SocketFactory(sf);
        }
        HurlStack hurlStack = new HurlStack(null, sf) {
            @Override
            protected HttpURLConnection createConnection(URL url) throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super.createConnection(url);
                httpsURLConnection.setHostnameVerifier(new HostnameVerifier() {

                    @Override
                    public boolean verify(String hostName, SSLSession sslSession) {
                        String certificateDomainName = ((X509Certificate) ca).getSubjectDN().toString();
//                        String certificateName = certificateDomainName.substring(certificateDomainName.indexOf("CN="), certificateDomainName.codePointCount(certificateDomainName.indexOf("CN="), certificateDomainName.indexOf(",")));
                        String certificateName = "";
//                        certificateName = certificateName.replace("CN", "");
                        certificateName = certificateDomainName.replace("CN=", "");
                        if (certificateName.isEmpty())
                            return false;
                        return certificateName.equals(hostName);
                    }
                });
                return httpsURLConnection;
            }
        };

        return new Volley().newRequestQueue(context, hurlStack);
    }

    public class WebViewController extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            System.out.println("URL" + url);
            if (url.equalsIgnoreCase("https://test.avantgardepayments.com/agcore/testBankResponse")) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Intent i = new Intent();
                i.putExtra("MESSAGE", "Payment Cancelled");
                i.putExtra("RESPONSE", "");
                setResult(RESULT_OK, i);
                finish();
            } else {
                view.loadUrl(url);
            }

            return false;
        }

        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
            // TODO Auto-generated method stub
            super.onReceivedError(view, errorCode, description, failingUrl);
            System.out.println("Content : " + errorCode + "\n" + description + "\n" + failingUrl);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
            progressDialog.show();
            UrlQuerySanitizer sanitizer = new UrlQuerySanitizer();
            sanitizer.setAllowUnregisteredParamaters(true);
            System.out.println("URL : " + url);
            sanitizer.parseUrl(url);
            if (url.contains(successurl)) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                String value = sanitizer.getValue("txn_response");
                String valuePG = sanitizer.getValue("pg_details");
                String valueFRAUD = sanitizer.getValue("fraud_details");
                String valueOTHER = sanitizer.getValue("other_details");
                String resp = decrypt(value, ENCRYPT_KEY);
                String respPG = decrypt(valuePG, ENCRYPT_KEY);
                Log.v("succe", resp+"");
                pay();
//                Intent i = new Intent(PaymentGateActivity.this, );
//                i.putExtra("MESSAGE", "Payment Successful");
//                i.putExtra("RESPONSE", resp);
//                i.putExtra("PGRESPONSE", respPG);
//                setResult(RESULT_OK, i);
//                finish();

            } else if (url.contains(failureurl)) {

                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                String value = sanitizer.getValue("txn_response");
                String valuePG = sanitizer.getValue("pg_details");
                String resp = decrypt(value, ENCRYPT_KEY);
                String respPG = decrypt(valuePG, ENCRYPT_KEY);
                Intent i = new Intent();
                i.putExtra("MESSAGE", "Payment Failed");
                i.putExtra("RESPONSE", resp);
                i.putExtra("PGRESPONSE", respPG);
                setResult(RESULT_OK, i);
                finish();
            }
            System.out.println("PHANI URL" + url);
        }

        public void onPageFinished(WebView view, String url) {
            progressDialog.dismiss();
        }

        @Override
        public void onReceivedSslError(WebView view, final SslErrorHandler handler,
                                       SslError error) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(PaymentGateActivity.this);
            builder.setMessage("Want to proceed");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.proceed();
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.cancel();
                }
            });
            final AlertDialog dialog = builder.create();

            dialog.show();


        }
    }

    public void pay()    {

                        final Bean b = (Bean) getApplicationContext();


                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl(b.baseURL)
                                .addConverterFactory(ScalarsConverterFactory.create())
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();
                        Allapi cr = retrofit.create(Allapi.class);
                        Log.d("UserIdAdd MAi",b.userId);
                        Call<AddWalletBean> call = cr.addWallet(b.userId, amt);
                        call.enqueue(new Callback<AddWalletBean>() {
                            @Override
                            public void onResponse(Call<AddWalletBean> call, retrofit2.Response<AddWalletBean> response) {
                                if (Objects.equals(response.body().getStatus(),"1")){
                                    //bar.setVisibility(View.GONE);
                                    Toast.makeText(PaymentGateActivity.this, "Money Added to Wallet", Toast.LENGTH_SHORT).show();
                                    Intent i = new Intent(PaymentGateActivity.this, MainActivity.class);
                                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(i);
                                    finish();
                                    //amount.setText("");

                                    //bar.setVisibility(View.VISIBLE)


                                }else {
                                    Toast.makeText(PaymentGateActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                    //bar.setVisibility(View.GONE);
                                }
                            }

                            @Override
                            public void onFailure(Call<AddWalletBean> call, Throwable t) {

                                //bar.setVisibility(View.GONE);
                            }
                        });
                        //finalpay(orderid,refID,statuspayment);

    }




}

