package com.cab.ethrocab.ethro;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static com.facebook.FacebookSdk.getApplicationContext;

public class Support extends AppCompatActivity {

    Toolbar toolbar;


    ProgressBar progress;

    Spinner topics, subTopics;


    EditText desc;

    Button submit;


    List<String> tid;
    List<String> ttext;

    List<String> sid;
    List<String> stext;


    String id;
    String id2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.support);


        tid = new ArrayList<>();
        ttext = new ArrayList<>();
        sid = new ArrayList<>();
        stext = new ArrayList<>();


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setNavigationIcon(R.drawable.arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        progress = (ProgressBar) findViewById(R.id.progressBar4);
        topics = findViewById(R.id.spinner);
        subTopics = findViewById(R.id.spinner2);
        desc = findViewById(R.id.editText2);

        submit = findViewById(R.id.button4);


        progress.setVisibility(View.VISIBLE);

        final Bean b = (Bean) getApplicationContext();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(b.baseURL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Allapi cr = retrofit.create(Allapi.class);


        Call<List<topicBean>> call = cr.getTopics();

        call.enqueue(new Callback<List<topicBean>>() {
            @Override
            public void onResponse(Call<List<topicBean>> call, Response<List<topicBean>> response) {

                Log.d("asdasd", "response");
                Log.d("respomse_body",response.body()+"" );
                if(response.body()==null){

                }
                else {
                    for (int i = 0; i < response.body().size(); i++) {


                        tid.add(response.body().get(i).getId());
                        ttext.add(response.body().get(i).getText());

                    }


                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(Support.this, android.R.layout.simple_list_item_1, ttext);


                    topics.setAdapter(dataAdapter);
                }



                progress.setVisibility(View.GONE);


            }

            @Override
            public void onFailure(Call<List<topicBean>> call, Throwable t) {
                progress.setVisibility(View.GONE);
                Log.d("asdasd", t.toString());
            }
        });


        topics.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                id = tid.get(i);


                progress.setVisibility(View.VISIBLE);

                final Bean b = (Bean) getApplicationContext();
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(b.baseURL)
                        .addConverterFactory(ScalarsConverterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                Allapi cr = retrofit.create(Allapi.class);


                Call<List<subTopicBean>> call = cr.getSubTopics(id);

                call.enqueue(new Callback<List<subTopicBean>>() {
                    @Override
                    public void onResponse(Call<List<subTopicBean>> call, Response<List<subTopicBean>> response) {


                        for (int i = 0; i < response.body().size(); i++) {

                            sid.add(response.body().get(i).getId());
                            stext.add(response.body().get(i).getText());

                        }


                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(Support.this, android.R.layout.simple_list_item_1, stext);


                        subTopics.setAdapter(dataAdapter);


                        progress.setVisibility(View.GONE);


                    }

                    @Override
                    public void onFailure(Call<List<subTopicBean>> call, Throwable t) {
                        progress.setVisibility(View.GONE);
                    }
                });


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        subTopics.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                id2 = sid.get(i);


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String t = desc.getText().toString();


                if (t.length() > 0) {
                    progress.setVisibility(View.VISIBLE);

                    final Bean b = (Bean) getApplicationContext();
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(b.baseURL)
                            .addConverterFactory(ScalarsConverterFactory.create())
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    Allapi cr = retrofit.create(Allapi.class);


                    Call<String> call12 = cr.submitQuery(b.userId, id, id2);

                    call12.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {


                            Toast.makeText(Support.this, "Your Query Submitted Successfully", Toast.LENGTH_SHORT).show();


                            progress.setVisibility(View.GONE);

                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            progress.setVisibility(View.GONE);
                        }
                    });
                } else {
                    Toast.makeText(Support.this, "Please add a Description", Toast.LENGTH_SHORT).show();
                }


            }
        });


    }
}
