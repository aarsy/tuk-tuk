package com.softmine.fcm;

import android.content.Context;

import frameworks.dbhandler.PrefManager;


public class FCMCacheManager {
    public static final String GCM_ID = "gcm_id";


    public static void storeRegId(String id, Context context) {
        PrefManager cache = new PrefManager(context);
        cache.putString(GCM_ID, id);
    }


    public static String getRegistrationId(Context context) {
        PrefManager cache = new PrefManager(context);
        return cache.getString(GCM_ID);
    }

}
