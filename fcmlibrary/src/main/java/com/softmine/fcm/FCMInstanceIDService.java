package com.softmine.fcm;


import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;


public class FCMInstanceIDService extends FirebaseInstanceIdService  {

    private static final String TAG = FCMInstanceIDService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        propagateIDtoServer(refreshedToken);
    }


    public void propagateIDtoServer(String token) {
        FCMCacheManager.storeRegId(token, getBaseContext());
    }
}
