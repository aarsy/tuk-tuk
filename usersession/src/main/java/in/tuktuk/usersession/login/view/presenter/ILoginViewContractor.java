package in.tuktuk.usersession.login.view.presenter;


import frameworks.basemvp.IPresenter;
import frameworks.basemvp.IView;

/**
 * Created by sandeepgoyal on 02/05/18.
 */

public interface ILoginViewContractor {
    interface View extends IView {

        void setText(boolean show);

        void setInputType(boolean show);

        void onLoginSuccess();
    }

    interface Presenter extends IPresenter<View> {
        void onLoginClick(String email, String password, int userType);
        void onToggleClick(String text, String show, String hide);
    }

}
