package in.tuktuk.usersession.login.domain;


import javax.inject.Inject;

import in.tuktuk.usersession.userSession.domain.GetTokenUseCase;
import frameworks.appsession.SessionValue;
import frameworks.network.usecases.RequestParams;
import frameworks.network.usecases.UseCase;
import rx.Observable;

/**
 * Created by sandeepgoyal on 04/05/18.
 */

public class LoginEmailUseCase extends UseCase<SessionValue> {

    private final String USERID = "userID";
    private final String PASSWORD = "password";
    private final String USERTYPE = "userType";
    private final String DEVICE_ID = "device_id";
    private final String DEVICE_TYPE = "device_type";


    GetTokenUseCase getTokenUseCase;

    @Inject
    public LoginEmailUseCase(GetTokenUseCase getTokenUseCase) {
        this.getTokenUseCase = getTokenUseCase;
    }

    public RequestParams createRequestParams(String userId, String password, int userType, String deviceId) {
        RequestParams requestParams = RequestParams.create();
        requestParams.putString(USERID, userId);
        requestParams.putString(PASSWORD, password);
        requestParams.putInt(USERTYPE, userType);
//        requestParams.putInt(DEVICE_TYPE,2);
//        requestParams.putString(DEVICE_ID, "12345678");
        return requestParams;
    }

    @Override
    public Observable<SessionValue> createObservable(RequestParams requestParams) {
        return getTokenUseCase.createObservable(requestParams);
    }
}
