package in.tuktuk.usersession.login.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import javax.inject.Inject;

import in.tuktuk.usersession.R;
import in.tuktuk.usersession.login.di.DaggerLoginComponent;
import in.tuktuk.usersession.login.di.LoginComponent;
import in.tuktuk.usersession.login.view.presenter.ForgotPasswordPresenter;
import in.tuktuk.usersession.login.view.presenter.IForgotPasswordContract;
import frameworks.AppBaseApplication;
import frameworks.basemvp.AppBaseActivity;
import frameworks.utils.Validator;

public class ForgotPasswordActivity extends AppBaseActivity<IForgotPasswordContract.Presenter> implements IForgotPasswordContract.View {

    @Inject
    ForgotPasswordPresenter mPresenter;
    EditText email;
    Button recover;
    Button cancel;

    public static Intent getIntent(Context context) {
        Intent i = new Intent(context, ForgotPasswordActivity.class);
        return i;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        email=findViewById(R.id.emailLogin);
        recover=findViewById(R.id.button_recover);
        cancel=findViewById(R.id.button_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        recover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Validator.validateEmail(email.getText())){
                    mPresenter.clickForgot(email.getText().toString());
                }else{
                    email.requestFocus();
                    showToast(getString(R.string.enter_valid_email));
                }
            }
        });
    }

    @Override
    protected void initInjector() {
        LoginComponent loginComponent= DaggerLoginComponent.builder().baseAppComponent((AppBaseApplication.getApplication()).getBaseAppComponent()).build();
        loginComponent.inject(this);
    }



    @Override
    public int getViewToCreate() {
        return R.layout.activity_forget_password;
    }

    @Override
    public IForgotPasswordContract.Presenter getPresenter() {
        return mPresenter;
    }
}
