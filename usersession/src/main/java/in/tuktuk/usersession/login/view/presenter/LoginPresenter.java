package in.tuktuk.usersession.login.view.presenter;


import com.softmine.fcm.FCMCacheManager;

import javax.inject.Inject;

import in.tuktuk.usersession.login.domain.LoginEmailUseCase;
import frameworks.appsession.SessionValue;
import frameworks.basemvp.AppBasePresenter;
import frameworks.network.Utils;
import frameworks.network.model.ResponseException;
import frameworks.network.usecases.RequestParams;
import rx.Subscriber;

/**
 * Created by sandeepgoyal on 03/05/18.
 */

public class LoginPresenter extends AppBasePresenter<ILoginViewContractor.View> implements ILoginViewContractor.Presenter {

    LoginEmailUseCase loginEmailUseCase;

    @Inject
    public LoginPresenter(LoginEmailUseCase loginEmailUseCase) {
        this.loginEmailUseCase = loginEmailUseCase;
    }

    @Override
    public void onLoginClick(String userId, String password, int userType) {
        final RequestParams requestParams = loginEmailUseCase.createRequestParams(userId, password, userType, FCMCacheManager.getRegistrationId(getView().getContext()));

        if(!Utils.isInternetOn()) {
            getView().showToast("Internet Not Available! Try Again");
            return;
        }
        getView().showProgressBar();
        loginEmailUseCase.execute(requestParams, new Subscriber<SessionValue>() {
            @Override
            public void onCompleted() {
                getView().hideProgressBar();
            }

            @Override
            public void onError(Throwable e) {
                getView().hideProgressBar();
                if (e instanceof ResponseException) {
                    getView().showToast(e.getMessage());
                }
            }

            @Override
            public void onNext(SessionValue sessionValue) {
                getView().onLoginSuccess();

            }
        });
    }

    @Override
    public void onToggleClick(String text, String show, String hide) {
        boolean ifShowPassword = text.equals(show);
        getView().setInputType (ifShowPassword);
        getView().setText(ifShowPassword);

    }


}
