package in.tuktuk.usersession.login.view.presenter;

import javax.inject.Inject;

import in.tuktuk.usersession.userSession.domain.ForgotPasswordUseCase;
import in.tuktuk.usersession.userSession.model.ForgotPasswordResponse;
import frameworks.basemvp.AppBasePresenter;
import rx.Subscriber;

public class ForgotPasswordPresenter extends AppBasePresenter<IForgotPasswordContract.View> implements IForgotPasswordContract.Presenter{


    private ForgotPasswordUseCase forgotPasswordUseCase;

    @Inject
    public ForgotPasswordPresenter(ForgotPasswordUseCase forgotPasswordUseCase) {
        this.forgotPasswordUseCase=forgotPasswordUseCase;
    }

    @Override
    public void clickForgot(String email){
        forgotPasswordUseCase.execute(ForgotPasswordUseCase.createRequestParams(email), new Subscriber<ForgotPasswordResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(ForgotPasswordResponse forgotPasswordResponse) {
                if(forgotPasswordResponse.isSuccess()){
                    getView().showToast("Password link sent on mail");
                }else {
                    getView().showToast("Please enter registered email address");
                }

            }
        });
    }

    @Override
    public void clickChange(String oldPass, String newPass) {

    }
}
