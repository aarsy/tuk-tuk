package in.tuktuk.usersession.login.view;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.Task;

import java.util.Arrays;

import javax.inject.Inject;


import in.tuktuk.usersession.R;
import in.tuktuk.usersession.login.di.DaggerLoginComponent;
import in.tuktuk.usersession.login.di.LoginComponent;
import in.tuktuk.usersession.login.view.presenter.ILoginViewContractor;
import in.tuktuk.usersession.login.view.presenter.LoginPresenter;
import frameworks.AppBaseApplication;
import frameworks.basemvp.AppBaseActivity;
import frameworks.di.component.HasComponent;
import frameworks.utils.Validator;
import in.tuktuk.usersession.registerDriver.view.ActivityCreateDriver;

/**
 * A login screen that offers login via email/password.
 */
public class CustomerLoginActivity extends AppBaseActivity<ILoginViewContractor.Presenter> implements ILoginViewContractor.View, HasComponent<LoginComponent>, View.OnClickListener {
    private static final int RC_SIGN_IN = 1001;
    @Inject
    LoginPresenter presenter;
    private EditText email;
    private EditText password;
    private TextView showPassword;
    private TextView forgotPassword, changePassword;
    private Button loginSignin;
    LoginComponent loginComponent;
    private CallbackManager callbackManager;
    private LoginButton loginButton;
    private GoogleSignInClient mGoogleSignInClient;


    public static Intent getIntent(Context context) {
        Intent i = new Intent(context, LoginActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        return i;
    }

    public static Intent getIntentWithoutFlags(Context context){
        Intent i = new Intent(context, CustomerLoginActivity.class);
        return i;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        email=findViewById(R.id.emailLogin);
        password=findViewById(R.id.passwordLogin);
        showPassword=findViewById(R.id.toggleButtonShowPassword);
        forgotPassword=findViewById(R.id.forgotPassword);
        changePassword=findViewById(R.id.changePassword);
        loginSignin=findViewById(R.id.loginSignIn);
        loginSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLoginClicked();
            }
        });
        showPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onToggleButtonClicked();
            }
        });
        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onForgotPasswordClick();
            }
        });
        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onChangePasswordClick();
            }
        });

        ImageView welcomeLogo=findViewById(R.id.welcomeLogo);
        TextView btnCreateAcc=findViewById(R.id.btn_create_account);
        if(LoginActivity.userType == LoginActivity.USER_TYPE_DRIVER){
            welcomeLogo.setImageResource(R.drawable.app_logo_driver);
            btnCreateAcc.setVisibility(View.VISIBLE);
            btnCreateAcc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(),ActivityCreateDriver.class));
                }
            });
        }else if(LoginActivity.userType == LoginActivity.USER_TYPE_CUSTOMER){
            facebookSignInCallbacks();
            googleSignInCallbacks();
        }

    }

    private void googleSignInCallbacks() {

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .requestServerAuthCode(getString(R.string.server_client_id), false)
                .requestIdToken(getString(R.string.server_client_id))
                .requestId()
                .build();
//        googleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this, this).addApi(Auth.GOOGLE_SIGN_IN_API, signInOptions)
//                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        findViewById(R.id.sign_in_button).setOnClickListener(this);


    }

    private static final String EMAIL = "email";

    private void facebookSignInCallbacks() {
        callbackManager = CallbackManager.Factory.create();
        loginButton = findViewById(R.id.login_button);
        loginButton.setVisibility(View.VISIBLE);
        loginButton.setReadPermissions(Arrays.asList(EMAIL));
        // If you are using in a fragment, call loginButton.setFragment(this);

        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                Log.d("FbAccessToken", loginResult.getAccessToken().getUserId()+"     " +loginResult.getAccessToken().getToken());
            }

            @Override
            public void onCancel() {
                // App code
                Log.d("FbOnCancel", "true");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.d("FbOnError", "true");
            }
        });
    }

    @Override
    protected void initInjector() {
        loginComponent = DaggerLoginComponent.builder().baseAppComponent(((AppBaseApplication.getApplication()).getBaseAppComponent())).build();
        loginComponent.inject(this);
    }

    @Override
    public int getViewToCreate() {
        return R.layout.activity_customer_login;
    }

    @Override
    public ILoginViewContractor.Presenter getPresenter() {
        return presenter;
    }

    @Override
    public void setText(boolean show) {
        if (show)
            showPassword.setText(getString(R.string.hide));
        else
            showPassword.setText(getString(R.string.show));
    }

    @Override
    public void setInputType(boolean show) {
        if (show)
            password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        else
            password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
    }

    @Override
    public void onLoginSuccess() {
        setResult(RESULT_OK);
        finish();
    }


    public void onLoginClicked() {
        if (Validator.validateEmail(email.getText())) {
            if(TextUtils.isEmpty(password.getText())){
               getString(R.string.enter_non_empty_value);
               password.requestFocus();
            }else {
                getPresenter().onLoginClick(email.getText().toString(), password.getText().toString(), LoginActivity.userType);
            }
        } else {
            email.requestFocus();
            showToast(getString(R.string.enter_valid_email));
        }

    }

    public void onToggleButtonClicked() {
        getPresenter().onToggleClick(showPassword.getText().toString(), getResources().getString(R.string.show), getResources().getString(R.string.hide));
    }

    public void onForgotPasswordClick() {
        startActivity(ForgotPasswordActivity.getIntent(getContext()));
    }

    public void onChangePasswordClick() {
        startActivity(ChangePasswordActivity.getIntent(getContext()));
    }

    @Override
    public LoginComponent getComponent() {
        if (loginComponent == null)
            initInjector();
        return loginComponent;
    }

    public boolean isLocationNeeded() {
        return false;
    }


    @Override
    public void onLocationPermissionGranted() {

    }

    @Override
    public void onLocationPermissionDenied() {

    }

    @Override
    public void onLocationSettingEnabled() {

    }

    @Override
    public void onLocationSettingEnableDenied() {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.sign_in_button) {
            signIn();
            // ...
        }
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }else{
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            if (account != null) {
                Log.d("GoogleSignon Id", account.getId()+"    "+account.getIdToken());
            }
//            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
//            updateUI(null);
        }
    }
}

