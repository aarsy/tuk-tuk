package in.tuktuk.usersession.login.di;


import in.tuktuk.usersession.login.view.ChangePasswordActivity;
import in.tuktuk.usersession.login.view.CustomerLoginActivity;
import in.tuktuk.usersession.login.view.ForgotPasswordActivity;
import in.tuktuk.usersession.userSession.di.GetTokenModule;
import in.tuktuk.usersession.login.view.LoginActivity;
import dagger.Component;
import frameworks.di.component.BaseAppComponent;

/**
 * Created by sandeepgoyal on 03/05/18.
 */
@LoginScope
@Component(modules = {GetTokenModule.class, LoginModule.class}, dependencies = BaseAppComponent.class)
public interface LoginComponent {
    void inject(LoginActivity loginActivity);

    void inject(ForgotPasswordActivity forgotPasswordActivity);

    void inject(ChangePasswordActivity changePasswordActivity);

    void inject(CustomerLoginActivity customerLoginActivity);
}
