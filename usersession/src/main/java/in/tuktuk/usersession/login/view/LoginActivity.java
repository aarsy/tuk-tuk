package in.tuktuk.usersession.login.view;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import javax.inject.Inject;


import in.tuktuk.usersession.R;
import in.tuktuk.usersession.login.di.DaggerLoginComponent;
import in.tuktuk.usersession.login.di.LoginComponent;
import in.tuktuk.usersession.login.view.presenter.ILoginViewContractor;
import in.tuktuk.usersession.login.view.presenter.LoginPresenter;
import frameworks.AppBaseApplication;
import frameworks.basemvp.AppBaseActivity;
import frameworks.di.component.HasComponent;
import frameworks.utils.Validator;
import in.tuktuk.usersession.registerDriver.view.ActivityCreateDriver;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppBaseActivity<ILoginViewContractor.Presenter> implements ILoginViewContractor.View, HasComponent<LoginComponent>{
    @Inject
    LoginPresenter presenter;
    private EditText email;
    private EditText password;
    private TextView showPassword;
    private TextView forgotPassword, changePassword;
    private Button loginSignin;
    LoginComponent loginComponent;
    public static int userType;
    public static final int USER_TYPE_DRIVER=2;
    public static final int USER_TYPE_EMPLOYEE=3;
    public static final int USER_TYPE_CUSTOMER=1;


    public static Intent getIntent(Context context) {
        Intent i = new Intent(context, LoginActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        return i;
    }

    public static Intent getIntentWithoutFlags(Context context){
        Intent i = new Intent(context, LoginActivity.class);
        return i;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        email=findViewById(R.id.emailLogin);
        password=findViewById(R.id.passwordLogin);
        showPassword=findViewById(R.id.toggleButtonShowPassword);
        forgotPassword=findViewById(R.id.forgotPassword);
        changePassword=findViewById(R.id.changePassword);
        loginSignin=findViewById(R.id.loginSignIn);
        loginSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLoginClicked();
            }
        });
        showPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onToggleButtonClicked();
            }
        });
        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onForgotPasswordClick();
            }
        });
        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onChangePasswordClick();
            }
        });

        ImageView welcomeLogo=findViewById(R.id.welcomeLogo);
        TextView btnCreateAcc=findViewById(R.id.btn_create_account);
        if(userType ==USER_TYPE_DRIVER){
            welcomeLogo.setImageResource(R.drawable.app_logo_driver);
            btnCreateAcc.setVisibility(View.VISIBLE);
            btnCreateAcc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(),ActivityCreateDriver.class));
                }
            });
        }

    }

    @Override
    protected void initInjector() {
        loginComponent = DaggerLoginComponent.builder().baseAppComponent(((AppBaseApplication.getApplication()).getBaseAppComponent())).build();
        loginComponent.inject(this);
    }

    @Override
    public int getViewToCreate() {
        return R.layout.activity_login;
    }

    @Override
    public ILoginViewContractor.Presenter getPresenter() {
        return presenter;
    }

    @Override
    public void setText(boolean show) {
        if (show)
            showPassword.setText(getString(R.string.hide));
        else
            showPassword.setText(getString(R.string.show));
    }

    @Override
    public void setInputType(boolean show) {
        if (show)
            password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        else
            password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
    }

    @Override
    public void onLoginSuccess() {
        setResult(RESULT_OK);
        finish();
    }


    public void onLoginClicked() {
        if (Validator.validateEmail(email.getText())) {
            if(TextUtils.isEmpty(password.getText())){
               getString(R.string.enter_non_empty_value);
               password.requestFocus();
            }else {
                getPresenter().onLoginClick(email.getText().toString(), password.getText().toString(), userType);
            }
        } else {
            email.requestFocus();
            showToast(getString(R.string.enter_valid_email));
        }

    }

    public void onToggleButtonClicked() {
        getPresenter().onToggleClick(showPassword.getText().toString(), getResources().getString(R.string.show), getResources().getString(R.string.hide));
    }

    public void onForgotPasswordClick() {
        startActivity(ForgotPasswordActivity.getIntent(getContext()));
    }

    public void onChangePasswordClick() {
        startActivity(ChangePasswordActivity.getIntent(getContext()));
    }


    @Override
    public LoginComponent getComponent() {
        if (loginComponent == null)
            initInjector();
        return loginComponent;
    }

    public boolean isLocationNeeded() {
        return false;
    }


    @Override
    public void onLocationPermissionGranted() {

    }

    @Override
    public void onLocationPermissionDenied() {

    }

    @Override
    public void onLocationSettingEnabled() {

    }

    @Override
    public void onLocationSettingEnableDenied() {

    }

    @Override
    public void onLocationChanged(Location location) {

    }
}

