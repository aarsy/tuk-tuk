package in.tuktuk.usersession.login.di;



import in.tuktuk.usersession.userSession.domain.GetTokenUseCase;
import in.tuktuk.usersession.userSession.domain.IUserSessionRepository;
import dagger.Module;
import dagger.Provides;

/**
 * Created by sandeepgoyal on 03/05/18.
 */

@Module
public class LoginModule {

    @Provides
    GetTokenUseCase getTokenUseCase(IUserSessionRepository getTokenRepository) {
        return new GetTokenUseCase(getTokenRepository);
    }



}
