package in.tuktuk.usersession.login.view.presenter;

import frameworks.basemvp.IPresenter;
import frameworks.basemvp.IView;

public interface IForgotPasswordContract {
    interface View extends IView{

    }
    interface Presenter extends IPresenter<View>{

        void clickForgot(String email);

        void clickChange(String oldPass, String newPass);
    }
}
