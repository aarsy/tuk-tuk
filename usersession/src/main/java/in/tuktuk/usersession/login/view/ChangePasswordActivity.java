package in.tuktuk.usersession.login.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import javax.inject.Inject;

import in.tuktuk.usersession.R;
import in.tuktuk.usersession.login.di.DaggerLoginComponent;
import in.tuktuk.usersession.login.di.LoginComponent;
import in.tuktuk.usersession.login.view.presenter.ForgotPasswordPresenter;
import in.tuktuk.usersession.login.view.presenter.IForgotPasswordContract;
import frameworks.AppBaseApplication;
import frameworks.basemvp.AppBaseActivity;
import frameworks.utils.Validator;

public class ChangePasswordActivity extends AppBaseActivity<IForgotPasswordContract.Presenter> implements IForgotPasswordContract.View {

    @Inject
    ForgotPasswordPresenter mPresenter;
    Button change;
    Button cancel;
    private EditText etNewPass2, etNewPass, etOldPass;

    public static Intent getIntent(Context context) {
        Intent i = new Intent(context, ChangePasswordActivity.class);
        return i;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        etOldPass = findViewById(R.id.et_old_password);
        etNewPass = findViewById(R.id.et_new_password);
        etNewPass2 = findViewById(R.id.et_new_password_2);

        change = findViewById(R.id.button_change);
        cancel = findViewById(R.id.button_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(etOldPass.getText())
                        || TextUtils.isEmpty(etNewPass.getText())
                        || TextUtils.isEmpty(etNewPass2.getText())) {
                    showToast(getString(R.string.fields_cannot_be_empty));
                } else if (!etNewPass.getText().equals(etNewPass2.getText())) {
                    showToast(getString(R.string.reentered_password_not_match));
                }else{
                    mPresenter.clickChange(etOldPass.getText().toString(), etNewPass.getText().toString());
                }


            }
        });
    }

    @Override
    protected void initInjector() {
        LoginComponent loginComponent = DaggerLoginComponent.builder().baseAppComponent((AppBaseApplication.getApplication()).getBaseAppComponent()).build();
        loginComponent.inject(this);
    }


    @Override
    public int getViewToCreate() {
        return R.layout.activity_change_password;
    }

    @Override
    public IForgotPasswordContract.Presenter getPresenter() {
        return mPresenter;
    }
}
