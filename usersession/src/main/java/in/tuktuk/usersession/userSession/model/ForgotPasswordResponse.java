package in.tuktuk.usersession.userSession.model;

import com.google.gson.annotations.SerializedName;

public class ForgotPasswordResponse {

	@SerializedName("success")
	private boolean success;

	public void setSuccess(boolean success){
		this.success = success;
	}

	public boolean isSuccess(){
		return success;
	}

	@Override
 	public String toString(){
		return 
			"ForgotPasswordResponse{" +
			"success = '" + success + '\'' + 
			"}";
		}
}