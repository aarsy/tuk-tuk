package in.tuktuk.usersession.userSession.data.repository.datasource;


import com.google.gson.JsonObject;

import javax.inject.Inject;

import in.tuktuk.usersession.userSession.data.net.LoginAPI;
import in.tuktuk.usersession.userSession.model.ForgotPasswordResponse;
import in.tuktuk.usersession.userSession.model.LoginResponse;
import frameworks.appsession.AppSessionManager;
import frameworks.appsession.SessionValue;
import frameworks.appsession.UserInfo;
import frameworks.network.mapper.DataResponseMapper;
import frameworks.network.model.DataResponse;
import frameworks.network.usecases.RequestParams;
import retrofit2.Response;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by sandeep on 5/5/18.
 */
public class UserSessionDataSource {
    private final AppSessionManager appSessionManager;
    private LoginAPI loginAPI;

    @Inject
    public UserSessionDataSource(LoginAPI loginAPI, AppSessionManager appSessionManager) {
        this.loginAPI = loginAPI;
        this.appSessionManager = appSessionManager;
    }

    public Observable<SessionValue> getToken(RequestParams requestParams) {
        return loginAPI.login(requestParams.getParameters()).map(new Func1<LoginResponse, SessionValue>() {
            @Override
            public SessionValue call(LoginResponse loginResponse) {
                SessionValue sessionValue = new SessionValue();
                sessionValue.setApi_key(loginResponse.getAuthToken());
                return sessionValue;
            }
        }).doOnNext(saveSession());
    }

    private Action1<? super SessionValue> saveSession() {
        return new Action1<SessionValue>() {
            @Override
            public void call(SessionValue sessionValue) {
                appSessionManager.saveSession(sessionValue);
            }
        };
    }

    public Observable<ForgotPasswordResponse> forgotPassword(JsonObject body) {
        return loginAPI.forgot(body);
    }
}
