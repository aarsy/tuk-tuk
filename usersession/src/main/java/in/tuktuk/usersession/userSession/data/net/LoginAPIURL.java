package in.tuktuk.usersession.userSession.data.net;


/**
 * Created by sandeepgoyal on 03/05/18.
 */

public class LoginAPIURL {
    public static String BASE_URL = "https://ec2-52-90-245-8.compute-1.amazonaws.com/api/";     //default url is live

    interface HelperUrl {
        String Login_API = "login";
        String FORGOT_PASSWORD_API = "forgot";
    }
}
