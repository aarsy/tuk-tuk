package in.tuktuk.usersession.userSession.data.repository;


import com.google.gson.JsonObject;

import javax.inject.Inject;

import in.tuktuk.usersession.userSession.data.repository.datasource.UserSessionDataFactory;
import in.tuktuk.usersession.userSession.domain.IUserSessionRepository;
import in.tuktuk.usersession.userSession.model.ForgotPasswordResponse;
import frameworks.appsession.SessionValue;
import frameworks.network.usecases.RequestParams;
import rx.Observable;

/**
 * Created by sandeep on 4/5/18.
 */
public class UserSessionDataRepository implements IUserSessionRepository {

    UserSessionDataFactory userSessionDataFactory;

    @Inject
    public UserSessionDataRepository(UserSessionDataFactory userSessionDataFactory) {
        this.userSessionDataFactory = userSessionDataFactory;
    }


    @Override
    public Observable<SessionValue> getToken(RequestParams requestParams) {
        return userSessionDataFactory.getToken(requestParams);
    }

    @Override
    public Observable<ForgotPasswordResponse> forgotPassword(JsonObject body) {
        return userSessionDataFactory.forgotPassword(body);
    }
}
