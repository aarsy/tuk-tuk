package in.tuktuk.usersession.userSession.data.repository.datasource;

import com.google.gson.JsonObject;

import javax.inject.Inject;

import in.tuktuk.usersession.userSession.model.ForgotPasswordResponse;
import frameworks.appsession.SessionValue;
import frameworks.network.usecases.RequestParams;
import rx.Observable;

/**
 * Created by sandeep on 5/5/18.
 */
public class UserSessionDataFactory {

    UserSessionDataSource userSessionDataSource;

    @Inject
    public UserSessionDataFactory(UserSessionDataSource userSessionDataSource) {
        this.userSessionDataSource = userSessionDataSource;
    }

    public Observable<SessionValue> getToken(RequestParams requestParams) {
        return userSessionDataSource.getToken(requestParams);
    }

    public Observable<ForgotPasswordResponse> forgotPassword(JsonObject body) {
        return userSessionDataSource.forgotPassword(body);
    }

}
