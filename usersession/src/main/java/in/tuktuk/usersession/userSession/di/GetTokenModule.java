package in.tuktuk.usersession.userSession.di;


import in.tuktuk.usersession.userSession.data.interceptor.GetTokenInterceptor;
import in.tuktuk.usersession.userSession.data.net.LoginAPI;
import in.tuktuk.usersession.userSession.data.net.LoginAPIURL;
import in.tuktuk.usersession.userSession.data.repository.UserSessionDataRepository;
import in.tuktuk.usersession.userSession.data.repository.datasource.UserSessionDataSource;
import in.tuktuk.usersession.userSession.data.repository.datasource.UserSessionDataFactory;
import in.tuktuk.usersession.userSession.domain.IUserSessionRepository;
import dagger.Module;
import dagger.Provides;
import frameworks.appsession.AppSessionManager;
import frameworks.network.interceptor.ErrorResponseInterceptor;
import frameworks.network.model.BaseResponseError;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;

/**
 * Created by sandeep on 5/5/18.
 */
@Module
public class GetTokenModule {
    @Provides
    LoginAPI provideLoginApi(Retrofit retrofit) {
        return retrofit.create(LoginAPI.class);
    }

    @Provides
    Retrofit provideRetrofit(OkHttpClient okHttpClient,
                             Retrofit.Builder retrofitBuilder) {
        return retrofitBuilder.baseUrl(LoginAPIURL.BASE_URL).client(okHttpClient).build();
    }

    @Provides
    OkHttpClient provideOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor) {
        return UnsafeOkHttpClient.getUnsafeOkHttpClient()
                .addInterceptor(httpLoggingInterceptor)
                .addInterceptor(new GetTokenInterceptor())
                .addInterceptor(new ErrorResponseInterceptor(BaseResponseError.class))
                .build();
    }

    @Provides
    IUserSessionRepository getTokenRepository(UserSessionDataFactory userSessionDataFactory) {
        return new UserSessionDataRepository(userSessionDataFactory);
    }

    @Provides
    UserSessionDataFactory getTokenDataFactory(UserSessionDataSource userSessionDataSource) {
        return new UserSessionDataFactory(userSessionDataSource);
    }

    @Provides
    UserSessionDataSource getTokenCloudDataSource(LoginAPI loginAPI, AppSessionManager appSessionManager) {
        return new UserSessionDataSource(loginAPI, appSessionManager);
    }


}
