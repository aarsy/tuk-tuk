package in.tuktuk.usersession.userSession.model;

import com.google.gson.annotations.SerializedName;


public class LoginData {

	@SerializedName("errorCode")
	private int errorCode;

	@SerializedName("message")
	private String message;

	public void setErrorCode(int errorCode){
		this.errorCode = errorCode;
	}

	public int getErrorCode(){
		return errorCode;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	@Override
 	public String toString(){
		return 
			"LoginData{" +
			"errorCode = '" + errorCode + '\'' + 
			",message = '" + message + '\'' + 
			"}";
		}
}