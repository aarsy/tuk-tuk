package in.tuktuk.usersession.userSession.domain;

import com.google.gson.JsonObject;

import in.tuktuk.usersession.userSession.model.ForgotPasswordResponse;
import frameworks.appsession.SessionValue;
import frameworks.network.usecases.RequestParams;
import rx.Observable;

/**
 * Created by sandeep on 5/5/18.
 */
public interface IUserSessionRepository {
    Observable<SessionValue> getToken(RequestParams requestParams);

    Observable<ForgotPasswordResponse> forgotPassword(JsonObject jsonObject);
}
