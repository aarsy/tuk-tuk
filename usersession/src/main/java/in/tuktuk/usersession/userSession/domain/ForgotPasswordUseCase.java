package in.tuktuk.usersession.userSession.domain;

import com.google.gson.JsonObject;

import javax.inject.Inject;

import in.tuktuk.usersession.userSession.model.ForgotPasswordResponse;
import frameworks.network.usecases.RequestParams;
import frameworks.network.usecases.UseCase;
import rx.Observable;


public class ForgotPasswordUseCase extends UseCase<ForgotPasswordResponse> {

    IUserSessionRepository getTokenRepository;

    public static RequestParams createRequestParams(String email){
        RequestParams requestParams=RequestParams.create();
        JsonObject jsonObject=new JsonObject();
        jsonObject.addProperty("email", email);
        requestParams.putObject("value", jsonObject);
        return requestParams;
    }

    @Inject
    public ForgotPasswordUseCase(IUserSessionRepository getTokenRepository) {
        this.getTokenRepository = getTokenRepository;
    }

    @Override
    public Observable<ForgotPasswordResponse> createObservable(RequestParams requestParams) {
        return getTokenRepository.forgotPassword((JsonObject) requestParams.getObject("value"));
    }
}
