package in.tuktuk.usersession.userSession.data.net;


import com.google.gson.JsonObject;

import java.util.Map;

import in.tuktuk.usersession.userSession.model.ForgotPasswordResponse;
import in.tuktuk.usersession.userSession.model.LoginResponse;
import frameworks.network.model.DataResponse;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by sandeepgoyal on 03/05/18.
 */

public interface LoginAPI {
//    @FormUrlEncoded
    @POST(LoginAPIURL.HelperUrl.Login_API)
    Observable<LoginResponse> login(@Body Map<String, Object> params);

    @POST(LoginAPIURL.HelperUrl.FORGOT_PASSWORD_API)
    Observable<ForgotPasswordResponse> forgot(@Body JsonObject value);


}
