package in.tuktuk.usersession.userSession.model;

import com.google.gson.annotations.SerializedName;

public class LoginResponse{

    @SerializedName("data")
    private LoginData loginData;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private String status;

    @SerializedName("statusCode")
    private String statusCode;

    private String authToken;
    
    public void setLoginData(LoginData loginData){
        this.loginData = loginData;
    }

    public LoginData getLoginData(){
        return loginData;
    }

    public void setMessage(String message){
        this.message = message;
    }

    public String getMessage(){
        return message;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public String getStatus(){
        return status;
    }

    public void setStatusCode(String statusCode){
        this.statusCode = statusCode;
    }

    public String getStatusCode(){
        return statusCode;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getAuthToken() {
        return authToken;
    }

    @Override
    public String toString(){
        return
                "LoginResponse2{" +
                        "loginData = '" + loginData + '\'' +
                        ",message = '" + message + '\'' +
                        ",status = '" + status + '\'' +
                        ",statusCode = '" + statusCode + '\'' +
                        "}";
    }
    

}