package in.tuktuk.usersession.registerDriver.domain;


import javax.inject.Inject;

import frameworks.network.usecases.RequestParams;
import frameworks.network.usecases.UseCase;
import in.tuktuk.usersession.registerDriver.data.model.VehicleTypeResponse;
import rx.Observable;

public class GetVehicleTypeUseCase extends UseCase<VehicleTypeResponse> {


    private final IUserRegisterRepository deliveryRepository;

    @Inject
    public GetVehicleTypeUseCase(IUserRegisterRepository deliveryRepository) {
        this.deliveryRepository = deliveryRepository;
    }

    @Override
    public Observable<VehicleTypeResponse> createObservable(RequestParams requestParams) {
        return deliveryRepository.getVehicleType();
    }
}
