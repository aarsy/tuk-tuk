package in.tuktuk.usersession.registerDriver.domain;



import javax.inject.Inject;

import frameworks.network.usecases.RequestParams;
import frameworks.network.usecases.UseCase;
import in.tuktuk.usersession.registerDriver.data.model.CreateDriverResponse;
import in.tuktuk.usersession.registerDriver.data.model.DriverInfo;
import rx.Observable;

public class UserRegisterUseCase extends UseCase<CreateDriverResponse> {

    private final IUserRegisterRepository deliveryRepository;
    private final String DRIVER_INFO="DRIVER_INFO";

    @Inject
    public UserRegisterUseCase(IUserRegisterRepository deliveryRepository) {
        this.deliveryRepository = deliveryRepository;
    }

    public RequestParams createRequestParams(DriverInfo driverInfo){
        RequestParams requestParams=RequestParams.create();
        requestParams.putObject(DRIVER_INFO,driverInfo);
        return requestParams;
    }

    @Override
    public Observable<CreateDriverResponse> createObservable(RequestParams requestParams) {
        return deliveryRepository.createUser((DriverInfo) requestParams.getObject(DRIVER_INFO));
    }
}
