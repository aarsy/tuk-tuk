package in.tuktuk.usersession.registerDriver.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import javax.inject.Inject;

import frameworks.AppBaseApplication;
import frameworks.basemvp.AppBaseActivity;
import frameworks.imageloader.view.ActivityPicChooser;
import in.tuktuk.usersession.R;
import in.tuktuk.usersession.registerDriver.data.model.DriverInfo;
import in.tuktuk.usersession.registerDriver.di.CreateUserComponent;
import in.tuktuk.usersession.registerDriver.di.DaggerCreateUserComponent;
import in.tuktuk.usersession.registerDriver.domain.UserRegisterUseCase;
import in.tuktuk.usersession.registerDriver.service.DocumentsUploadService;
import in.tuktuk.usersession.registerDriver.view.presenter.DriverDocumentContract;
import in.tuktuk.usersession.registerDriver.view.presenter.DriverDocumentPresenter;

import static frameworks.imageloader.view.ActivityPicChooser.IMAGE_URI_RESULT;

public class DriverDocument extends AppBaseActivity<DriverDocumentContract.Presenter>  implements DriverDocumentContract.View{

    private Toolbar mToolbar;
    private RelativeLayout mBtnProfilePhoto;
    private RelativeLayout mBtnDriverLicence;
    private RelativeLayout mBtnPanCard;
    private RelativeLayout mBtnCertificateRegistration;
    private RelativeLayout mBtnMotorInsurance;
    private RelativeLayout mBtnPoliceVerfication;
    private RelativeLayout mBtnAadharCard;
    private TextView mBtnSubmit;

    private static final int REQUEST_PROFILE_PICTURE = 1;
    private String profile_pic_path;
    private static final int REQUEST_DRIVER_LICENCE = 2;
    private String driver_licence_path;
    private static final int REQUEST_PAN_CARD = 3;
    private String pan_card_path;
    private static final int REQUEST_REGISTRATION_CERTIFICATE = 4;
    private String registration_certificate_path;
    private static final int REQUEST_MOTOR_INSURANCE_CERTIFICATE = 5;
    private String motor_insurance_path;
    private static final int REQUEST_POLICE_VERIFICATION = 6;
    private String police_verification_path;
    private static final int REQUEST_AADHAR_CARD = 7;
    private String aadhar_card_path;
    private ImageView mProfilePicArrow;
    private ImageView mDriverLicencePicArrow;
    private ImageView mPanCardPicArrow;
    private ImageView mRegistrationCertificatePicArrow;
    private ImageView mMotorInsurancePicArrow;
    private ImageView mPoliceVerificationPicArrow;
    private ImageView mAadharCardPicArrow;
    private static final String LICENCE = "driving_licence_front";
    private static final String DRIVER_PIC = "driver_pic";
    private static final String PANCARD = "pancard";
    private static final String REGISTRATION_CERTI = "registration_certificate";
    private static final String MOTOR_INSURANCE = "motor_insurence";
    private static final String POLICE_VERIFICATION = "police_verification";
    private static final String AADHAR_CARD = "adhar_card";
    @Inject
    DriverDocumentPresenter mPresenter;


    public static Intent getInstance(DriverInfo driverInfo, Context context) {
        Intent i = new Intent(context, DriverDocument.class);
        i.putExtra("DRIVER_INFO", driverInfo);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        setClickListener();
        setLeftIcon(R.drawable.back_arrow_white);
        setTitle(getString(R.string.title_activity_documents));
    }

    @Override
    public void onLeftMenuClick() {
        super.onLeftMenuClick();
        onBackPressed();
    }

    CreateUserComponent createUserComponent;

    @Override
    protected void initInjector() {
        createUserComponent = DaggerCreateUserComponent.builder().baseAppComponent(AppBaseApplication.getApplication().getBaseAppComponent()).build();
        createUserComponent.inject(this);
    }

    @Override
    public int getViewToCreate() {
        return R.layout.activity_driver_document;
    }

    @Override
    public DriverDocumentContract.Presenter getPresenter() {
        return mPresenter;
    }

    private void setClickListener() {
        mBtnProfilePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(DriverDocument.this, ActivityPicChooser.class), REQUEST_PROFILE_PICTURE);
            }
        });
        mBtnDriverLicence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(DriverDocument.this, ActivityPicChooser.class), REQUEST_DRIVER_LICENCE);
            }
        });
        mBtnPanCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(DriverDocument.this, ActivityPicChooser.class), REQUEST_PAN_CARD);
            }
        });
        mBtnCertificateRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(DriverDocument.this, ActivityPicChooser.class), REQUEST_REGISTRATION_CERTIFICATE);
            }
        });
        ;
        mBtnMotorInsurance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(DriverDocument.this, ActivityPicChooser.class), REQUEST_MOTOR_INSURANCE_CERTIFICATE);
            }
        });
        mBtnPoliceVerfication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(DriverDocument.this, ActivityPicChooser.class), REQUEST_POLICE_VERIFICATION);
            }
        });
        mBtnAadharCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(DriverDocument.this, ActivityPicChooser.class), REQUEST_AADHAR_CARD);
            }
        });

        mBtnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadData();
            }
        });


    }

    @Inject
    UserRegisterUseCase userRegisterUseCase;

    private void uploadData() {
        DriverInfo driverInfo = (DriverInfo) getIntent().getSerializableExtra("DRIVER_INFO");
        showProgressBar("Uploading Driver Info");
        mPresenter.uploadDriverInfo(driverInfo);

    }

    public void uploadDocuments(String id) {
        showProgressBar("Uploading Documents");
        ArrayList<DocumentUploadData> picPaths=new ArrayList<>();
        if(!TextUtils.isEmpty(profile_pic_path)){
            picPaths.add(new DocumentUploadData(DRIVER_PIC, profile_pic_path));
        }
        if(!TextUtils.isEmpty(driver_licence_path)){
            picPaths.add(new DocumentUploadData(LICENCE, driver_licence_path));
        }
        if(!TextUtils.isEmpty(police_verification_path)){
            picPaths.add(new DocumentUploadData(POLICE_VERIFICATION, police_verification_path));
        }
        if(!TextUtils.isEmpty(aadhar_card_path)){
            picPaths.add(new DocumentUploadData(AADHAR_CARD, aadhar_card_path));
        }
        if(!TextUtils.isEmpty(pan_card_path)){
            picPaths.add(new DocumentUploadData(PANCARD, pan_card_path));
        }
        if(!TextUtils.isEmpty(registration_certificate_path)){
            picPaths.add(new DocumentUploadData(REGISTRATION_CERTI, registration_certificate_path));
        }
        if(!TextUtils.isEmpty(motor_insurance_path)){
            picPaths.add(new DocumentUploadData(MOTOR_INSURANCE, motor_insurance_path));
        }
        startService(DocumentsUploadService.getIntent(this, picPaths, id));
        onUploadComplete();
    }

    private void onUploadComplete() {
        showToast("Uploading Documents");
        hideProgressBar();
        setResult(Activity.RESULT_OK);
        finish();
    }



    private void initView() {
        mToolbar = findViewById(R.id.toolbar);
        mBtnProfilePhoto = findViewById(R.id.btn_profile_photo);
        mBtnDriverLicence = findViewById(R.id.btn_driver_licence);
        mBtnPanCard = findViewById(R.id.btn_pan_card);
        mBtnCertificateRegistration = findViewById(R.id.btn_certificate_registration);
        mBtnMotorInsurance = findViewById(R.id.btn_motor_insurance);
        mBtnPoliceVerfication = findViewById(R.id.btn_police_verfication);
        mBtnAadharCard = findViewById(R.id.btn_aadhar_card);
        mBtnSubmit = findViewById(R.id.btn_submit);
        mProfilePicArrow = findViewById(R.id.profile_pic_arrow);
        mDriverLicencePicArrow = findViewById(R.id.driver_licence_pic_arrow);
        mPanCardPicArrow = findViewById(R.id.pan_card_pic_arrow);
        mRegistrationCertificatePicArrow = findViewById(R.id.registration_certificate_pic_arrow);
        mMotorInsurancePicArrow = findViewById(R.id.motor_insurance_pic_arrow);
        mPoliceVerificationPicArrow = findViewById(R.id.police_verification_pic_arrow);
        mAadharCardPicArrow = findViewById(R.id.aadhar_card_pic_arrow);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == IMAGE_URI_RESULT) {
            final Uri uri = data.getData();
            switch (requestCode) {
                case REQUEST_PROFILE_PICTURE:
                    profile_pic_path = uri.getPath();
                    mProfilePicArrow.setImageResource(R.drawable.success);
                    break;
                case REQUEST_DRIVER_LICENCE:
                    driver_licence_path = uri.getPath();
                    mDriverLicencePicArrow.setImageResource(R.drawable.success);
                    break;
                case REQUEST_PAN_CARD:
                    pan_card_path = uri.getPath();
                    mPanCardPicArrow.setImageResource(R.drawable.success);
                    break;
                case REQUEST_REGISTRATION_CERTIFICATE:
                    registration_certificate_path = uri.getPath();
                    mRegistrationCertificatePicArrow.setImageResource(R.drawable.success);
                    break;
                case REQUEST_MOTOR_INSURANCE_CERTIFICATE:
                    motor_insurance_path = uri.getPath();
                    mMotorInsurancePicArrow.setImageResource(R.drawable.success);
                    break;
                case REQUEST_POLICE_VERIFICATION:
                    police_verification_path = uri.getPath();
                    mPoliceVerificationPicArrow.setImageResource(R.drawable.success);
                    break;
                case REQUEST_AADHAR_CARD:
                    aadhar_card_path = uri.getPath();
                    mAadharCardPicArrow.setImageResource(R.drawable.success);
                    break;
            }
        }
    }
}