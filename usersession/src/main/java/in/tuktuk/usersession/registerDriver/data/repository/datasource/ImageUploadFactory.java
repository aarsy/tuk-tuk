package in.tuktuk.usersession.registerDriver.data.repository.datasource;

import javax.inject.Inject;

import in.tuktuk.usersession.registerDriver.data.model.GetImageResponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;

/**
 * Created by sandeepgoyal on 13/05/18.
 */

public class ImageUploadFactory {
    private CloudImageUploader cloudImageUploader;

    @Inject
    public ImageUploadFactory(CloudImageUploader cloudImageUploader) {
        this.cloudImageUploader = cloudImageUploader;
    }

    public Observable<GetImageResponse> uploadDocument(RequestBody documentType, RequestBody driver_id, MultipartBody.Part params) {
        return cloudImageUploader.uploadDocument(documentType, driver_id, params);
    }
}
