package in.tuktuk.usersession.registerDriver.domain;

import in.tuktuk.usersession.registerDriver.data.model.GetImageResponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;

/**
 * Created by sandeepgoyal on 13/05/18.
 */

public interface IImageUploadRepository {
    Observable<GetImageResponse> uploadDocument(RequestBody documentType, RequestBody driver_id, MultipartBody.Part params);

}
