package in.tuktuk.usersession.registerDriver.view.presenter;

import javax.inject.Inject;

import frameworks.basemvp.AppBasePresenter;
import frameworks.network.model.ResponseException;
import in.tuktuk.usersession.registerDriver.data.model.CreateDriverResponse;
import in.tuktuk.usersession.registerDriver.data.model.DriverInfo;
import in.tuktuk.usersession.registerDriver.domain.UserRegisterUseCase;
import rx.Subscriber;

public class DriverDocumentPresenter extends AppBasePresenter<DriverDocumentContract.View> implements DriverDocumentContract.Presenter {
    private UserRegisterUseCase userRegisterUseCase;

    @Inject
    public DriverDocumentPresenter(UserRegisterUseCase userRegisterUseCase) {
        this.userRegisterUseCase = userRegisterUseCase;
    }


    @Override
    public void uploadDriverInfo(DriverInfo driverInfo) {
        userRegisterUseCase.execute(userRegisterUseCase.createRequestParams(driverInfo), new Subscriber<CreateDriverResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                getView().hideProgressBar();
                if (e instanceof ResponseException) {
                    getView().showToast(e.getMessage());
                } else {
                    getView().showToast("Some Error Occurred Please Try again");
                }
            }

            @Override
            public void onNext(CreateDriverResponse createDriverResponse) {
                getView().hideProgressBar();
                getView().uploadDocuments(String.valueOf(createDriverResponse.getUserID()));
            }
        });
    }
}
