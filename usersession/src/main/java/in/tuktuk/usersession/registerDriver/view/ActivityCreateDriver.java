package in.tuktuk.usersession.registerDriver.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;


import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBufferResponse;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.RuntimeRemoteException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.List;

import javax.inject.Inject;

import frameworks.AppBaseApplication;
import frameworks.basemvp.AppBaseActivity;
import frameworks.utils.Validator;
import in.tuktuk.usersession.R;
import in.tuktuk.usersession.registerDriver.data.model.DriverInfo;
import in.tuktuk.usersession.registerDriver.di.CreateUserComponent;
import in.tuktuk.usersession.registerDriver.di.DaggerCreateUserComponent;
import in.tuktuk.usersession.registerDriver.view.adapter.PlaceAutocompleteAdapter;
import in.tuktuk.usersession.registerDriver.view.presenter.CreateDriverContract;
import in.tuktuk.usersession.registerDriver.view.presenter.CreateDriverPresenter;

public class ActivityCreateDriver extends AppBaseActivity<CreateDriverContract.Presenter> implements CreateDriverContract.View {


    private EditText mEdtName;
    private EditText mEdtEmailId;
    private EditText mEdtPassword;
    private EditText mEdtPhno;
    private AutoCompleteTextView mEdtCity;
    private Button mCreatButton;
    private Spinner spinnerVehicleType;

    protected GeoDataClient mGeoDataClient;
    private PlaceAutocompleteAdapter mAdapter;

    @Inject
    CreateDriverPresenter mPresenter;

    public static final LatLngBounds BOUNDS_INDIA = new LatLngBounds(new LatLng(7.798000, 68.14712), new LatLng(37.090000, 97.34466));
    private CreateUserComponent createUserComponent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGeoDataClient = Places.getGeoDataClient(this, null);
        initView();
        handleClick();
        setLeftIcon(R.drawable.back_arrow_white);
        setTitle(getString(R.string.title_activity_create_driver));
        mPresenter.getVehicleTypes();
    }

    @Override
    public void onLeftMenuClick() {
        super.onLeftMenuClick();
        onBackPressed();
    }

    @Override
    protected void initInjector() {
        createUserComponent = DaggerCreateUserComponent.builder().baseAppComponent(AppBaseApplication.getApplication().getBaseAppComponent()).build();
        createUserComponent.inject(this);
        mPresenter.attachView(this);
    }

    @Override
    public int getViewToCreate() {
        return R.layout.activity_create_driver;
    }

    @Override
    public CreateDriverContract.Presenter getPresenter() {
        return mPresenter;
    }

    private void handleClick() {
        mCreatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validName()) {
                    if (validEmailId()) {
                        if (validPassword()) {
                            if (validPhoneNumber()) {
                                if (validCity()) {
                                    submitProfile();
                                } else {
                                    mEdtCity.setError("Please enter City");
                                    mEdtCity.requestFocus();
                                }
                            }else {
                                mEdtPhno.setError("Please enter valid Phone number");
                                mEdtPhno.requestFocus();
                            }
                        }else {
                            mEdtPassword.setError("Password length should greater than 6");
                            mEdtPassword.requestFocus();
                        }
                    }else {
                        mEdtEmailId.setError("Please enter valid Email-Id");
                        mEdtEmailId.requestFocus();
                    }
                }else {
                    mEdtName.setError("Please enter valid Name");
                    mEdtName.requestFocus();
                }
            }
        });
    }

    private void submitProfile() {
        DriverInfo driverInfo = new DriverInfo();
        driverInfo.setName(mEdtName.getText().toString());
        driverInfo.setCity(mEdtCity.getText().toString());
        driverInfo.setEmailid(mEdtEmailId.getText().toString());
        driverInfo.setMobileNumber(mEdtPhno.getText().toString());
        driverInfo.setPassword(mEdtPassword.getText().toString());
        driverInfo.setVehicleType(mPresenter.getVehicleIdFor(spinnerVehicleType.getSelectedItemPosition()));
        startActivityForResult(DriverDocument.getInstance(driverInfo,this),1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1) {
            if(resultCode == Activity.RESULT_OK) {
                finish();
            }
        }
    }

    private boolean validCity() {
        return mEdtCity.getText().toString().length() >= 1;
    }

    private boolean validPhoneNumber() {
        return Validator.validateMobile(mEdtPhno.getText().toString());
    }

    private boolean validPassword() {
        return Validator.validatePassword(mEdtPassword.getText().toString());
    }

    private boolean validEmailId() {
        return Validator.validateEmail(mEdtEmailId.getText().toString());
    }

    private boolean validName() {
        return Validator.validateName(mEdtName.getText().toString());
    }


    private void initView() {
        mEdtName = findViewById(R.id.edt_name);
        mEdtEmailId = findViewById(R.id.edt_email_id);
        mEdtPassword = findViewById(R.id.edt_password);
        mEdtPhno = findViewById(R.id.edt_phno);
        mEdtCity = findViewById(R.id.edt_city);
        mCreatButton = findViewById(R.id.nxt_button);
        spinnerVehicleType=findViewById(R.id.spinner_vehicle_type);
        mAdapter = new PlaceAutocompleteAdapter(this, mGeoDataClient, BOUNDS_INDIA, null);
        mEdtCity.setAdapter(mAdapter);

    }

    /**
     * Listener that handles selections from suggestions from the AutoCompleteTextView that
     * displays Place suggestions.
     * Gets the place id of the selected item and issues a request to the Places Geo ImageResponseData Client
     * to retrieve more details about the place.
     *
     * @see GeoDataClient#getPlaceById(String...)
     */
    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            /*
             Retrieve the place ID of the selected item from the Adapter.
             The adapter stores each Place suggestion in a AutocompletePrediction from which we
             read the place ID and title.
              */
            final AutocompletePrediction item = mAdapter.getItem(position);
            final String placeId = item.getPlaceId();
            final CharSequence primaryText = item.getPrimaryText(null);


            /*
             Issue a request to the Places Geo ImageResponseData Client to retrieve a Place object with
             additional details about the place.
              */
            Task<PlaceBufferResponse> placeResult = mGeoDataClient.getPlaceById(placeId);
            placeResult.addOnCompleteListener(mUpdatePlaceDetailsCallback);

        }
    };

    /**
     * Callback for results from a Places Geo ImageResponseData Client query that shows the first place result in
     * the details view on screen.
     */
    private OnCompleteListener<PlaceBufferResponse> mUpdatePlaceDetailsCallback
            = new OnCompleteListener<PlaceBufferResponse>() {
        @Override
        public void onComplete(Task<PlaceBufferResponse> task) {
            try {
                PlaceBufferResponse places = task.getResult();

                // Get the Place object from the buffer.
                final Place place = places.get(0);


                places.release();
            } catch (RuntimeRemoteException e) {
                // Request did not complete successfully
                return;
            }
        }
    };

    @Override
    public void renderVehicleTypes(List<String> vehicleTypes) {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, vehicleTypes);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerVehicleType.setAdapter(dataAdapter);
    }
}