
package in.tuktuk.usersession.registerDriver.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ImageResponseData implements Serializable
{

    @SerializedName("DriverId")
    @Expose
    private String driverId;
    @SerializedName("adhar_card_front_status")
    @Expose
    private Integer adharCardFrontStatus;
    @SerializedName("adhar_card_back_status")
    @Expose
    private int adharCardBackStatus;
    @SerializedName("pan_card_status")
    @Expose
    private int panCardStatus;
    @SerializedName("driving_licencefront_status")
    @Expose
    private int drivingLicencefrontStatus;
    @SerializedName("driving_licenceback_status")
    @Expose
    private int drivingLicencebackStatus;
    @SerializedName("police_verification_status")
    @Expose
    private int policeVerificationStatus;
    private final static long serialVersionUID = -7114799878882769649L;

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public int getAdharCardFrontStatus() {
        return adharCardFrontStatus;
    }

    public void setAdharCardFrontStatus(int adharCardFrontStatus) {
        this.adharCardFrontStatus = adharCardFrontStatus;
    }

    public int getAdharCardBackStatus() {
        return adharCardBackStatus;
    }

    public void setAdharCardBackStatus(int adharCardBackStatus) {
        this.adharCardBackStatus = adharCardBackStatus;
    }

    public int getPanCardStatus() {
        return panCardStatus;
    }

    public void setPanCardStatus(int panCardStatus) {
        this.panCardStatus = panCardStatus;
    }

    public int getDrivingLicencefrontStatus() {
        return drivingLicencefrontStatus;
    }

    public void setDrivingLicencefrontStatus(int drivingLicencefrontStatus) {
        this.drivingLicencefrontStatus = drivingLicencefrontStatus;
    }

    public int getDrivingLicencebackStatus() {
        return drivingLicencebackStatus;
    }

    public void setDrivingLicencebackStatus(int drivingLicencebackStatus) {
        this.drivingLicencebackStatus = drivingLicencebackStatus;
    }

    public int getPoliceVerificationStatus() {
        return policeVerificationStatus;
    }

    public void setPoliceVerificationStatus(int policeVerificationStatus) {
        this.policeVerificationStatus = policeVerificationStatus;
    }

}
