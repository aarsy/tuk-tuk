package in.tuktuk.usersession.registerDriver.service;


import java.util.ArrayList;

import in.tuktuk.usersession.registerDriver.view.DocumentUploadData;

public interface IUploadDocumentsServiceContract {

    interface UploadImagesListener {

        void onProgressComplete();

        void onProgressFail();
    }

    interface UploadImagesPresenter {
        void uploadDocuments(ArrayList<DocumentUploadData> documentData, String userId);
    }
}
