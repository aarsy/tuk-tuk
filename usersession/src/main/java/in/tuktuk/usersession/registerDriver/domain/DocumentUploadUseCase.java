package in.tuktuk.usersession.registerDriver.domain;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import frameworks.network.usecases.RequestParams;
import frameworks.network.usecases.UseCase;

import in.tuktuk.usersession.registerDriver.data.model.GetImageResponse;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;

/**
 * Created by sandeepgoyal on 13/05/18.
 */

public class DocumentUploadUseCase extends UseCase<GetImageResponse> {
    public static final String DOCUMENT_TYPE = "DOCUMENT_TYPE";
    public static String IMAGE_PATH = "PATH";
    IImageUploadRepository repository;

    @Inject
    public DocumentUploadUseCase(IImageUploadRepository repository) {
        this.repository = repository;
    }

    @Override
    public Observable<GetImageResponse> createObservable(RequestParams requestParams) {
        return generateRequestImageAndUpload(requestParams.getString(IMAGE_PATH, "")
        , requestParams.getString(DOCUMENT_TYPE, "")
                , requestParams.getString(DocumentListUploadSubmitUseCase.USER_ID, ""));
    }

    public Map<String, RequestBody> getParamsUploadImage(String pathFile) {
        Map<String, RequestBody> paramsUploadImage = new HashMap<>();

        File file = new File(pathFile);

        RequestBody fileToUpload = RequestBody.create(MediaType.parse("image/*"), file);

        paramsUploadImage.put("image", fileToUpload);

        return paramsUploadImage;
    }


    private Observable<GetImageResponse> generateRequestImageAndUpload(String pathFile, String type, String userId) {

        File file = new File(pathFile);
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
        RequestBody type1 = RequestBody.create(MediaType.parse("text/plain"), type);
        RequestBody driver_id = RequestBody.create(MediaType.parse("text/plain"), userId);
        return repository.uploadDocument(type1, driver_id, body);

    }

}
