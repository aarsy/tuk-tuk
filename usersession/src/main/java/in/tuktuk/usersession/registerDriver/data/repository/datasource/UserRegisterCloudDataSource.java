package in.tuktuk.usersession.registerDriver.data.repository.datasource;

import javax.inject.Inject;

import frameworks.network.mapper.DataResponseMapper;
import frameworks.network.model.DataResponse;
import in.tuktuk.usersession.registerDriver.data.model.CreateDriverResponse;
import in.tuktuk.usersession.registerDriver.data.model.DriverInfo;
import in.tuktuk.usersession.registerDriver.data.model.VehicleTypeResponse;
import in.tuktuk.usersession.registerDriver.data.net.UserRegisterAPI;
import rx.Observable;
import rx.functions.Func1;

public class UserRegisterCloudDataSource {
    UserRegisterAPI deliveryAPI;

    @Inject
    public UserRegisterCloudDataSource(UserRegisterAPI deliveryAPI) {
        this.deliveryAPI = deliveryAPI;
    }

    public Observable<CreateDriverResponse> createUser(DriverInfo driverInfo) {
        return deliveryAPI.createUser(driverInfo).map(new Func1<DataResponse<CreateDriverResponse>, CreateDriverResponse>() {
            @Override
            public CreateDriverResponse call(DataResponse<CreateDriverResponse> createDriverResponseDataResponse) {
                return createDriverResponseDataResponse.getData();
            }
        });
    }

    public Observable<VehicleTypeResponse> getVehicleType() {
        return deliveryAPI.getVehicleType().map(new DataResponseMapper<VehicleTypeResponse>());
    }
}
