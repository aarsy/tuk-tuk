package in.tuktuk.usersession.registerDriver.data.repository.datasource;

import javax.inject.Inject;


import in.tuktuk.usersession.registerDriver.data.model.GetImageResponse;
import in.tuktuk.usersession.registerDriver.data.net.UserRegisterAPI;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;

/**
 * Created by sandeepgoyal on 13/05/18.
 */

public class CloudImageUploader {
    private UserRegisterAPI documentUpload;

    @Inject
    public CloudImageUploader(UserRegisterAPI documentUpload) {
        this.documentUpload = documentUpload;
    }

    public Observable<GetImageResponse> uploadDocument(RequestBody documentType, RequestBody driver_id, MultipartBody.Part params) {
        return documentUpload.documentUpload(documentType, driver_id, params);
    }

}
