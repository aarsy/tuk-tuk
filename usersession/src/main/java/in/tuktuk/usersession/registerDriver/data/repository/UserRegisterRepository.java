package in.tuktuk.usersession.registerDriver.data.repository;




import javax.inject.Inject;

import in.tuktuk.usersession.registerDriver.data.model.CreateDriverResponse;
import in.tuktuk.usersession.registerDriver.data.model.DriverInfo;
import in.tuktuk.usersession.registerDriver.data.model.VehicleTypeResponse;
import in.tuktuk.usersession.registerDriver.data.repository.datasource.UserRegisteryDataFactory;
import in.tuktuk.usersession.registerDriver.domain.IUserRegisterRepository;
import rx.Observable;


public class UserRegisterRepository implements IUserRegisterRepository {
    UserRegisteryDataFactory userRegisteryDataFactory;
    @Inject
    public UserRegisterRepository(UserRegisteryDataFactory userRegisteryDataFactory) {
        this.userRegisteryDataFactory = userRegisteryDataFactory;

    }

    @Override
    public Observable<CreateDriverResponse> createUser(DriverInfo driverInfo) {
        return userRegisteryDataFactory.getDataSource().createUser(driverInfo);
    }

    @Override
    public Observable<VehicleTypeResponse> getVehicleType() {
        return userRegisteryDataFactory.getDataSource().getVehicleType();
    }
}
