package in.tuktuk.usersession.registerDriver.di;


import dagger.Component;
import frameworks.di.component.BaseAppComponent;
import in.tuktuk.usersession.registerDriver.service.DocumentsUploadService;
import in.tuktuk.usersession.registerDriver.view.ActivityCreateDriver;
import in.tuktuk.usersession.registerDriver.view.DriverDocument;

@CreateUserScope
@Component(modules = {CreateUserModule.class}, dependencies = BaseAppComponent.class)
public interface CreateUserComponent {
    void inject(DriverDocument driverDocument);

    void inject(ActivityCreateDriver activityCreateDriver);

    void inject(DocumentsUploadService documentsUploadService);
}
