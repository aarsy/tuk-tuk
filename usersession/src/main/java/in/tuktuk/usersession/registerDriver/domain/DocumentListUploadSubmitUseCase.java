package in.tuktuk.usersession.registerDriver.domain;


import java.util.ArrayList;

import javax.inject.Inject;

import frameworks.network.usecases.RequestParams;
import frameworks.network.usecases.UseCase;
import in.tuktuk.usersession.registerDriver.data.model.GetImageResponse;
import in.tuktuk.usersession.registerDriver.view.DocumentUploadData;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by sandeepgoyal on 13/05/18.
 */

public class DocumentListUploadSubmitUseCase extends UseCase<GetImageResponse> {
    private final String IMAGE_PATHS = "IMAGE_PATHS";
    public static final String USER_ID = "USER_ID";
    private final DocumentUploadUseCase documentUploadUseCase;


    public RequestParams createRequestParams(ArrayList<DocumentUploadData> paths, String userId) {
        RequestParams requestParams = RequestParams.create();
        requestParams.putObject(IMAGE_PATHS, paths);
        requestParams.putString(USER_ID, userId);
        return requestParams;
    }


    @Inject
    public DocumentListUploadSubmitUseCase(DocumentUploadUseCase documentUploadUseCase) {
        this.documentUploadUseCase = documentUploadUseCase;
    }

    @Override
    public Observable<GetImageResponse> createObservable(final RequestParams requestParams) {
        ArrayList<DocumentUploadData> filePaths = (ArrayList<DocumentUploadData>) requestParams.getObject(IMAGE_PATHS);
        requestParams.getParameters().remove(IMAGE_PATHS);
        if (filePaths == null || filePaths.size() == 0)
            return null;
        else {
            return Observable.from(filePaths).concatMap(new Func1<DocumentUploadData, Observable<GetImageResponse>>() {
                @Override
                public Observable<GetImageResponse> call(final DocumentUploadData documentUploadData) {
                    requestParams.putString(DocumentUploadUseCase.IMAGE_PATH, documentUploadData.getFilePath());
                    requestParams.putString(DocumentUploadUseCase.DOCUMENT_TYPE, documentUploadData.getDocumentType());
                    return documentUploadUseCase.createObservable(requestParams);
                }
            });
        }
    }


}
