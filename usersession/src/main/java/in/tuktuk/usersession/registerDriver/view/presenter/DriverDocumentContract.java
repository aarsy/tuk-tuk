package in.tuktuk.usersession.registerDriver.view.presenter;

import frameworks.basemvp.IPresenter;
import frameworks.basemvp.IView;
import in.tuktuk.usersession.registerDriver.data.model.DriverInfo;

public interface DriverDocumentContract {
    interface View extends IView{

        void uploadDocuments(String id);
    }
    interface Presenter extends IPresenter<View>{

        void uploadDriverInfo(DriverInfo driverInfo);
    }
}
