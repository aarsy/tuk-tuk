package in.tuktuk.usersession.registerDriver.data.repository;

import javax.inject.Inject;

import in.tuktuk.usersession.registerDriver.data.model.GetImageResponse;
import in.tuktuk.usersession.registerDriver.data.repository.datasource.ImageUploadFactory;
import in.tuktuk.usersession.registerDriver.domain.IImageUploadRepository;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;

/**
 * Created by sandeepgoyal on 13/05/18.
 */

public class ImageUploadRepository implements IImageUploadRepository {
    ImageUploadFactory imageUploadFactory;

    @Inject
    public ImageUploadRepository(ImageUploadFactory imageUploadFactory) {
        this.imageUploadFactory = imageUploadFactory;
    }

    @Override
    public Observable<GetImageResponse> uploadDocument(RequestBody documentType, RequestBody driver_id, MultipartBody.Part params) {
        return imageUploadFactory.uploadDocument(documentType, driver_id, params);
    }

}
