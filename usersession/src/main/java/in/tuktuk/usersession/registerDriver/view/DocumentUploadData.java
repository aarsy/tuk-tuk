package in.tuktuk.usersession.registerDriver.view;

import android.os.Parcel;
import android.os.Parcelable;

public class DocumentUploadData implements Parcelable {
    private String filePath;
    private String documentType;

    public DocumentUploadData(String documentType,String filePath) {
        this.filePath = filePath;
        this.documentType = documentType;
    }

    protected DocumentUploadData(Parcel in) {
        filePath = in.readString();
        documentType = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(filePath);
        dest.writeString(documentType);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DocumentUploadData> CREATOR = new Creator<DocumentUploadData>() {
        @Override
        public DocumentUploadData createFromParcel(Parcel in) {
            return new DocumentUploadData(in);
        }

        @Override
        public DocumentUploadData[] newArray(int size) {
            return new DocumentUploadData[size];
        }
    };

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }
}
