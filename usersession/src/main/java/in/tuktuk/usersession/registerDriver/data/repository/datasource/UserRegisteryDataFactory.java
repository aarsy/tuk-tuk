package in.tuktuk.usersession.registerDriver.data.repository.datasource;

import javax.inject.Inject;

public class UserRegisteryDataFactory {

    UserRegisterCloudDataSource userRegisterCloudDataSource;

    @Inject
    public UserRegisteryDataFactory(UserRegisterCloudDataSource userRegisterCloudDataSource) {
        this.userRegisterCloudDataSource = userRegisterCloudDataSource;
    }

    public UserRegisterCloudDataSource getDataSource() {
        return userRegisterCloudDataSource;
    }

}
