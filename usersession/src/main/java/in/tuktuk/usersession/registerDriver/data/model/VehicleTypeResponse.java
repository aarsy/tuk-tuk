package in.tuktuk.usersession.registerDriver.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VehicleTypeResponse {

    @SerializedName("message")
    private String message;

    @SerializedName("VehicleTypes")
    private List<VehicleType> vehicleTypes;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<VehicleType> getVehicleTypes() {
        return vehicleTypes;
    }

    public void setVehicleTypes(List<VehicleType> vehicleTypes) {
        this.vehicleTypes = vehicleTypes;
    }
}
