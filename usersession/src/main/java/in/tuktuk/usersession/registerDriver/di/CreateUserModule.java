package in.tuktuk.usersession.registerDriver.di;


import dagger.Module;
import dagger.Provides;
import frameworks.network.interceptor.ErrorResponseInterceptor;
import frameworks.network.model.BaseResponseError;
import in.tuktuk.usersession.registerDriver.data.net.UserRegisterAPI;
import in.tuktuk.usersession.registerDriver.data.net.UserRegisterURL;
import in.tuktuk.usersession.registerDriver.data.repository.ImageUploadRepository;
import in.tuktuk.usersession.registerDriver.data.repository.UserRegisterRepository;
import in.tuktuk.usersession.registerDriver.data.repository.datasource.ImageUploadFactory;
import in.tuktuk.usersession.registerDriver.data.repository.datasource.UserRegisteryDataFactory;
import in.tuktuk.usersession.registerDriver.domain.IImageUploadRepository;
import in.tuktuk.usersession.registerDriver.domain.IUserRegisterRepository;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;

@Module
public class CreateUserModule {

    @Provides
    Retrofit provideRetrofit(OkHttpClient okHttpClient,
                             Retrofit.Builder retrofitBuilder) {
        return retrofitBuilder.baseUrl(UserRegisterURL.BASE_URL).client(okHttpClient).build();
    }

    @Provides
    OkHttpClient provideOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .addInterceptor(new ErrorResponseInterceptor(BaseResponseError.class))
                .build();
    }

    @Provides
    @CreateUserScope
    UserRegisterAPI provideDeliveryApi(Retrofit retrofit) {
        return retrofit.create(UserRegisterAPI.class);
    }
    @Provides
    @CreateUserScope
    IUserRegisterRepository getUserRegisterRepository(UserRegisteryDataFactory userRegisteryDataFactory) {
        return new UserRegisterRepository(userRegisteryDataFactory);
    }

    @Provides
    @CreateUserScope
    IImageUploadRepository getImageUploadRepository(ImageUploadFactory imageUploadFactory) {
        return new ImageUploadRepository(imageUploadFactory);
    }

}

