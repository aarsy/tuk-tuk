package in.tuktuk.usersession.registerDriver.data.model;

import com.google.gson.annotations.SerializedName;

public class CreateDriverResponse{

	@SerializedName("message")
	private String message;

	@SerializedName("userID")
	private int userID;

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setUserID(int userID){
		this.userID = userID;
	}

	public int getUserID(){
		return userID;
	}

	@Override
 	public String toString(){
		return 
			"CreateDriverResponse{" + 
			"message = '" + message + '\'' + 
			",userID = '" + userID + '\'' + 
			"}";
		}
}