package in.tuktuk.usersession.registerDriver.data.net;


public class UserRegisterURL {
    public static String BASE_URL = "http://122.160.30.50:5092/";
    public static final String CREATE_USER_API = "create";
    public static final String GET_VEHICLE_TYPE = "vehicle/getVehicleType";
    public static final String PUT_DOCUMENT = "edit";
}
