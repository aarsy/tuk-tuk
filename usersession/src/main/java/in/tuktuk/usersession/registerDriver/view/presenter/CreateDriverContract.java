package in.tuktuk.usersession.registerDriver.view.presenter;

import java.util.List;

import frameworks.basemvp.IPresenter;
import frameworks.basemvp.IView;

public interface CreateDriverContract {

    interface View extends IView {

        void renderVehicleTypes(List<String> vehicleTypes);
    }

    interface Presenter extends IPresenter<View> {
        int getVehicleIdFor(int position);

        void getVehicleTypes();
    }
}
