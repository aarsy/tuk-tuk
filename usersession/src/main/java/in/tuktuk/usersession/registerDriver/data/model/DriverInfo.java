package in.tuktuk.usersession.registerDriver.data.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DriverInfo implements Serializable {

	public static final String DRIVER_TYPE = "2";

	@SerializedName("password")
	private String password;

	@SerializedName("user_type")
	private String userType = DRIVER_TYPE;

	@SerializedName("city")
	private String city;

	@SerializedName("sex")
	private String sex;

	@SerializedName("name")
	private String name;

	@SerializedName("emailid")
	private String emailid;

	@SerializedName("mobile_number")
	private String mobileNumber;

	@SerializedName("age")
	private String age;

	@SerializedName("vehicle_type")
	private int vehicleType;

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}

	public void setUserType(String userType){
		this.userType = userType;
	}

	public String getUserType(){
		return userType;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	public void setSex(String sex){
		this.sex = sex;
	}

	public String getSex(){
		return sex;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setEmailid(String emailid){
		this.emailid = emailid;
	}

	public String getEmailid(){
		return emailid;
	}

	public void setMobileNumber(String mobileNumber){
		this.mobileNumber = mobileNumber;
	}

	public String getMobileNumber(){
		return mobileNumber;
	}

	public void setAge(String age){
		this.age = age;
	}

	public String getAge(){
		return age;
	}

	public int getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(int vehicleType) {
		this.vehicleType = vehicleType;
	}

	@Override
 	public String toString(){
		return 
			"DriverInfo{" + 
			"password = '" + password + '\'' + 
			",user_type = '" + userType + '\'' + 
			",city = '" + city + '\'' + 
			",sex = '" + sex + '\'' + 
			",name = '" + name + '\'' + 
			",emailid = '" + emailid + '\'' + 
			",mobile_number = '" + mobileNumber + '\'' + 
			",age = '" + age + '\'' + 
			"}";
		}
}