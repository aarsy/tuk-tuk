package in.tuktuk.usersession.registerDriver.data.model;

import com.google.gson.annotations.SerializedName;

public class VehicleType {
    @SerializedName("vehicle_id")
    private int vehicleId;
    @SerializedName("vehicle_type")
    private String vehicleType;

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }
}
