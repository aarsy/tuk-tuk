package in.tuktuk.usersession.registerDriver.domain;


import in.tuktuk.usersession.registerDriver.data.model.CreateDriverResponse;
import in.tuktuk.usersession.registerDriver.data.model.DriverInfo;
import in.tuktuk.usersession.registerDriver.data.model.VehicleTypeResponse;
import rx.Observable;

public interface IUserRegisterRepository {

    Observable<CreateDriverResponse> createUser(DriverInfo driverInfo);

    Observable<VehicleTypeResponse> getVehicleType();

}
