package in.tuktuk.usersession.registerDriver.view.presenter;

import android.text.TextUtils;



import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import frameworks.basemvp.AppBasePresenter;
import in.tuktuk.usersession.registerDriver.data.model.VehicleType;
import in.tuktuk.usersession.registerDriver.data.model.VehicleTypeResponse;
import in.tuktuk.usersession.registerDriver.domain.GetVehicleTypeUseCase;
import rx.Subscriber;

public class CreateDriverPresenter extends AppBasePresenter<CreateDriverContract.View> implements CreateDriverContract.Presenter {


    private final GetVehicleTypeUseCase getVehicleTypeUseCase;
    private List<VehicleType> vehicleTypes;

    @Inject
    public CreateDriverPresenter(GetVehicleTypeUseCase getVehicleTypeUseCase) {
        this.getVehicleTypeUseCase = getVehicleTypeUseCase;
    }

    @Override
    public int getVehicleIdFor(int position){
        if(position<vehicleTypes.size()){
            return vehicleTypes.get(position).getVehicleId();
        }else{
            return -1;
        }
    }
    @Override
    public void getVehicleTypes() {
        getVehicleTypeUseCase.execute(new Subscriber<VehicleTypeResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(VehicleTypeResponse vehicleTypeResponse) {
                if(vehicleTypeResponse!=null && vehicleTypeResponse.getVehicleTypes()!=null && vehicleTypeResponse.getVehicleTypes().size()>0){
                    vehicleTypes=vehicleTypeResponse.getVehicleTypes();
                    ArrayList<String> vehicleType=new ArrayList<>();
                    for(VehicleType vehicleType1: vehicleTypes){
                        if(vehicleType1!=null && !TextUtils.isEmpty(vehicleType1.getVehicleType())){
                            vehicleType.add(vehicleType1.getVehicleType());
                        }
                    }
                    getView().renderVehicleTypes(vehicleType);
                }
            }
        });
    }

    @Override
    public void detachView() {
        super.detachView();
        getVehicleTypeUseCase.unsubscribe();
    }
}
