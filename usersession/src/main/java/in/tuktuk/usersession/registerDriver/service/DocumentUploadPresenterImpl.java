package in.tuktuk.usersession.registerDriver.service;

import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

import javax.inject.Inject;

import in.tuktuk.usersession.registerDriver.data.model.GetImageResponse;
import in.tuktuk.usersession.registerDriver.domain.DocumentListUploadSubmitUseCase;
import in.tuktuk.usersession.registerDriver.view.DocumentUploadData;
import rx.Subscriber;

public class DocumentUploadPresenterImpl implements IUploadDocumentsServiceContract.UploadImagesPresenter {
    private IUploadDocumentsServiceContract.UploadImagesListener uploadImagesListener;
    private DocumentListUploadSubmitUseCase imageUploadUseCase;

    @Inject
    public DocumentUploadPresenterImpl(DocumentListUploadSubmitUseCase imageUploadUseCase) {
        this.imageUploadUseCase = imageUploadUseCase;
    }

    public void attach(IUploadDocumentsServiceContract.UploadImagesListener uploadImagesListener) {
        this.uploadImagesListener = uploadImagesListener;
    }

    @Override
    public void uploadDocuments(ArrayList<DocumentUploadData> documentData, String userId) {
        imageUploadUseCase.execute(imageUploadUseCase.createRequestParams(documentData, userId), new Subscriber<GetImageResponse>() {
            @Override
            public void onCompleted() {
                Log.d("OnCompletedExecuted", "true");
                uploadImagesListener.onProgressComplete();
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                uploadImagesListener.onProgressFail();
            }

            @Override
            public void onNext(GetImageResponse imageUploadResponse) {
                Log.d("uploadresp", imageUploadResponse.toString());
            }
        });
    }

}
