package in.tuktuk.usersession.registerDriver.data.net;



import frameworks.network.model.DataResponse;
import in.tuktuk.usersession.registerDriver.data.model.CreateDriverResponse;
import in.tuktuk.usersession.registerDriver.data.model.DriverInfo;
import in.tuktuk.usersession.registerDriver.data.model.GetImageResponse;
import in.tuktuk.usersession.registerDriver.data.model.VehicleTypeResponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import rx.Observable;

public interface UserRegisterAPI {

    @POST(UserRegisterURL.CREATE_USER_API)
    Observable<DataResponse<CreateDriverResponse>> createUser(@Body DriverInfo driverInfo);

    @GET(UserRegisterURL.GET_VEHICLE_TYPE)
    Observable<Response<DataResponse<VehicleTypeResponse>>> getVehicleType();

    @Multipart
    @PUT(UserRegisterURL.PUT_DOCUMENT)
    Observable<GetImageResponse> documentUpload(
            @Part("type") RequestBody type,
            @Part("driver_id") RequestBody driver_id,
            @Part MultipartBody.Part body);
}
