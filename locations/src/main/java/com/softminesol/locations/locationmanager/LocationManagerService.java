package com.softminesol.locations.locationmanager;

import android.location.Location;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by naveen on 15/07/2017.
 */

public class LocationManagerService {

    Locations currentLocation;
    boolean isLocationPermissionGiven;
    boolean isLocationSettingEnabled;
    boolean isLocationSettingDenied;
    List<OnLocationChangedListener> onLocationChangedListeners = new ArrayList<>();



    private static final LocationManagerService ourInstance = new LocationManagerService();

    public static LocationManagerService getInstance() {
        return ourInstance;
    }

    private LocationManagerService() {
    }

    public Locations getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(Locations currentLocation) {
        this.currentLocation = currentLocation;
    }


    public void addOnLocationChangedListeners(OnLocationChangedListener onLocationChangedListeners) {
        synchronized (this) {
            this.onLocationChangedListeners.add(onLocationChangedListeners);
        }

    }

    public void removeLocationChangedListener(OnLocationChangedListener locationChangedListener) {
        synchronized (this) {
            this.onLocationChangedListeners.remove(locationChangedListener);
        }
    }

    public void setCurrentLocation(Location currentLocation) {
        synchronized (this) {
            Locations locations = new Locations();
            locations.setLattitude(currentLocation.getLatitude());
            locations.setLongitude(currentLocation.getLongitude());
            this.currentLocation = locations;
            for (OnLocationChangedListener onLocationChangedListener : new ArrayList<OnLocationChangedListener>(this.onLocationChangedListeners)) {
                onLocationChangedListener.onLocationChanged(this.currentLocation);
            }
        }
    }

    public boolean isLocationPermissionGiven() {
        return isLocationPermissionGiven;
    }

    public void setLocationPermissionGiven(boolean locationPermissionGiven) {
        isLocationPermissionGiven = locationPermissionGiven;
    }

    public boolean isLocationSettingEnabled() {
        return isLocationSettingEnabled;
    }

    public void setLocationSettingEnabled(boolean locationSettingEnabled) {
        isLocationSettingEnabled = locationSettingEnabled;
    }

    public boolean isLocationSettingDenied() {
        return isLocationSettingDenied;
    }

    public void setLocationSettingDenied(boolean locationSettingDenied) {
        isLocationSettingDenied = locationSettingDenied;
    }

    public interface OnLocationChangedListener {
        public void onLocationChanged(Locations location);
    }
}
